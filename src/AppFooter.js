import React from 'react';
import { format } from 'date-fns';

const AppFooter = () => {

    return (
        // <div className="layout-footer">
        //     <span className="footer-text-left"><strong style={{color: "#8882bd"}}>Max Desk</strong> <small>Powered by</small> <a href="https://softwaregalaxy.co.tz" style={{color: "#39a3f4"}}>Software Galaxy</a></span>
        //     <span className="footer-text-right" style={{color: "#8882bd"}}>All Rights Reserved</span>
        // </div>
        <div className="layout-footer">
            <span className="footer-text-left" style={{color: "#8882bd"}}>&copy; {format(new Date(), "yyyy")}, Software Galaxy. All rights reserved.</span>
        </div>
    )

}

export default AppFooter;

import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';
import { ACCESS_TOKEN } from '../constants';

export const Welcome = () => {

    const history = useHistory();

    useEffect(() => {
        if (!localStorage.getItem(ACCESS_TOKEN)) {
            history.push("/login");
        }
        window.addEventListener("popstate", () => {
            history.go(1);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    // const resetPassword = () => {
    //     history.push("/reset/password");
    // }

    // const goToLogin = () => {
    //     history.push("/login");
    // }

	return (
        <div className="p-grid">
            <div className="p-grid p-col-12 p-text-center p-pb-6 p-pb-md-6 p-pb-lg-6 p-pt-6 p-pt-md-6 p-pt-lg-6">
                <div className="p-col-12 p-md-3 p-lg-3 p-text-center">
                    <img src="assets/layout/images/logo.png" alt="Logo" className="logo" style={{ width: "20%" }} />
                </div>
                <div className="p-col-12 p-md-9 p-lg-9 p-text-center">
                    <div className="p-col-12 p-md-offset-1 p-md-6 p-text-center">

                        <div className="p-col-12 p-md-offset-1 p-md-10 p-text-center">
                            <h3 className="p-mb-0">Welcome to maxxdesk</h3>
                            <span style={{fontSize: "large"}}>We recommend a quick setup before proceeding ahead</span>
                        </div>
                        <div className="p-col-12 p-md-offset-1 p-md-10 p-pl-md-6 p-pr-md-6 p-text-center p-pt-6 p-pt-md-6 p-pt-lg-6 p-mt-md-4 p-mt-lg-4">
                            <div className="p-ml-4 p-mr-4 div-rounded">
                                <Link to="/quick/slas" className="welcome-link">
                                    <div className="widget-pricing-box quick-setup" style={{backgroundColor: "#ffffff"}}>
                                        <span className="pricing-name" style={{color: "#8882bd"}}>Quick Setup</span>
                                        <div className="pricing-fee p-p-0 p-p-md-0 p-p-lg-0 p-text-center quick-setup-barge">
                                            <img src="assets/layout/images/quick_setup.png" alt="quick-setup" style={{ width: "90%", height: "auto", verticalAlign: "middle" }} />
                                        </div>
                                        <p style={{color: "#8882bd"}}>Lets get you started with Max Desk.</p>
                                    </div>
                                    {/* <div className="base-image">
                                        <img src="assets/layout/images/dashboard/graph-max-desk-1.png" alt="apollo-layout" style={{width: "100%", backgroundColor: "#e1e6e9"}} />
                                    </div> */}
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <div className="p-grid p-col-12 p-md-offset-3 p-md-9 p-text-center">
                <div className="p-col-12 p-md-offset-3 p-md-4 p-text-center">
                    <Link to="/quick/slas">
                        <Panel className="welcome-panel">
                            <div className="widget-pricing-box quick-setup" style={{backgroundColor: "#ffffff"}}>
                                <span className="pricing-name" style={{color: "#8882bd"}}>Quick Setup</span>
                                <div className="pricing-fee p-p-0 p-p-md-0 p-p-lg-0 p-text-center quick-setup-barge">
                                    <img src="assets/layout/images/quick_setup.png" className="quick-setup-image" alt="quick-setup" style={{ width: "90%", height: "auto", verticalAlign: "middle" }} />
                                </div>
                                <p style={{color: "#8882bd"}}>Lets get you started with Max Desk.</p>
                            </div>
                        </Panel>
                    </Link>
                </div>
            </div> */}
            {/* <div className="p-md-1 p-text-center">
                <div className="vertical-line p-text-center"></div>
            </div>
            <div className="p-col-12 p-md-5">
                <Link to="/congratulations">
                    <Panel className="welcome-panel">
                        <div className="widget-pricing-box">
                            <span className="pricing-name">Proceed Ahead</span>
                            <div className="pricing-fee" style={{backgroundColor: "#000000"}}><i className="pi pi-arrow-right" style={{fontSize: "100%"}}></i></div>
                            <p>Proceed ahead of the desk.</p>
                        </div>
                        <div className="base-image">
                            <img src="assets/layout/images/dashboard/graph-max-desk-2.png" alt="apollo-layout" style={{width: "100%", backgroundColor: "#e1e6e9"}} />
                        </div>
                    </Panel>
                </Link>
            </div> */}
            <div className="p-grid p-col-12 p-md-12 p-lg-12 layout-footer" style={{position: "absolute", bottom: "0"}}>
                <span className="p-text-center p-offset-4 p-col-4 p-md-offset-4 p-md-4 p-lg-offset-4 p-lg-4" style={{color: "#8882bd"}}><small>&copy; {format(new Date(), "yyyy")}, Software Galaxy. All rights reserved.</small></span>
            </div>
        </div>
	)
}

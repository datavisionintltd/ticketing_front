import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from "primereact/button";
import { Link, useHistory, useParams } from 'react-router-dom';
import AuthService from '../service/AuthService';
import { ResponseAlert } from '../utilities/components/ResponseAlert';

export const Activate = () => {

    const [userJson, setUserJson] = useState({});
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const authService = new AuthService();

    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault();
        authService.activateUserAccount(userJson)
        .then(response => {
            if (!response) {
                // eslint-disable-next-line no-throw-literal
                throw "Something went wrong"
            }
            if (response.status !== "success") {
                setFailed(true)
                setAlert("Failed to activate! Error:" + response.message);
            } else {
                setFailed(false);
                setAlert("You have your account successfully!");
                history.push('/users');
            }
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to activate! " + error);
        });
    }

    const goToLogin = () => {
        history.push("/login");
    }

    return (
        <div className="login-body" style={{ overflowX: "hidden" }}>
            {/* <div className="body-container"> */}
            <div className="p-grid">
                {/* <div className="p-col-12 p-lg-6 left-side" style={{ paddingTop: "14em" }}> */}
                <div className="p-col-12 p-lg-6 left-side p-pt-lg-6 p-pt-md-6 p-mt-lg-6 p-mt-md-6">
                    <div className="p-col-12 p-md-12 p-lg-12 p-text-center p-pt-lg-6 p-pt-md-6 p-mt-lg-6 p-mt-md-6" >
                        <img src="assets/layout/images/maxdesk_logo.png" alt="Max Desk" className="logo p-mt-lg-6 p-mt-md-6" style={{ padding: "0em", width: "15em" }} />
                    </div>
                    <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                        <h2>Welcome to Max Desk</h2>
                        <span>Sign up to submit your complaints</span>
                    </div>
                </div>
                <div className="p-col-12 p-lg-6 right-side p-pt-lg-6 p-mt-lg-6 p-pt-md-6 p-mt-md-6">
                    <div className="login-wrapper p-pt-lg-6 p-mt-lg-6 p-pt-md-6 p-mt-md-6">
                        <div className="login-container p-md-offset-1 p-md-10 p-pt-lg-6 p-mt-lg-6 p-pt-md-6 p-mt-md-6" style={{ border: "solid #8882bd", paddingTop: "2em", backgroundColor: "#ffffff" }}>
                            <span className="title">Login</span>
                            <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                            <form onSubmit={(e) => handleSubmit(e)}>
                                <div className="p-grid p-fluid">
                                    <div className="p-col-12">
                                        <InputText type="text" name="code" value={userJson.code || ''} onChange={e => setUserJson({ ...userJson, code: e.target.value })} required placeholder="Activation Code" />
                                    </div>
                                    <div className="p-col-12">
                                        <Button type="submit" label="Activate" icon="pi pi-check" autoFocus={true} />
                                    </div>
                                    <div className="p-col-8 p-offset-1">
                                        <Link to="#" onClick={()=>goToLogin()} className="p-button p-button-link">
                                            <i className="pi pi-fw pi-arrow-circle-left"></i>Login
                                        </Link>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {/* </div> */}
        </div>
    )
}

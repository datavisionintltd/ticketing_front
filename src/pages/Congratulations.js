import React, { useEffect } from 'react';
import { Button } from 'primereact/button';
import { format } from 'date-fns';
import { useHistory } from 'react-router';
import { ACCESS_TOKEN } from '../constants';

export const Congratulations = () => {

    const history = useHistory();

    useEffect(() => {
        if (!localStorage.getItem(ACCESS_TOKEN)) {
            history.push("/login");
        }
        window.addEventListener("popstate", () => {
            history.go(1);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const setPassword = () => {
        history.push("/set/password");
    }

    const askLater = () => {
        history.push("/");
    }

    return (
        <div className="p-grid">
            <div className="p-grid p-col-12 p-md-12 p-lg-12 p-text-left p-pt-6 p-pt-md-6 p-pt-lg-6" >
                <div className="p-col-12 p-md-3 p-lg-3 p-text-center">
                    <img src="assets/layout/images/logo.png" alt="Logo" className="logo" style={{ width: "20%" }} />
                </div>
            </div>
            <div className="p-col-12 p-md-12 p-lg-12 p-text-center">
                <img src="assets/layout/images/congratulations.png" alt="Max Desk" className="logo p-fluid" />
                <div className="p-col-12 p-text-center" style={{textAlign: "center"}}>
                    <h4><strong>Congratulations</strong></h4>
                    <h5>Your Account is Ready, Just set password to protect it</h5>
                </div>
                <div className="p-col-12 p-text-center">
                    <Button type="submit" onClick={() => setPassword(false)} label="Set Password" className="p-button-sm button-rounded" style={{ marginRight: '.50em' }} autoFocus={true} />
                    <Button type="button" onClick={() => askLater(false)} label="Ask me later" className="p-button-sm button-rounded" style={{ marginLeft: '.50em', backgroundColor: "#333333" }} />
                </div>
            </div>
            <div className="p-grid p-col-12 p-md-12 p-lg-12 p-text-center" style={{bottom: "0"}}>
                <span className="p-text-center p-offset-4 p-col-4 p-md-offset-4 p-md-4 p-lg-offset-4 p-lg-4" style={{color: "#8882bd"}}><small>&copy; {format(new Date(), "yyyy")}, Software Galaxy. All rights reserved.</small></span>
            </div>
        </div>
    )
}

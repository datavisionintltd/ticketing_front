import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from "primereact/button";
import { useHistory } from 'react-router-dom';
import AuthService from '../service/AuthService';
import { ACCESS_TOKEN, FULL_NAME, SECURED, ROLES, TENANT, TENANT_ID, USER_ID, UUID } from '../constants';
import UserService from '../service/UserService';
import { ResponseAlert } from '../utilities/components/ResponseAlert';
import { hasRole } from '../utilities/Helpers';

export const Login = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const authService = new AuthService();
    const userService = new UserService();

    const history = useHistory();

    const goDashboard = () => {
        // if (hasRole("ROLE_USER")) {
        //     history.push('/tickets')
        // } else {
        //     history.push('/');
        // }
        history.push('/');
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        authService.logIn(username, password)
            .then(response => {
                localStorage.setItem(ACCESS_TOKEN, response.access_token);
                setFailed(false);
                setAlert("You're successfully logged in!");
                getUserDetails();
            }).catch(error => {
                setFailed(true);
                setAlert("Invalid username or password!");
            });
    }

    const getUserDetails = () => {
        userService.getUserDetails()
            .then(response => {
                let fullName = response.fullName;
                if (fullName.replace(/\s+/g, '') === "") {
                    fullName = username;
                }
                localStorage.setItem(FULL_NAME, fullName);
                localStorage.setItem(TENANT_ID, response.tenant !== null ? response.tenant.id : 0);
                localStorage.setItem(UUID, response.tenant !== null ? response.tenant.uuid : 0);
                localStorage.setItem(ROLES, JSON.stringify(response.roles));
                localStorage.setItem(USER_ID, response.id);
                localStorage.setItem(TENANT, response.tenant !== null ? response.tenant.name : 'Software Galaxy');
                localStorage.setItem(SECURED, response.passwordProtected);
                goDashboard();
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to fetch user details");
            });
    }

    const forgotPasswordClick = () => {
        history.push("/forgot/password");
    }

    const signUpClick = () => {
        history.push("/register");
    }

	const goToGalaxy = () => {
		window.location.assign('https://softwaregalaxy.co.tz/_s/auth?callback=http://154.74.133.74:61616/galaxy/token&xc=555ec53e-3425-450d-950a-57621dbcf695');
	}

    return (
        <div className="login-body" style={{ overflowX: "hidden" }}>
            {/* <div className="body-container"> */}
            <div className="p-grid">
                <div className="p-col-12 p-lg-6 left-side p-pt-lg-6 p-pt-md-6 p-mt-lg-6 p-mt-md-6">
                    <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                        <img src="assets/layout/images/maxxdesk_logo.png" alt="Max Desk" className="logo p-mt-lg-6 p-mt-md-6" style={{ padding: "0em", width: "15em" }} />
                    </div>
                    { window.innerWidth >= 1025 &&
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center">
                            <img src="assets/layout/images/desk.jpg" alt="Max Desk" className="logo p-p-0" style={{ width: "70%" }} />
                        </div>
                    }
                    { window.innerWidth >= 1025 &&
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                            <span>Trusted by Industry Leading Companies</span>
                        </div>
                    }
                    { window.innerWidth >= 1025 &&
                        <div className="p-grid p-offset-2 p-col-8 p-md-offset-2 p-md-8 p-lg-offset-2 p-lg-8 p-text-center p-d-flex" >
                            <div className="p-col-4 p-md-4 p-lg-4">
                                <img src="assets/layout/images/dvi_logo.png" alt="Data Vision" className="logo p-p-0" style={{ width: "100%" }} />
                            </div>
                            <div className="p-col-4 p-md-4 p-lg-4">
                                <img src="assets/layout/images/mus_logo.png" alt="Data Vision" className="logo p-p-0" style={{ width: "70%" }} />
                            </div>
                            <div className="p-col-4 p-md-4 p-lg-4">
                                <img src="assets/layout/images/idcard_logo.png" alt="Data Vision" className="logo p-p-0" style={{ width: "80%" }} />
                            </div>
                        </div>
                    }
                </div>
                <div className="p-col-12 p-lg-6 right-side p-pt-lg-6 p-pt-md-6 p-mt-lg-6 p-mt-md-6">
                    <div className="login-wrapper">
                        <div className="login-container p-md-offset-1 p-md-10 p-pt-lg-6 p-pt-md-6 p-mt-lg-6 p-mt-md-6" style={{ border: "solid #8882bd", backgroundColor: "#ffffff" }}>
                            <span className="title">Login</span>
                            <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                            <form onSubmit={(e) => handleSubmit(e)}>
                                <div className="p-grid p-fluid">
                                    <div className="p-col-12">
                                        <InputText type="username" name="username" value={username} onChange={e => setUsername(e.target.value)} required placeholder="Username" />
                                    </div>
                                    <div className="p-col-12">
                                        <InputText type="password" name="password" value={password} onChange={e => setPassword(e.target.value)} required placeholder="Password" />
                                    </div>
                                    <div className="p-col-12">
                                        <Button type="submit" label="Sign In" icon="pi pi-check" autoFocus={true} />
                                    </div>
                                    <div className="p-col-12">
                                        <Button type="button" onClick={() => signUpClick()} label="Sign Up" icon="pi pi-user-plus" />
                                    </div>
                                    <div className="p-col-12">
                                        <button id="sg-signin-button" onClick={() => goToGalaxy()} style={{width: "100%"}}></button>
                                    </div>
                                    <div className="p-col-12 password-container">
                                        <button type="text" onClick={() => forgotPasswordClick()} className="p-link">Forgot Password?</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {/* </div> */}
        </div>
    )
}

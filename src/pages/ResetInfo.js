import React from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';

export const ResetInfo = () => {

    const history = useHistory();

    const resetPassword = () => {
        history.push("/reset/password");
    }

    const goToLogin = () => {
        history.push("/login");
    }

	return (
        <div className="p-grid" style={{height: "56.5em", backgroundColor: "#f5f5f5", backgroundPosition: "center", backgroundRepeat: "no-repeat", backgroundSize: "100% 100%", overflow: "hidden"}}>
            <div className="login-wrapper" style={{overflow: "hidden", width: "80%", marginLeft: "10%"}}>
                <div className="login-container" style={{border: "solid #8882BD", marginTop: "10em", paddingTop: "2em", backgroundColor: "#ffffff"}}>
                    <img src="assets/layout/images/maxdesk_logo.png" alt="max-desk" className="logo" style={{padding: "0px", width: "8em"}} />
                    <div className="p-message p-component p-message-success" style={{ display: 'block', margin: "2em" }}>
                        <div className="p-message-wrapper">
                            <div className="p-col-12 p-md-12 p-lg-12">
                                <span className="p-message-text">Please visit your email or your text messages to <Link to="#" onClick={resetPassword} style={{color: "#7E57C2", fontWeight: "bold"}}>rest password here</Link></span>
                            </div>
                            <div className="p-col-8 p-offset-1">
                                <Link to="#" onClick={()=>goToLogin()} className="p-button p-button-link">
                                    <i className="pi pi-fw pi-arrow-circle-left"></i>Login
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	)
}

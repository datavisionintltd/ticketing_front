import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from "primereact/button";
import { Dropdown } from 'primereact/dropdown';
import { Link, useHistory } from 'react-router-dom';
import { ACCESS_TOKEN, FULL_NAME, ROLES, SECURED, TENANT, TENANT_ID, USER_ID, UUID } from '../constants';
import AuthService from '../service/AuthService';
import UserService from '../service/UserService';
import { ResponseAlert } from '../utilities/components/ResponseAlert';
import axios from 'axios';

export const Register = () => {

    const [countryJson, setCountryJson] = useState({dialCode: 256, name: "USA"})
    const [countries, setCountries] = useState([]);
    const [adminJson, setAdminJson] = useState({});
    const [thisCountry, setThisCountry] = useState(true);
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const authService = new AuthService();
    const userService = new UserService();

    const history = useHistory();

    useEffect(() => {
        localStorage.clear();
        axios.get('https://ipapi.co/json/').then((response) => {
            let data = response.data;
            setCountryJson({
                dialCode: data.country_calling_code,
                name: data.country_name
            });
        });
        const countryParamsJson = { name: "", page: 0, size: 1000, sort: "id", order: "asc" };
        authService.getCountries(countryParamsJson)
        .then(data => {
            setCountries(data.content);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const submitCompany = (e) => {
        e.preventDefault();
        adminJson["dialCode"] = countryJson.dialCode;
        authService.registerCompany(adminJson).then(response => {
            localStorage.setItem(ACCESS_TOKEN, response.access_token);
            setFailed(false);
            setAlert("Your account is successfully registered!");
            getUserDetails();
        }).catch(error => {
            setFailed(true);
            if (error.message !== null) {
                setAlert(error.message);
            } else {
                setAlert("Failed to connect, Please check your internet");
            }
        });
    }

    const getUserDetails = () => {
        userService.getUserDetails()
            .then(response => {
                let fullName = response.fullName;
                if (fullName.replace(/\s+/g, '') === "") {
                    fullName = adminJson.email;
                }
                localStorage.setItem(FULL_NAME, fullName);
                localStorage.setItem(TENANT_ID, response.tenant !== null ? response.tenant.id : 0);
                localStorage.setItem(TENANT, response.tenant !== null ? response.tenant.name : 'Software Galaxy');
                localStorage.setItem(UUID, response.tenant !== null ? response.tenant.uuid : 0);
                localStorage.setItem(ROLES, JSON.stringify(response.roles));
                localStorage.setItem(USER_ID, response.id);
                localStorage.setItem(SECURED, response.passwordProtected);
                history.push('/welcome');
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to fetch user details");
            });
    }

    const goToLogin = () => {
        history.push("/login");
    }

    return (
        <div className="login-body" style={{ overflowX: "hidden" }}>
            <div className="p-grid">
                <div className="p-col-12 p-lg-6 left-side p-pt-lg-6 p-pt-md-6 p-mt-lg-6 p-mt-md-6">
                    <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                        <img src="assets/layout/images/maxxdesk_logo.png" alt="Max Desk" className="logo p-mt-lg-6 p-mt-md-6" style={{ padding: "0em", width: "15em" }} />
                    </div>
                    { window.innerWidth >= 1025 &&
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center">
                            <img src="assets/layout/images/desk.jpg" alt="Max Desk" className="logo p-p-0" style={{ width: "70%" }} />
                        </div>
                    }
                    { window.innerWidth >= 1025 &&
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                            <span>Trusted by Industry Leading Companies</span>
                        </div>
                    }
                    { window.innerWidth >= 1025 &&
                        <div className="p-grid p-offset-2 p-col-8 p-md-offset-2 p-md-8 p-lg-offset-2 p-lg-8 p-text-center p-d-flex" >
                            <div className="p-col-4 p-md-4 p-lg-4">
                                <img src="assets/layout/images/dvi_logo.png" alt="Data Vision" className="logo p-p-0" style={{ width: "100%" }} />
                            </div>
                            <div className="p-col-4 p-md-4 p-lg-4">
                                <img src="assets/layout/images/mus_logo.png" alt="Data Vision" className="logo p-p-0" style={{ width: "70%" }} />
                            </div>
                            <div className="p-col-4 p-md-4 p-lg-4">
                                <img src="assets/layout/images/idcard_logo.png" alt="Data Vision" className="logo p-p-0" style={{ width: "80%" }} />
                            </div>
                        </div>
                    }
                </div>
                <div className="p-col-12 p-lg-6 right-side p-pt-lg-6 p-pt-md-6 p-mt-lg-6 p-mt-md-6">
                    <div className="login-wrapper">
                        <div className="login-container p-md-offset-1 p-md-10 p-pt-lg-6 p-pt-md-6 p-mt-lg-6 p-mt-md-6" style={{ border: "solid #8882bd", backgroundColor: "#ffffff" }}>
                            <div className="p-grid">
                                <div className="p-col-12" style={{ padding: "2%" }}>
                                    <form onSubmit={submitCompany}>
                                        <span className="title">Get Started now</span>
                                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                                        <div className="p-grid">
                                            <div className="p-col-12">
                                                {/* <div className="p-fluid p-field p-formgrid p-grid">
                                                </div> */}
                                                <div className="p-fluid p-formgrid p-grid">
                                                    <div className="p-field p-col-12 p-md-12">
                                                        <span className="p-float-label">
                                                            <InputText type="text" name="fullName" value={adminJson.fullName || ''} onChange={e => setAdminJson({ ...adminJson, fullName: e.target.value })} required placeholder="Full Name" />
                                                            {/* <label htmlFor="fullName">Full Name</label> */}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="p-fluid p-formgrid p-grid">
                                                    <div className="p-field p-col-12 p-md-12">
                                                        <span className="p-float-label">
                                                            <InputText type="text" name="email" value={adminJson.email || ''} onChange={e => setAdminJson({ ...adminJson, email: e.target.value })} required placeholder="Email" />
                                                            {/* <label htmlFor="email">Email</label> */}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="p-fluid p-formgrid p-grid">
                                                    <div className="p-field p-col-12 p-md-12">
                                                        <span className="p-float-label">
                                                            <InputText type="" name="phoneNumber" value={adminJson.phoneNumber || ''} onChange={e => setAdminJson({ ...adminJson, phoneNumber: e.target.value })} required placeholder="Phone Number" />
                                                            {/* <label htmlFor="phoneNumber">Phone Number</label> */}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="p-fluid p-formgrid p-grid">
                                                    <div className="p-field p-col-12 p-md-12">
                                                        <span className="p-float-label">
                                                            <InputText type="text" name="name" value={adminJson.name || ''} onChange={e => setAdminJson({ ...adminJson, name: e.target.value })} required placeholder="Company Name" />
                                                            {/* <label htmlFor="name">Company Name</label> */}
                                                        </span>
                                                    </div>
                                                </div>
                                                {/* <div className="p-fluid p-formgrid p-grid">
                                                    <div className="p-field p-col-12 p-md-12">
                                                        <InputText type="password" name="password" value={adminJson.password || ''} onChange={e => setAdminJson({ ...adminJson, password: e.target.value })} required placeholder="Enter the password" />
                                                    </div>
                                                </div>
                                                <div className="p-fluid p-formgrid p-grid">
                                                    <div className="p-field p-col-12 p-md-12">
                                                        <InputText type="password" name="confirm" value={adminJson.confirm || ''} onChange={e => setAdminJson({ ...adminJson, confirm: e.target.value })} required placeholder="Confirm the Password" />
                                                    </div>
                                                </div> */}
                                                <div className="p-fluid p-formgrid p-grid">
                                                    <div className="p-field p-col-12 p-md-12">
                                                        { thisCountry &&
                                                            <small>
                                                                If your company is based outside { countryJson.name },&nbsp;
                                                                <Link to="#" type="button" onClick={() => setThisCountry(false)} style={{color: "#000000", textDecoration: "underline", textDecorationStyle: "solid"}}>Change Country</Link>
                                                            </small>
                                                        }
                                                        { !thisCountry &&
                                                            <Dropdown id="state" value={countryJson.dialCode} onChange={(e) => setCountryJson({...countryJson, dialCode: e.value})} options={countries} optionLabel="name" optionValue="dialCode" filter filterBy="name" placeholder="Select Country"></Dropdown>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-4">
                                                <Link to="#" onClick={()=>goToLogin()} className="p-button p-button-link">
                                                    <i className="pi pi-fw pi-arrow-circle-left"></i>Login
                                                </Link>
                                            </div>
                                            <div className="p-field p-col-12 p-md-4">
                                                <Button type="submit" label="Next" icon="pi pi-check" autoFocus={true} />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}

import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from "primereact/button";
import { Link, useHistory } from 'react-router-dom';
import AuthService from '../service/AuthService';
import { ResponseAlert } from '../utilities/components/ResponseAlert';

export const ForgotPassword = () => {

    const [email, setEmail] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const authService = new AuthService();

	const history = useHistory();

	const goDashboard = () => {
		history.push('/reset/info');
	}

    const handleSubmit = (e) => {
        e.preventDefault();
        const resetJson = {
            email: email
        }
        authService.passwordResetRequest(resetJson)
        .then(response => {
			setFailed(false);
			setAlert(response.message);
            goDashboard();
        }).catch(error => {
            setFailed(true);
			setAlert(error.message);
        });
	}

    const goToLogin = () => {
        history.push("/login");
    }

	return (
        <div className="body-container" style={{background: "url(assets/layout/images/galaxy.png)", backgroundPosition: "center", backgroundRepeat: "no-repeat", backgroundSize: "100% 100%" , overflowX: "hidden" }}>
        <div className="p-grid" style={{height: "48em", overflow: "hidden"}}>
            <div className="login-wrapper" style={{overflow: "hidden"}}>
                <div className="login-container" style={{border: "solid #8882BD", marginTop: "10em", paddingTop: "2em", backgroundColor: "#ffffff"}}>
                    <img src="assets/layout/images/maxdesk_logo.png" alt="max-desk" className="logo" style={{padding: "0px", width: "8em"}} />
                    <span className="title p-text-center" style={{color: "#00a14b"}}>Enter email to request password reset</span>
                    <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                    <form onSubmit={(e) => handleSubmit(e)}>
                        <div className="p-grid p-fluid">
                            <div className="p-col-8 p-offset-2">
                                <span className="p-float-label">
                                    <InputText type="email" name="email" value={email} onChange={e => setEmail(e.target.value)} required placeholder="Email" />
                                    <label htmlFor="email">Email</label>
                                </span>
                            </div>
                            <div className="p-col-8 p-offset-2">
                                <Button type="submit" label="Send" icon="pi pi-check" autoFocus={true}/>
                            </div>
                            <div className="p-col-8 p-offset-1">
                                <Link to="#" onClick={()=>goToLogin()} className="p-button p-button-link">
                                    <i className="pi pi-fw pi-arrow-circle-left"></i>Login
                                </Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
	)
}

import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { format } from 'date-fns';
import UserService from '../service/UserService';
import { ResponseAlert } from '../utilities/components/ResponseAlert';
import { useHistory } from 'react-router';
import { ACCESS_TOKEN } from '../constants';

export const SetPassword = () => {

    const [password, setPassword] = useState("");
    const [confirm, setConfirm] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const userService = new UserService();

    const history = useHistory();

    useEffect(() => {
        if (!localStorage.getItem(ACCESS_TOKEN)) {
            history.push("/login");
        }
        window.addEventListener("popstate", () => {
            history.go(1);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitUser = (e) => {
        e.preventDefault();
        const userJson = {
            password: password,
            confirm: confirm
        };
        if(password === confirm) {
            userService.resetPassword(userJson)
            .then(response => {
                setFailed(false);
                setAlert("Password Added successfully!");
                history.push("/");
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to reset password!");
            });
        } else {
            setFailed(true);
            setAlert("Password does not match!");
        }
    }

    return (
        <div className="p-grid">
            <div className="p-grid p-col-12 p-md-12 p-lg-12 p-text-left p-pt-6 p-pt-md-6 p-pt-lg-6" >
                <div className="p-col-12 p-md-3 p-lg-3 p-text-center">
                    <img src="assets/layout/images/logo.png" alt="Logo" className="logo" style={{ width: "20%" }} />
                </div>
            </div>
            <div className="p-col-12 p-md-offset-3 p-md-6 p-lg-offset-3 p-lg-6">
                <h3 style={{textAlign: "center", padding: "0.5em"}}>Set password to protect your account</h3>
                <form onSubmit={submitUser}>
                    <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                    <div className="p-fluid p-formgrid p-grid p-col-12 p-pt-6 p-pt-md-6 p-pt-lg-6">
                        <div className="p-grid p-col-12 p-md-offset-2 p-md-8">
                            <div className="p-field p-col-12 p-md-6">
                                <label htmlFor="password"></label>
                                <InputText type="password" name="password" value={password} onChange={e => setPassword(e.target.value)} required placeholder="Enter Password" />
                            </div>
                            <div className="p-field p-col-12 p-md-6">
                                <label htmlFor="confirm"></label>
                                <InputText type="password" name="confirm" value={confirm} onChange={e => setConfirm(e.target.value)} required placeholder="Confirm Password" />
                            </div>
                        </div>
                        {/* <div className="p-col-12 p-md-offset-4 p-md-4">
                            <Button type="submit" label="Save" icon="pi pi-check" autoFocus={true}/>
                        </div> */}
                    </div>
                    <div className="p-col-12 p-text-center p-pt-5 p-pt-md-5 p-pt-lg-5">
                        <Button type="submit" label="Save" className="p-button-sm button-rounded p-col-2 p-md-2 p-lg-2" style={{ marginRight: '.25em', backgroundColor: "#333333" }} autoFocus={true} />
                    </div>
                </form>
            </div>
            <div className="p-grid p-col-12 p-md-12 p-lg-12 p-text-center" style={{position: "absolute", bottom: "0"}}>
                <span className="p-text-center p-offset-4 p-col-4 p-md-offset-4 p-md-4 p-lg-offset-4 p-lg-4" style={{color: "#8882bd"}}><small>&copy; {format(new Date(), "yyyy")}, Software Galaxy. All rights reserved.</small></span>
            </div>
        </div>
    )
}

import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from "primereact/button";
import { Link, useHistory, useParams } from 'react-router-dom';
import AuthService from '../service/AuthService';
import { ResponseAlert } from '../utilities/components/ResponseAlert';

export const SignUp = () => {

    const { uuid } = useParams();
    const [userJson, setUserJson] = useState({});
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const authService = new AuthService();

    const history = useHistory();

    useEffect(() => {
        setUserJson({...userJson, uuid: uuid});
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleSubmit = (e) => {
        e.preventDefault();
        if (userJson.password === userJson.confirm) {
            authService.signUp(userJson)
            .then(response => {
                if (!response) {
                    // eslint-disable-next-line no-throw-literal
                    throw "Something went wrong"
                }
                if (response.status !== "success") {
                    setFailed(true)
                    setAlert("Failed to sign up! Error:" + response.message);
                } else {
                    setFailed(false);
                    setAlert("You have signed up successfully!");
                    history.push('/activate');
                }
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to signup! " + error);
            });
        } else {
            setFailed(true);
            setAlert("Password does not match!");
        }
    }

    const goToLogin = () => {
        history.push("/login");
    }

    return (
        <div className="login-body" style={{ overflowX: "hidden" }}>
            {/* <div className="body-container"> */}
            <div className="p-grid">
                {/* <div className="p-col-12 p-lg-6 left-side" style={{ paddingTop: "14em" }}> */}
                <div className="p-col-12 p-lg-6 left-side p-pt-lg-6 p-pt-md-6 p-mt-lg-6 p-mt-md-6">
                    <div className="p-col-12 p-md-12 p-lg-12 p-text-center p-pt-lg-6 p-pt-md-6 p-mt-lg-6 p-mt-md-6" >
                        <img src="assets/layout/images/maxdesk_logo.png" alt="Max Desk" className="logo p-mt-lg-6 p-mt-md-6" style={{ padding: "0em", width: "15em" }} />
                    </div>
                    <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                        <h2>Welcome to Max Desk</h2>
                        <span>Sign up to submit your complaints</span>
                    </div>
                </div>
                <div className="p-col-12 p-lg-6 right-side p-pt-lg-6 p-mt-lg-6 p-pt-md-6 p-mt-md-6">
                    <div className="login-wrapper">
                        <div className="login-container p-md-offset-1 p-md-10 p-pt-lg-6 p-mt-lg-6 p-pt-md-6 p-mt-md-6" style={{ border: "solid #8882bd", paddingTop: "2em", backgroundColor: "#ffffff" }}>
                            <span className="title">Login</span>
                            <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                            <form onSubmit={(e) => handleSubmit(e)}>
                                <div className="p-grid p-fluid">
                                    {/* <div className="p-col-12">
                                        <InputText type="username" name="username" value={userJson.username || ''} onChange={e => setUserJson({ ...userJson, username: e.target.value })} required placeholder="Username" />
                                    </div> */}
                                    <div className="p-col-12">
                                        <InputText type="text" name="username" value={userJson.username || ''} onChange={e => setUserJson({ ...userJson, username: e.target.value })} required placeholder="Enter Email" />
                                    </div>
                                    <div className="p-col-12">
                                        <InputText type="password" name="password" value={userJson.password || ''} onChange={e => setUserJson({ ...userJson, password: e.target.value })} required placeholder="Password" />
                                    </div>
                                    <div className="p-col-12">
                                        <InputText type="password" name="confirm" value={userJson.confirm || ''} onChange={e => setUserJson({ ...userJson, confirm: e.target.value })} required placeholder="Confirm Password" />
                                    </div>
                                    <div className="p-col-12">
                                        <InputText type="text" name="fullName" value={userJson.fullName || ''} onChange={e => setUserJson({ ...userJson, fullName: e.target.value })} required placeholder="Enter Full Name" />
                                    </div>
                                    <div className="p-col-12">
                                        <InputText type="text" name="phoneNumber" value={userJson.phoneNumber || ''} onChange={e => setUserJson({ ...userJson, phoneNumber: e.target.value })} required placeholder="Enter Phone Number" />
                                    </div>
                                    <div className="p-col-12">
                                        <Button type="submit" label="Sign Up" icon="pi pi-check" autoFocus={true} />
                                    </div>
                                    <div className="p-col-8 p-offset-1">
                                        <Link to="#" onClick={()=>goToLogin()} className="p-button p-button-link">
                                            <i className="pi pi-fw pi-arrow-circle-left"></i>Login
                                        </Link>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {/* </div> */}
        </div>
    )
}

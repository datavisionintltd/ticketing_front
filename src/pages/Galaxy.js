import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { ACCESS_TOKEN, FULL_NAME, SECURED, ROLES, TENANT, TENANT_ID, USER_ID, UUID } from '../constants';
import UserService from '../service/UserService';
import { ResponseAlert } from '../utilities/components/ResponseAlert';
import { hasRole } from '../utilities/Helpers';

export const Galaxy = () => {

    const { token } = useParams();
    const userService = new UserService();

    const history = useHistory();

    const goDashboard = () => {
        // if (hasRole("ROLE_USER")) {
        //     history.push('/tickets')
        // } else {
        //     history.push('/');
        // }
        history.push('/');
    }

    useEffect(() => {
        storeToken();
    }, []);

    const storeToken = () => {
        if(token !== "none" && token !== "") {
            localStorage.setItem(ACCESS_TOKEN, token);
			getUserDetails();
        }
    }

    const getUserDetails = () => {
        userService.getUserDetails()
            .then(response => {
                localStorage.setItem(FULL_NAME, response.fullName);
                localStorage.setItem(TENANT_ID, response.tenant !== null ? response.tenant.id : 0);
                localStorage.setItem(UUID, response.tenant !== null ? response.tenant.uuid : 0);
                localStorage.setItem(ROLES, JSON.stringify(response.roles));
                localStorage.setItem(USER_ID, response.id);
                localStorage.setItem(TENANT, response.tenant !== null ? response.tenant.name : 'Software Galaxy');
                localStorage.setItem(SECURED, response.passwordProtected);
                goDashboard();
            }).catch(error => {
                history.push("/login");
            });
    }

    return (
        <div className="p-grid dashboard">
        <div className="p-grid p-col-12 p-text-center p-pb-6 p-pb-md-6 p-pb-lg-6 p-pt-6 p-pt-md-6 p-pt-lg-6">
            <div className="p-col-12 p-md-offset-4 p-md-4 p-text-center">
                <h3>Welcome to Max Desk</h3>
            </div>
            <div className="p-col-12 p-md-offset-4 p-md-4 p-text-center">
                <img src="assets/layout/images/logo.png" alt="Logo" className="logo" style={{ width: "20%" }} />
            </div>
            <div className="p-col-12 p-md-offset-4 p-md-4 p-text-center">
                <h1>Loading...</h1>
                <p>
                    System is assesing your privileges.
                </p>
            </div>
        </div>
    </div>
    )
}

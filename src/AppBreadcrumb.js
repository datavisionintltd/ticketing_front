import React, { useState } from 'react';
import { useHistory, useLocation, withRouter } from 'react-router-dom';
import AuthService from './service/AuthService';

const AppBreadcrumb = () => {

    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const authService = new AuthService();

    const location = useLocation();
    const history = useHistory();

    const paths = location.pathname.split('/');

    const logOut = () => {
        authService.signOut()
        .then(response => {
            localStorage.clear();
			setFailed(false);
			setAlert("You're successfully logged out!");
            history.push('/');
        }).catch(error => {
            localStorage.clear();
			setFailed(false);
			setAlert("You're successfully logged out!");
            history.push('/');
        });
    }

    const listTicket = () => {
        history.push("/tickets");
    }

    return (
        <div className="layout-breadcrumb">
            <ul>
                <li><button type="button" className="p-link" onClick={() => history.push('/')}><i className="pi pi-home"></i></button></li>
                {
                    location.pathname === '/' ? <li>/</li> : paths.map((path, index) => <li key={index}>{path === '' ? '/' : path}</li>)
                }
            </ul>

            <div className="layout-breadcrumb-options">
                {/* <button type="button" className="p-link" title="Tickets" onClick={() => listTicket()}>
                    <div className=""><i className="pi pi-bookmark"></i> Tickets</div>
                </button> */}
                <button type="button" className="p-link" title="Backup">
                    <i className="pi pi-cloud-upload"></i>
                </button>
                <button type="button" className="p-link" title="Bookmark">
                    <i className="pi pi-bookmark"></i>
                </button>
                <button type="button" className="p-link" onClick={() => logOut()} title="Logout">
                    <i className="pi pi-power-off" style={{color: "#E00000"}}></i>
                </button>
            </div>
        </div>
    );

}

export default withRouter(AppBreadcrumb);

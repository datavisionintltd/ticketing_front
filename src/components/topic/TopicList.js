import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import TopicService from '../../service/TopicService';
import { Link } from 'react-router-dom';
import { Sidebar } from 'primereact/sidebar';
import { Dialog } from 'primereact/dialog';
import { SplitButton } from 'primereact/splitbutton';
import { Redirect, Route } from 'react-router-dom';
import { Calendar } from 'primereact/calendar';
import { InputTextarea } from 'primereact/inputtextarea';
import { InputNumber } from 'primereact/inputnumber';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';

export const TopicList = () => {

    const [topic, setTopic] = useState(null);
    const [selectedUser, setSelectedUser] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);

    const [visibleRight, setVisibleRight] = useState(false);
    const [id, setId] = useState("");
    const [name, setName] = useState("");
	const [code, setCode] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [submitted, setSubmitted] = useState(false);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteTopic, setDeleteDictionary] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [first, setFirst] = useState(0);

	const [gracePeriod, setGracePeriod] = useState("");
    const [description, setDescription] = useState("");
    const [dueDate, setCalendarValue] = useState(null);



    const topicService = new TopicService();

    useEffect(() => {
        const paramsJson = { name: globalFilter, page: pageIndex, size: pageSize, sort: sortField, order: (sortOrder === 1 ? 'asc' : 'desc') };
        topicService.getTopics(paramsJson).then(data => {
            setId(data.id);
            setTopic(data.content);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
            //setDepartmentId(0);
            setLoading(false);
        })
        .catch(error => {
            setTopic([]);
            setLoading(false);
            setAlert("Failed to update topic! " + error.error_description);
        });
    }, [submitted,pageIndex, sortField, sortOrder, first, pageSize, globalFilter]);

    const submitTopic = (e) => {
        e.preventDefault();
        const topicJson = {
            id:id,
            name: name,
            description: description,
            dueDate:dueDate,
            gracePeriod:gracePeriod
        };
        topicService.editTopic(topicJson)
        .then(response => {
            setFailed(false);
            setAlert("Topic Updated successfully!");
            setSubmitted(true);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update topic! " + error.error_description);
        });
    }
    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }
    const removeTopic=(e)=>{
        e.preventDefault();
        setDisplayConfirmation(false);
        topicService.deleteTopic(id)
        .then(response => {
            setFailed(false);
            setAlert("Topic Deleted successfully!");
            setSubmitted(true);
            setTopic(response)
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete topic! ");
        });
    }

    const onDeleteButtonClick = (data) => {
        setDeleteDictionary(data.name);
        setDisplayConfirmation(true);
        setId(data.id);

    }

    const onEditButtonClick = (data) => {
        setId(data.id);
        setName(data.name);
        setDescription(data.description);
        setVisibleRight(true);
    }

    const topicTableHeader = (
        <div className="table-header">
            Categories
            <div className="p-grid">
                <div className="p-col-6 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-6 p-md-6">
                <Link to="topic/create" style={{float: "right"}}><Button  style={{float: "right"}}  label="Add Category" icon="pi pi-unlock"/></Link>
                </div>
            </div>
        </div>
    );

    const userBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">name</span>
                <img alt={data.name} src={`assets/layout/images/avatar/user.png`} width="32" style={{ verticalAlign: 'middle' }} />
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.name}</span>
            </>
        );
    };
    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-pencil" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="Edit Category" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} tooltip="Delete Category"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeTopic(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">
                    <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                    {/* LIST ROLES DATATABLE */}
                    <DataTable value={topic} paginator={false} className="p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedUser} onSelectionChange={(e) => setSelectedUser(e.value)}
                        responsive={true} globalFilter={globalFilter} emptyMessage="No Topics found." loading={loading} header={topicTableHeader}>
                        <Column field="name" header="Category" sortable body={bodyTemplate}></Column>
                        <Column field="serviceLevelAgreement.name" header="Priority" sortable></Column>
                        <Column field="department.name" header="Department" sortable></Column>
                        <Column header="Actions" headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>
                    <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                        first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                    </Paginator>
                    {/* EDIT DICTIONARY RIGHT SIDE FORM */}
                    <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                        <form onSubmit={submitTopic}>
                            <div className="p-grid m3">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit Category</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Name</label>
                                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter name" />
                                            </div>
                                        </div>

                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="code">Description</label>
                                                <InputTextarea name="description" value={description} onChange={ev => setDescription(ev.target.value)} placeholder="Enter Description" autoResize rows="3" cols="30" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE USER CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteTopic}?</span>
                        </div>
                    </Dialog>





                </div>
            </div>
        </div>
    )
}

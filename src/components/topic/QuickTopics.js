import React, { useState, useEffect } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import TopicService from '../../service/TopicService';
import DictionaryService from '../../service/DictionaryService';
import { useHistory } from 'react-router';
import { Dropdown } from 'primereact/dropdown';
import { format } from 'date-fns';
import UserService from '../../service/UserService';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import { ACCESS_TOKEN } from '../../constants';


export const QuickTopics = () => {

    const history = useHistory();

    const [topics, setTopics] = useState([{departmentId: 0, serviceLevelAgreementId: 0, name: "", description: "N/A"}]);
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const topicService = new TopicService();
    const dictionaryService = new DictionaryService();
    const [departmentValues, setDepartmentValues] = useState([]);
    const [slas, setSlas] = useState([]);

    const userService = new UserService();

    useEffect(() => {
        if (!localStorage.getItem(ACCESS_TOKEN)) {
            history.push("/login");
        }
        const paramsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        userService.getDepartments(paramsJson)
        .then(data => {
            setDepartmentValues(data.content);
        });
        dictionaryService.getSla(paramsJson)
        .then(data => {
            setSlas(data.content);
        });
        window.addEventListener("popstate", () => {
            history.go(1);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitTopic = (e) => {
        e.preventDefault();
        topicService.addTopics(topics)
        .then(response => {
            setFailed(false);
            setAlert("Topics added successfully!");
            history.push('/congratulations');
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add Topic! " + error.error_description);
        });
    }

    const handleTopics = (name, value, index) => {
        // console.log(name + ": " + value);
        const items = [...topics];
        items[index][name] = value;
        setTopics(items);
    }

    const addTopic = () => {
        setTopics(prevTopics => [...prevTopics, {departmentId: 0, serviceLevelAgreementId: 0, name: "", description: ""}]);
    }

    const removeTopic = (index) => {
        if (topics.length > 1) {
            setTopics(topics.filter((item, i) => i !== index));
        }
    }

    return (
        <div className="p-grid">
            <div className="p-grid p-col-12 p-md-12 p-lg-12 p-text-left p-pt-6 p-pt-md-6 p-pt-lg-6" >
                <div className="p-col-12 p-md-3 p-lg-3 p-text-center">
                    <img src="assets/layout/images/logo.png" alt="Logo" className="logo" style={{ width: "20%" }} />
                </div>
            </div>
            <div className="p-col-12 p-md-offset-3 p-md-6 p-lg-offset-3 p-lg-6">
                <h3 style={{textAlign: "center", padding: "0.5em"}}>In general, what are the common complaints you usually deal with?</h3>
                <form onSubmit={submitTopic}>
                    <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                    { topics.map((item, index) => (
                        <div className="p-fluid p-formgrid p-grid p-col-12 p-pb-3" key={index}>
                            <div className="p-field p-col-12 p-md-4">
                                <InputText type="text" data-id={index} name="name" value={item.name} onChange={e => handleTopics(e.target.name, e.target.value, index)} required placeholder="Enter Issue" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <Dropdown data-id={index} name="departmentId" value={item.departmentId} onChange={e => handleTopics(e.target.name, e.value, index)} options={departmentValues} optionLabel="name" optionValue="id" placeholder="Select Department" />
                            </div>
                            { topics.length === 1 &&
                                <div className="p-field p-col-12 p-md-4">
                                    <Dropdown data-id={index} name="serviceLevelAgreementId"  value={item.serviceLevelAgreementId} onChange={e => handleTopics(e.target.name, e.value, index)} options={slas} optionLabel="name" optionValue="id" placeholder="Select Priority" />
                                </div>
                            }
                            { topics.length > 1 &&
                                <div className="p-field p-col-12 p-md-3">
                                    <Dropdown data-id={index} name="serviceLevelAgreementId"  value={item.serviceLevelAgreementId} onChange={e => handleTopics(e.target.name, e.value, index)} options={slas} optionLabel="name" optionValue="id" placeholder="Select Priority" />
                                </div>
                            }
                            { topics.length > 1 &&
                                <div className="p-col-12 p-md-1">
                                    <Button type="button" icon="pi pi-minus" onClick={() => removeTopic(index)}/>
                                </div>
                            }
                        </div>
                    ))}
                    <div className="p-fluid p-formgrid p-grid p-col-12">
                        <div className="p-col-12 p-md-4">
                            <Button type="button" icon="pi pi-plus" label="Add another issue" onClick={addTopic}/>
                        </div>
                    </div>
                    {/* <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-offset-4 p-md-4">
                            <label htmlFor="submit">&nbsp;</label>
                            <Button type="submit" label="Save" icon="pi pi-check" />
                        </div>
                    </div> */}
                    <div className="p-col-12 p-text-center p-pt-5 p-pt-md-5 p-pt-lg-5">
                        <Button type="submit" label="Save" className="p-button-sm button-rounded p-col-2 p-md-2 p-lg-2" style={{ marginRight: '.25em', backgroundColor: "#333333" }} autoFocus={true} />
                    </div>
                </form>
            </div>
            <div className="p-grid p-col-12 p-md-12 p-lg-12 p-text-center" style={{position: "absolute", bottom: "0"}}>
                <span className="p-text-center p-offset-4 p-col-4 p-md-offset-4 p-md-4 p-lg-offset-4 p-lg-4" style={{color: "#8882bd"}}><small>&copy; {format(new Date(), "yyyy")}, Software Galaxy. All rights reserved.</small></span>
            </div>
        </div>
    )
}

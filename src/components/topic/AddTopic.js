import React, { useState, useEffect } from 'react';
import { InputText } from 'primereact/inputtext';
import { Calendar } from 'primereact/calendar';
import { InputTextarea } from 'primereact/inputtextarea';
import { InputNumber } from 'primereact/inputnumber';
import { Button } from 'primereact/button';
import DictionaryService from '../../service/DictionaryService';
import { useHistory } from 'react-router';
import { Dropdown } from 'primereact/dropdown';
import UserService from '../../service/UserService';
import TopicService from '../../service/TopicService';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';


export const AddTopic = () => {

    const history = useHistory();

    const [name, setName] = useState("");
    const [gracePeriod, setGracePeriod] = useState("");
    const [alert, setAlert] = useState("");
    const [description, setDescription] = useState("");
    const [failed, setFailed] = useState(null);
    const [dueDate, setCalendarValue] = useState(null);
    const [departmentValue, setDepartmentValue] = useState(0);
    const [departmentValues, setDepartmentValues] = useState([]);
    const [slaId, setSlaId] = useState(0);
    const [slas, setSlas] = useState([]);
    const [add, setAdd] = useState(false);
    const [list, setList] = useState(false);

    const dictionaryService = new DictionaryService();
    const userService = new UserService();
    const topicService = new TopicService();

    useEffect(() => {
        const paramsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        userService.getDepartments(paramsJson)
        .then(data => {
            setDepartmentValues(data.content);
        });
        dictionaryService.getSla(paramsJson)
        .then(data => {
            setSlas(data.content);
        });
    }, [])

    const submitTopic = (e) => {
        e.preventDefault();
        const dictionaryJson = {
            department: {id: departmentValue},
            serviceLevelAgreement: {id: slaId},
            name: name,
            description: description
        };
        topicService.addTopic(dictionaryJson)
            .then(response => {
                setFailed(false);
                setAlert("Topic added successfully!");
                if (!add) {
                    setName("");
                    setDescription("");
                    //history.push('/settings/topic/create');
                }
                if (list) {
                    history.push('/settings/topics');
                }
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to add Topic! " + error.error_description);
            });
    }

    return (
        <div className="p-grid">
            <div className="p-col-12">
                <form onSubmit={submitTopic}>
                    <div className="card">
                        <div className="p-card-title" style={{backgroundColor: "#8882BD", color: "#FFFFFF"}}>
                            <h5 style={{textAlign: "center", padding: "0.5em"}}>New Category</h5>
                        </div>
                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="name">Category</label>
                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter Name" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="department">Department</label>
                                <Dropdown value={departmentValue} onChange={(e) => setDepartmentValue(e.value)} options={departmentValues} optionLabel="name" optionValue="id" placeholder="Select Department" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="sla">Priority</label>
                                <Dropdown value={slaId} onChange={(e) => setSlaId(e.value)} options={slas} optionLabel="name" optionValue="id" placeholder="Select Priority" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-12">
                                <label htmlFor="code">Description</label>
                                <InputTextarea name="description" value={description} onChange={ev => setDescription(ev.target.value)} placeholder="Enter Description" autoResize rows="3" cols="30" />

                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-3 p-md-3">
                                <label htmlFor="submit">&nbsp;</label>
                                <Button type="submit" label="Save" icon="pi pi-check" onClick={() => (setList(true))} autoFocus={true} />
                            </div>
                            <div className="p-field p-col-12 p-md-3">
                                <label htmlFor="submit">&nbsp;</label>
                                <Button type="submit" label="Save and insert next" icon="pi pi-check" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

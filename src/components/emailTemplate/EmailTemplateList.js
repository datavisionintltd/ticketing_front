import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import DictionaryService from '../../service/DictionaryService';
import { Link } from 'react-router-dom';
import { Sidebar } from 'primereact/sidebar';
import { Dialog } from 'primereact/dialog';
import { SplitButton } from 'primereact/splitbutton';
import { Redirect, Route } from 'react-router-dom';
import { Calendar } from 'primereact/calendar';
import { InputTextarea } from 'primereact/inputtextarea';
import { InputNumber } from 'primereact/inputnumber';
// import { Editor } from "react-draft-wysiwyg";
// import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

export const EmailTemplateList = () => {

    const [sla, setSla] = useState(null);
    const [selectedUser, setSelectedUser] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);

    const [visibleRight, setVisibleRight] = useState(false);
    const [id, setId] = useState("");
    const [name, setName] = useState("");
	const [code, setCode] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [submitted, setSubmitted] = useState(false);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteSla, setDeleteDictionary] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [first, setFirst] = useState(0);

	const [gracePeriod, setGracePeriod] = useState("");
    const [template, setTemplate] = useState("");



    const dicctionaryService = new DictionaryService();

    useEffect(() => {
        const paramsJson = { name: "name=" + globalFilter, page: "&page=" + pageIndex, size: "&size=" + pageSize, sort: "&sort=" + sortField, order: "&order=" + (sortOrder === 1 ? 'asc' : 'desc') };
        dicctionaryService.getEmailTemplate(paramsJson).then(data => {
            setId(data.id);
            setSla(data.content);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
            //setDepartmentId(0);
            setLoading(false);
        })
        .catch(error => {
            setSla([]);
            setLoading(false);
            setAlert("Failed to update sla! " + error.error_description);
        });
    }, [submitted,pageIndex, sortField, sortOrder, first, pageSize, globalFilter]);

    const submitTemplate = (e) => {
        e.preventDefault();
        const slaJson = {
            id:id,
            name: name,
            template:template
        };
        dicctionaryService.editTemplate(slaJson)
        .then(response => {
            setFailed(false);
            setAlert("sla Updated successfully!");
            setSubmitted(true);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update sla! " + error.error_description);
        });
    }
    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }
    const removeTemplate=(e)=>{
        e.preventDefault();
        setDisplayConfirmation(false);
        dicctionaryService.deleteTemplate(id)
        .then(response => {
            setFailed(false);
            setAlert("sla Deleted successfully!");
            setSubmitted(true);
            setSla(response)
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete sla! " + error.error_description);
        });
    }
    const onDeleteButtonClick = (data) => {
        setDeleteDictionary(data.name);
        setDisplayConfirmation(true);
        setId(data.id);
    }
    const onViewItemDetails = (id) => {
        dicctionaryService.viewItems(id)
        .then(response => {
            setFailed(false);
            setSubmitted(true);
            setSla(response)
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to Fetch sla! " +id+error.error_description);
        });

    }
    const onEditButtonClick = (data) => {
        setId(data.id);
        setName(data.name);
        setTemplate(data.template);
        setVisibleRight(true);
    }

    const slaTableHeader = (
        <div className="table-header">
            Email Template List
            <div className="p-grid">
                <div className="p-col-6 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>

            </div>
        </div>
    );

    const userBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">name</span>
                <img alt={data.name} src={`assets/layout/images/avatar/user.png`} width="32" style={{ verticalAlign: 'middle' }} />
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.name}</span>
            </>
        );
    };
    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };
    const items = [
        {
            label: 'View',
            icon: 'pi pi-refresh'
        },
        {
            label: 'Edit',
            icon: 'pi pi-times'
        },
        {
            label: 'Delete',
            icon: 'pi pi-home'
        }
    ];
    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-pencil" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="Edit SLA" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} tooltip="Delete SLA"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeTemplate(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">
                    { failed === false &&
                        <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                <span className="p-message-text">{alert}.</span>
                            </div>
                        </div>
                    }
                    { failed === true &&
                        <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                <span className="p-message-text">{alert}.</span>
                            </div>
                        </div>
                    }

                    {/* LIST ROLES DATATABLE */}
                    <DataTable value={sla} paginator={false} className="p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedUser} onSelectionChange={(e) => setSelectedUser(e.value)}
                        responsive={true} globalFilter={globalFilter} emptyMessage="No Slas found." loading={loading} header={slaTableHeader}>
                        <Column field="name" header="Name" sortable body={bodyTemplate}></Column>
                        <Column field="code" header="Code" sortable body={bodyTemplate}></Column>
                        <Column field="template" header="Tempalte " sortable body={bodyTemplate}></Column>
                        <Column header="Actions" headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>
                    <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                        first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                    </Paginator>
                    {/* EDIT DICTIONARY RIGHT SIDE FORM */}
                    <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                        <form onSubmit={submitTemplate}>
                            <div className="p-grid m3">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit Email Tempalte</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Name</label>
                                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter name" />
                                            </div>
                                        </div>

                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="code">Description</label>
                                            <InputTextarea name="template" value={template} onChange={ev => setTemplate(ev.target.value)} placeholder="Enter Template" autoResize rows="3" cols="30" />
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE USER CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteSla}?</span>
                        </div>
                    </Dialog>





                </div>
            </div>
        </div>
    )
}

import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import TaskService from '../../service/TaskService';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import TicketService from '../../service/TicketService';
import { taskActions } from './ViewTicket';
import { hasRole } from '../../utilities/Helpers';

export const ListTasks = ({ ticket, onCreateClick, onViewTaskClicked }) => {

    const [tasks, setTasks] = useState(null);
    const [selectedTask, setSelectedTask] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [pagination, setPagination] = useState({
        pageSize: 10,
        pageIndex: 0,
        first: 0,
        totalPages: 0,
        sortOrder: -1,
        sortField: 'id',
        numberOfElements: 0
    })

    const [visibleRight, setVisibleRight] = useState(false);
    const [taskId, setTaskId] = useState("");
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [rolesValue, setRolesValue] = useState([]);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteTask, setDeleteTask] = useState(0);

    const ticketService = new TicketService();
    const taskService = new TaskService();

    const getTasksList = () => {
        const paramsJson = {
            page: pagination.pageIndex,
            size: pagination.pageSize,
            sort: pagination.sortField,
            order: (pagination.sortOrder === 1 ? 'asc' : 'desc')
        };
        ticketService.getTasksByTicket(ticket.id, paramsJson).then(data => {
            setTasks(data.content);
            setLoading(false);
            setPagination({
                ...pagination,
                pageIndex: data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number,
                totalPages: data.totalPages,
                numberOfElements: data.totalElements,
                first: ((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1
            })
        })
            .catch(error => {
                setTasks([]);
                setLoading(false);
            });
    }

    useEffect(() => {
        let mounted = true
        if(mounted)
            getTasksList();
        return () => {
            mounted = false
        }
    }, []);

    const onSortChange = (e) => {
        setPagination({
            ...pagination,
            sortField: e.sortField,
            sortOrder: e.sortOrder
        })
    }

    const onPageChange = (e) => {
        setPagination({
            ...pagination,
            pageIndex: e.page,
            first: e.first,
            pageSize: e.rows
        })
    }

    const onStatusButtonClick = (data) => {
        const enableJson = {
            taskId: data.id,
            enable: !data.enabled
        };
        taskService.enableTask(enableJson)
            .then(response => {
                setFailed(false);
                setAlert(response.message + "!");
                getTasksList();
            }).catch(error => {
                setFailed(true);
                setAlert(error.message + "!");
            });
    }

    const onEditButtonClick = (data) => {
        setTaskId(data.id);
        setName(data.name);
        setDescription(data.description);
        setVisibleRight(true);
    }

    const updateTask = (e) => {
        e.preventDefault();
        const taskJson = {
            id: taskId,
            name: name,
            description: description
        }
        if (description !== "") {
            taskJson["description"] = description;
        }
        taskService.editTask(taskJson, taskId)
            .then(response => {
                setFailed(false);
                setAlert("Task updated successfully!");
                getTasksList();
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to update task!");
            });
    }

    const removeTask = (e) => {
        e.preventDefault();
        setDisplayConfirmation(false);
        const taskJson = {
            id: taskId,
            name: name,
            description: description
        }
        taskService.removeTask(taskJson, taskId)
            .then(response => {
                setFailed(false);
                setAlert(response.message);
                getTasksList();
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to delete task!");
                getTasksList();
            });
    }


    const onDeleteButtonClick = (data) => {
        setDeleteTask(data.name);
        setTaskId(data.id);
        setDisplayConfirmation(true);
    }

    const tasksTableHeader = (
        <div className="table-header">
            List of Tasks
            <div className="p-grid">
                <div className="p-col-8 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-8 p-md-6">
                    { hasRole("ROLE_ADMIN") && <Button label="Add Task" icon="pi pi-user-plus" onClick={()=> onCreateClick() }/>}
                </div>
            </div>
        </div>
    );

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const onViewButtonClick = (data) => {
        onViewTaskClicked(data)
    }

    const actionTemplate = (data) => {
        return (
            <div className="p-d-flex">
                <Button type="button" icon="pi pi-eye" onClick={() => onViewButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="View Task" />
                <Button type="button" icon="pi pi-user-edit" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} title="Edit Task" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)}></Button>
            </div>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeTask(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="cardd">

                    {/* RESPONSE ALERTS */}
                    <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />

                    {/* LIST TASK DATATABLE */}
                    <DataTable value={tasks} paginator={false} className="p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedTask} onSelectionChange={(e) => setSelectedTask(e.value)}
                        sortField={pagination.sortField} sortOrder={pagination.sortOrder} onSort={onSortChange} responsive={true}
                        globalFilter={globalFilter} emptyMessage="No tasks found." loading={loading} header={tasksTableHeader}>
                        <Column field="name" header="Name" sortable body={bodyTemplate}></Column>
                        <Column field="ticket.name" header="Ticket" sortable></Column>
                        <Column field="description" header="Description" sortable body={bodyTemplate}></Column>
                        <Column field="status" header="Status" sortable body={bodyTemplate}></Column>
                        <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>
                    <Paginator currentPageReportTemplate={"Showing " + ((pagination.pageIndex * pagination.pageSize) + 1) + " to " + ((pagination.pageIndex * pagination.pageSize) + pagination.pageSize) + " of " + pagination.totalElements}
                        first={pagination.first} rows={pagination.pageSize} rowsPerPageOptions={[10, 20, 30]} onPageChange={onPageChange} totalPages={pagination.totalPages} totalRecords={pagination.totalElements}
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                    </Paginator>

                    {/* EDIT TASK RIGHT SIDE FORM */}
                    <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                        <form onSubmit={updateTask}>
                            <div className="p-grid">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit Task</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Name</label>
                                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter Task Name" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="description">Description</label>
                                                <InputText type="text" name="description" value={description} onChange={e => setDescription(e.target.value)} required placeholder="Enter Decsription" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE TASK CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteTask}?</span>
                        </div>
                    </Dialog>

                </div>
            </div>
        </div>
    )
}

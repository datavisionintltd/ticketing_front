import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import TaskService from '../../service/TaskService';
import TicketService from '../../service/TicketService';
import UserService from '../../service/UserService';
import { useHistory } from 'react-router';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import { Dropdown } from 'primereact/dropdown';
import { InputTextarea } from 'primereact/inputtextarea';


export const AddTask = ({ ticket, onAllTasksClick }) => {

    const history = useHistory();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [ticketId, setTicketId] = useState(0);
    const [tickets, setTickets] = useState([]);
    const [agentId, setAgentId] = useState(0);
    const [users, setUsers] = useState([]);

    const taskService = new TaskService();
    const ticketService = new TicketService();
    const userService = new UserService();

    useEffect(() => {
        const paramsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        ticketService.getTickets(paramsJson)
            .then(data => {
                setTickets(data.content);
            });
        userService.getUsers(paramsJson)
            .then(data => {
                setUsers(data.content);
            });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const submitTask = (e) => {
        e.preventDefault();
        const taskJson = {
            name: name,
            description: description,
            ticket: {
                id: ticket.id
            },
            agent: {
                id: agentId
            }
        };
        taskService.addTask(taskJson)
            .then(response => {
                setFailed(false);
                setAlert(response.message);
                onAllTasksClick()
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to add Task! " + error.error_description);
            });
    }

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{ padding: "2%" }}>
                <form onSubmit={submitTask} className="p-grid p-formgrid p-fluid">
                    <div className="p-col-10 p-col-md-10">
                        <h5 >New Task</h5>
                    </div>
                    <div className="p-col-2 p-col-md-2 p-text-right">
                        <Button label="All tasks" icon="pi pi-list" onClick={() => onAllTasksClick()} />
                    </div>
                    <div className="p-col-12">
                        <hr />
                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-12">
                                <label htmlFor="name">Task</label>
                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter Name" />
                            </div>
                            <div className="p-field p-col-12 p-md-12">
                                <label htmlFor="description">Description</label>
                                <InputTextarea type="text" name="description" value={description} onChange={e => setDescription(e.target.value)} required placeholder="Enter Description" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-12">
                                <label htmlFor="agentId">Agent</label>
                                <Dropdown id="agentId" value={agentId} onChange={(e) => setAgentId(e.value)} options={users} optionLabel="fullName" optionValue="id" filter filterBy="fullName" placeholder="Select Agent"></Dropdown>
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-2">
                                <Button type="submit" label="Add to Tasks" icon="pi pi-check" autoFocus={true} />
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    )
}

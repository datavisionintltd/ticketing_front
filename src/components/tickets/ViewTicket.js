import { Button } from "primereact/button"
import { Dropdown } from "primereact/dropdown"
import { TabView } from "primereact/tabview"
import { TabPanel } from "primereact/tabview"
import { InputTextarea } from "primereact/inputtextarea"
import React, { useState, useEffect } from "react"
import { CustomTimeline } from "../../utilities/components/CustomTimeline"
import { dateFormat, hasRole, dateTimeFormat, getUserId } from '../../utilities/Helpers';
import TicketService from "../../service/TicketService"
import { useParams } from "react-router"
import { ListTasks } from "./ListTasks"
import { AddTask } from "./AddTask"
import { ViewTask } from "./ViewTask"


export const taskActions = {
    LIST: 'list',
    CREATE: 'create',
    VIEW: 'view'
}


export const ViewTicket = () => {

    const ticketService = new TicketService()
    const { id } = useParams()

    const [ticket, setTicket] = useState({})
    const [messages, setMessages] = useState({})
    const [replyJson, setReplyJson] = useState({})
    const [taskAction, setTaskAction] = useState(taskActions.LIST)
    const [selectedTask, setSelectedTask] = useState({})

    useEffect(() => {
        getTicket()
    }, [])

    const getTicket = () => {
        ticketService.getTicket(id).then(res => {
            setReplyJson({ ...replyJson, status: res.status })
            setTicket(res)
            getMessages(id)
        })
    }

    const getMessages = () => {
        ticketService.getMessages(id).then(res => {
            if (Array.isArray(res.content)) {
                let messages = res.content.map((value, index) => {
                    return {
                        name: value?.user?.fullName,
                        time: dateTimeFormat(value?.createdAt),
                        position: getUserId() == value?.user?.id ? "right" : "left",
                        message: value?.message
                    }
                })
                setMessages(messages)
            }
        })
    }

    const onViewTaskClicked = (task) => {
        console.log(task)
        setTaskAction(taskActions.VIEW)
        setSelectedTask(task)
    }

    const submitReply = e => {
        e.preventDefault()
        ticketService.submitReply(id, replyJson).then(res => {
            if (res.message == 'OK') {
                setReplyJson({})
                getTicket()
            }
        })
    }

    const taskActionsView = () => {
        switch (taskAction) {
            case taskActions.CREATE:
                return <AddTask onAllTasksClick={()=>setTaskAction(taskActions.LIST)} ticket={ticket}></AddTask>;

            case taskActions.VIEW:
                return <ViewTask task={selectedTask} onAllTasksClick={()=>setTaskAction(taskActions.LIST)}></ViewTask>;
            case taskActions.LIST:
                return <ListTasks ticket={ticket} onCreateClick={() => { setTaskAction(taskActions.CREATE) }} onViewTaskClicked={onViewTaskClicked}></ListTasks>
            default:
                return <div></div>
        }
    }

    const statuses = [
        { name: "opened" },
        { name: "pending" },
        { name: "closed" },
    ]

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">
                    <h3><strong>Ticket #{ticket?.id}</strong></h3>
                    <hr></hr>
                    <div className="p-grid">
                        <div className="p-col-2">
                            <p>Ticket:</p>
                            <p><strong>{ticket?.name}</strong></p>
                        </div>
                        <div className="p-col-2">
                            <p>Topic:</p>
                            <p><strong>{ticket?.topic?.name}</strong></p>
                        </div>
                        <div className="p-col-2">
                            <p>Date created:</p>
                            <p><strong>{dateFormat(ticket?.createdAt)}</strong></p>
                        </div>
                        <div className="p-col-2">
                            <p>Created by:</p>
                            <p><strong>{ticket?.user?.fullName}</strong></p>
                        </div>
                        <div className="p-col-2">
                            <p>Ticket status:</p>
                            <p><strong>{ticket?.status}</strong></p>
                        </div>
                    </div>
                    <hr />
                    <TabView>
                        <TabPanel header="Ticket thread">
                            <CustomTimeline messages={messages}></CustomTimeline>
                            {
                                ticket?.status !== "closed" ?
                                    <form onSubmit={submitReply}>
                                        <div style={{ paddingTop: "20px" }}>
                                            <h3>Reply ticket</h3>
                                            <hr />
                                            <div className="p-fluid p-formgrid p-grid">
                                                <div className="p-field p-col-12 p-md-12">
                                                    <InputTextarea type="text" name="content" value={replyJson.message} onChange={e => setReplyJson({ ...replyJson, message: e.target.value })} required placeholder="Enter ticket content" />
                                                </div>
                                                {
                                                    hasRole("ROLE_AGENT", "ROLE_ADMIN") ?
                                                        <div className="p-col-4">
                                                            <Dropdown
                                                                id="state"
                                                                value={replyJson.status}
                                                                onChange={(e) => setReplyJson({ ...replyJson, status: e.value })}
                                                                options={statuses}
                                                                optionLabel="name"
                                                                optionValue="name"
                                                                placeholder="Status"></Dropdown>
                                                        </div> : null
                                                }
                                                <div className="p-col-8 p-md-2">
                                                    <Button type="submit" label="Reply" icon="pi pi-check" />
                                                </div>
                                            </div>
                                        </div>
                                    </form> : null
                            }
                        </TabPanel>
                        <TabPanel header="Tasks">
                            {taskActionsView()}
                        </TabPanel>
                    </TabView>
                </div>
            </div>
        </div>
    )
}

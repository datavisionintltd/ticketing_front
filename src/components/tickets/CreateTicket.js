import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import TicketService from '../../service/TicketService';
import ClientService from '../../service/ClientService';
import ProjectService from '../../service/ProjectService';
import UserService from '../../service/UserService';
import DictionaryService from '../../service/DictionaryService';
import { useHistory } from 'react-router';
import { InputTextarea } from 'primereact/inputtextarea';
import { Dropdown } from 'primereact/dropdown';
import { Dialog } from 'primereact/dialog';
import TopicService from '../../service/TopicService';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import { hasRole } from '../../utilities/Helpers';

export const CreateTicket = () => {

    const history = useHistory();
    const [ticketJson, setTicketJson] = useState({name: "", description: "N/A", topic: {id: 0}, client: {id: 0}, project: {id: 0}});
    const [topicId, setTopicId] = useState();
    const [topics, setTopics] = useState([]);
    const [clients, setClients] = useState([]);
    const [projects, setProjects] = useState([]);
    const [departments, setDepartments] = useState([]);
    const [slas, setSlas] = useState([]);

    const [clientJson, setClientJson] = useState({name: "", email: "N/A", phone: "N/A"});
    const [displayClientForm, setDisplayClientForm] = useState(false);
    const [projectJson, setProjectJson] = useState({name: "", clientId: 0, description: "N/A"});
    const [displayProjectForm, setDisplayProjectForm] = useState(false);
    const [topicJson, setTopicJson] = useState({name: "", department: {id: 0}, serviceLevelAgreement: {id: 0}, description: "N/A"});
    const [displayTopicForm, setDisplayTopicForm] = useState(false);
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const ticketService = new TicketService();
    const topicService = new TopicService();
    const clientService = new ClientService();
    const projectService = new ProjectService();
    const userService = new UserService();
    const dictionaryService = new DictionaryService();

    const getTopicsList = () => {
        const paramsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        topicService.getTopics(paramsJson).then(res => {
            setTopics(res.content);
        });
    }

    const getClientsList = () => {
        const paramsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        clientService.getClients(paramsJson).then(data => {
            setClients(data.content);
        });
    }

    const getProjectsList = () => {
        const paramsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        projectService.getProjects(paramsJson).then(data => {
            setProjects(data.content);
        });
    }

    const getClientProjectsList = (id) => {
        const paramsJson = { clientId: id, name: "", page: 0, size: 100, sort: "id", order: "asc" };
        projectService.getProjects(paramsJson).then(data => {
            setProjects(data.content);
        });
    }

    useEffect(() => {
        getTopicsList();
        getClientsList();
        getProjectsList();
        const paramsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        userService.getDepartments(paramsJson)
        .then(data => {
            setDepartments(data.content);
        });
        dictionaryService.getSla(paramsJson)
        .then(data => {
            setSlas(data.content);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitTicket = (e) => {
        e.preventDefault();
        ticketService.addTicket(ticketJson)
        .then(response => {
            setFailed(false);
            setAlert("Ticket added successfully!");
            history.push('/tickets');
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add ticket! " + error.error_description);
        });
    }

    const submitTopic = (e) => {
        e.preventDefault();
        topicService.addTopic(topicJson)
        .then(response => {
            setFailed(false);
            setAlert("Topic added successfully!");
            setDisplayTopicForm(false);
            getTopicsList();
            setTicketJson({...ticketJson, topic: {id: response.id}});
            setTopicJson({name: "", department: {id: 0}, serviceLevelAgreement: {id: 0}, description: "N/A"});
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add Topic! " + error.error_description);
        });
    }

    const submitClient = (e) => {
        e.preventDefault();
        clientService.addClient(clientJson)
        .then(response => {
            setFailed(false);
            setAlert("Customer added successfully!");
            setDisplayClientForm(false);
            getClientsList();
            setTicketClient(response.id);
            setClientJson({name: "", email: "N/A", phone: "N/A"});
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add customer! " + error.error_description);
            setDisplayClientForm(false);
        });
    }

    const submitProject = (e) => {
        e.preventDefault();
        projectService.addProject(projectJson)
        .then(response => {
            setFailed(false);
            setAlert("Product added successfully!");
            setDisplayProjectForm(false);
            getProjectsList();
            setTicketJson({...ticketJson, project: {id: response.id}});
            setProjectJson({name: "", description: "N/A"});
            if (projectJson.clientId > 0) {
                getClientProjectsList(projectJson.clientId);
            }
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add product! " + error.error_description);
            setDisplayProjectForm(false);
        });
    }

    const setTicketClient = (id) => {
        setTicketJson({...ticketJson, client: {id: id}});
        getClientProjectsList(id);
        setProjectJson({...projectJson, clientId: id});
    }

    const topicDialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="button" label="Save" onClick={(e) => submitTopic(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayTopicForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    const clientDialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="button" label="Save" onClick={(e) => submitClient(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayClientForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    const projectDialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="button" label="Save" onClick={(e) => submitProject(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayProjectForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{ padding: "2%" }}>
                <form onSubmit={submitTicket}>
                    <div className="card">
                        <div className="p-card-title" style={{backgroundColor: "#8882BD", color: "#FFFFFF"}}>
                            <h5 style={{textAlign: "center", padding: "0.5em"}}>New Ticket</h5>
                        </div>
                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-2 p-md-4">
                                <label htmlFor="name">Ticket</label>
                                <InputText type="text" id="name" name="name" value={ticketJson.name} onChange={e => setTicketJson({...ticketJson, name: e.target.value})} required placeholder="Enter ticket content" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="topic">Category</label>
                                <div className="p-inputgroup">
                                    <Dropdown id="topic" value={ticketJson.topic.id} onChange={(e) => setTicketJson({...ticketJson, topic: {id: e.value}})} options={topics} optionLabel="name" optionValue="id" filter showClear filterBy="name" filterPlaceholder="Click + button to add category" placeholder="Select Category"></Dropdown>
                                    {hasRole("ROLE_ADMIN") && <Button type="button" label="+" onClick={() => setDisplayTopicForm(true)} className="p-inputgroup-addon"/>}
                                </div>
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-2 p-md-4">
                                <label htmlFor="customer">Customer</label>
                                <div className="p-inputgroup">
                                    <Dropdown id="customer" value={ticketJson.client.id} onChange={(e) => setTicketClient(e.value)} options={clients} optionLabel="name" optionValue="id" filter showClear filterBy="name" filterPlaceholder="Click + button to add customer" placeholder="Select Customer"></Dropdown>
                                    {hasRole("ROLE_ADMIN") && <Button type="button" label="+" onClick={() => setDisplayClientForm(true)} className="p-inputgroup-addon"/>}
                                </div>
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="product">Product</label>
                                <div className="p-inputgroup">
                                    <Dropdown id="product" value={ticketJson.project.id} onChange={(e) => setTicketJson({...ticketJson, project: {id: e.value}})} options={projects} optionLabel="name" optionValue="id" filter showClear filterBy="name" filterPlaceholder="Click + button to add product" placeholder="Select Product"></Dropdown>
                                    {hasRole("ROLE_ADMIN") && <Button type="button" label="+" onClick={() => setDisplayProjectForm(true)} className="p-inputgroup-addon"/>}
                                </div>
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-2 p-md-8">
                                <label htmlFor="description">Description</label>
                                <InputTextarea type="text" name="description" value={ticketJson.description} onChange={e => setTicketJson({...ticketJson, description: e.target.value})} required placeholder="Enter description" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-col-12 p-md-offset-4 p-md-4">
                                <Button type="submit" label="Create Ticket" icon="pi pi-check" autoFocus={true} />
                            </div>
                        </div>
                    </div>
                </form>
                <Dialog header="New Category" visible={displayTopicForm} className="p-col-4 p-offset-2" modal footer={topicDialogFooter} onHide={() => setDisplayTopicForm(false)}>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="name">Category</label>
                            <InputText type="text" name="name" value={topicJson.name} onChange={e => setTopicJson({...topicJson, name: e.target.value})} required placeholder="Enter category" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="department">Department</label>
                            <Dropdown id="department" name="department" value={topicJson.department.id} onChange={(e) => setTopicJson({...topicJson, department: {id: e.value}})} options={departments} optionLabel="name" optionValue="id" filter filterBy="name" required placeholder="Select Department" appendTo={document.body}></Dropdown>
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="priority">Priority</label>
                            <Dropdown id="priority" name="priority" value={topicJson.serviceLevelAgreement.id} onChange={(e) => setTopicJson({...topicJson, serviceLevelAgreement: {id: e.value}})} options={slas} optionLabel="name" optionValue="id" filter filterBy="name" required placeholder="Select Priority" appendTo={document.body}></Dropdown>
                        </div>
                    </div>
                </Dialog>
                <Dialog header="New Customer" visible={displayClientForm} className="p-col-4 p-offset-2" modal footer={clientDialogFooter} onHide={() => setDisplayClientForm(false)}>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="name">Customer name</label>
                            <InputText type="text" name="name" value={clientJson.name} onChange={e => setClientJson({...clientJson, name: e.target.value})} required placeholder="Enter customer name" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="email">Email</label>
                            <InputText type="text" name="email" value={clientJson.email} onChange={e => setClientJson({...clientJson, email: e.target.value})} required placeholder="Enter Email Address" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="phone">Phone Number</label>
                            <InputText type="text" name="phone" value={clientJson.phone} onChange={e => setClientJson({...clientJson, phone: e.target.value})} required placeholder="Enter Phone Number" />
                        </div>
                    </div>
                </Dialog>
                <Dialog header="New Product" visible={displayProjectForm} className="p-col-4 p-offset-2" modal footer={projectDialogFooter} onHide={() => setDisplayProjectForm(false)}>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="name">Product name</label>
                            <InputText type="text" name="name" value={projectJson.name} onChange={e => setProjectJson({...projectJson, name: e.target.value})} required placeholder="Enter product name" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="client">Customer</label>
                            <Dropdown id="client" name="client" value={projectJson.clientId} onChange={(e) => setProjectJson({...projectJson, clientId: e.value})} options={clients} optionLabel="name" optionValue="id" filter filterBy="name" required placeholder="Select Customer" appendTo={document.body}></Dropdown>
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="description">Other Details</label>
                            <InputText type="text" name="description" value={projectJson.description} onChange={e => setProjectJson({...projectJson, description: e.target.value})} required placeholder="Enter Description" />
                        </div>
                    </div>
                </Dialog>
            </div>
        </div>
    )
}

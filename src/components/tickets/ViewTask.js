import { Button } from 'primereact/button'
import { Dropdown } from 'primereact/dropdown'
import { InputTextarea } from 'primereact/inputtextarea'
import React, { useEffect, useState } from 'react'
import TaskService from '../../service/TaskService'
import { CustomTimeline } from "../../utilities/components/CustomTimeline"
import { dateTimeFormat, getUserId, hasRole } from '../../utilities/Helpers'

export const ViewTask = ({ task, onAllTasksClick }) => {

    const [messages, setMessages] = useState({})
    const taskService = new TaskService();
    const [replyJson, setReplyJson] = useState({})

    const statuses = [
        { name: "opened" },
        { name: "pending" },
        { name: "closed" },
    ]

    const onSubmitTaskReply = (e) => {
        e.preventDefault()
        taskService.submitReply(task?.id, replyJson).then(res => {
            if (res.message == 'OK') {
                setReplyJson({ status: "", message: "" })
                getTask()
            }
        })
    }

    const getTask = () => {
        taskService.getTask(task?.id).then(res => {
            getMessages()
        })
    }

    const getMessages = () => {
        taskService.getMessages(task?.id).then(res => {
            if (Array.isArray(res?.content)) {
                let messages = res.content.map((value, index) => {
                    return {
                        name: value?.user?.fullName,
                        time: dateTimeFormat(value?.createdAt),
                        position: getUserId() === value?.user?.id ? "right" : "left",
                        message: value?.message
                    }
                })
                setMessages(messages)
            }
        })
    }

    useEffect(() => {
        getTask()
    }, [])

    return (
        <div>
            <div className="p-grid p-formgrid p-fluid">
                <div className="p-col-10 p-col-md-10">
                    <h5 >Task #{task?.id}</h5>
                </div>
                <div className="p-col-2 p-col-md-2 p-text-right">
                    <Button label="All tasks" icon="pi pi-list" onClick={() => onAllTasksClick()} />
                </div>
            </div>
            <hr />
            <CustomTimeline messages={messages}></CustomTimeline>
            {
                task?.status != "closed" ?
                    <form onSubmit={onSubmitTaskReply}>
                        <div style={{ paddingTop: "20px" }}>
                            <h3>Reply task</h3>
                            <hr />
                            <div className="p-fluid p-formgrid p-grid">
                                <div className="p-field p-col-12 p-md-12">
                                    <InputTextarea type="text" name="content" value={replyJson.message} onChange={e => setReplyJson({ ...replyJson, message: e.target.value })} required placeholder="Enter task content" />
                                </div>
                                {
                                    hasRole("ROLE_AGENT", "ROLE_ADMIN") ?
                                        <div className="p-col-4">
                                            <Dropdown
                                                id="state"
                                                value={replyJson.status}
                                                onChange={(e) => setReplyJson({ ...replyJson, status: e.value })}
                                                options={statuses}
                                                optionLabel="name"
                                                optionValue="name"
                                                placeholder="Status"></Dropdown>
                                        </div> : null
                                }
                                <div className="p-col-8 p-md-2">
                                    <Button type="submit" label="Reply" icon="pi pi-check" />
                                </div>
                            </div>
                        </div>
                    </form> : null
            }
        </div>
    )
}

import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import TicketService from '../../service/TicketService';
import TopicService from '../../service/TopicService';
import CompanyService from '../../service/CompanyService';
import { Link, useHistory } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';
import { InputTextarea } from 'primereact/inputtextarea';
import { Dropdown } from 'primereact/dropdown';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import { format } from 'date-fns';
import { dateFormat, dateTimeFormat, hasRole } from '../../utilities/Helpers';
import ClientService from '../../service/ClientService';

export const ListTickets = () => {

    const history = useHistory();

    const [tickets, setTickets] = useState([]);
    const [selectedTicket, setSelectedTicket] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [clientAlert, setClientAlert] = useState("");
    const [cientFailed, setClientFailed] = useState(null);

    const [displayForm, setDisplayForm] = useState(false);
    const [displayAssignForm, setDisplayAssignForm] = useState(false);
    const [visibleRight, setVisibleRight] = useState(false);
    const [ticketJson, setTicketJson] = useState({name: "", description: "N/A", topic: {id: 0}});
    const [ticketAssignJson, setTicketAssignJson] = useState({ticketId: 0, agentId: 0});
    const [ticketId, setTicketId] = useState(0);
    const [ticketContent, setTicketContent] = useState("");
    const [topic, setTopic] = useState("");
    const [topics, setTopics] = useState([]);
    const [agents, setAgents] = useState([]);
    const [ticketList, setTicketList] = useState([]);
    const [submitted, setSubmitted] = useState(false);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteTicket, setDeleteTicket] = useState(0);

    const [clientJson, setClientJson] = useState({name: "", email: "N/A", phone: "N/A"});
    const [displayClientForm, setDisplayClientForm] = useState(false);

    const ticketService = new TicketService();
    const topicService = new TopicService();
    const companyService = new CompanyService();

    const getTicketsList = () => {
        const paramsJson = { page: pageIndex, size: pageSize, sort: sortField, order: (sortOrder === 1 ? 'asc' : 'desc') };
        ticketService.getTickets(paramsJson).then(data => {
            setTickets(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
        })
        .catch(error => {
            setTickets([]);
            setLoading(false);
        });
    }

    useEffect(() => {
        getTicketsList();
        const paramsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        topicService.getTopics(paramsJson).then(res => {
            setTopics(res.content);
        });
        if (hasRole("ROLE_ADMIN")) {
            ticketService.getTickets(paramsJson).then(data => {
                setTicketList(data.content);
            });
            companyService.getAgents(paramsJson).then(data => {
                setAgents(data.content);
            });
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pageIndex, sortField, sortOrder, first, pageSize, submitted]);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }


    const submitClient = (e) => {
        e.preventDefault();

        const clientService = new ClientService();
        clientService.addClient(clientJson)
        .then(response => {
            setClientFailed(false);
            setClientAlert("Customer added successfully!");
            setDisplayClientForm(false);
            history.push("/clients");
        }).catch(error => {
            setClientFailed(true);
            setClientAlert("Failed to add customer! " + error.error_description);
            setDisplayClientForm(false);
        });
    }

    const submitTicket = (e) => {
        e.preventDefault();
        ticketService.addTicket(ticketJson)
        .then(response => {
            setFailed(false);
            setAlert("Ticket added successfully!");
            setTicketId(0);
            setTicketJson({name: "", description: "N/A", topic: {id: 0}});
            setSubmitted(true);
            setDisplayForm(false);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add ticket! " + error.error_description);
        });
    }

    const onEditButtonClick = (data) => {
        setTicketId(data.id);
        setTicketJson({id: data.id, name: data.name, description: data.description, topic: {id: data.topic.id}});
        setTicketContent(data.content);
    }

    const onViewButtonClick = (data) => {
        history.push(`/tickets/show/${data.id}`)
    }

    const editTicket = (e) => {
        e.preventDefault();
        ticketService.editTicket(ticketJson)
        .then(response => {
            setFailed(false);
            setAlert("Ticket updated successfully!");
            getTicketsList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update ticket! " + error.error_description);
        });
    }

    const assignTicket = (e) => {
        e.preventDefault();
        ticketService.assignTicket(ticketAssignJson)
        .then(response => {
            setFailed(false);
            setAlert("Ticket assigned successfully!");
            setTicketAssignJson({ticketId: 0, agentId: 0});
            setSubmitted(true);
            setDisplayAssignForm(false);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to assign ticket! " + error.error_description);
        });
    }

    const onAssignTicketClick = (data) => {
        setTicketAssignJson({...ticketAssignJson, ticketId: data.id});
        setDisplayAssignForm(true);
    }

    const onDeleteButtonClick = (data) => {
        setDeleteTicket(data.fullName);
        setTicketId(data.id);
        setDisplayConfirmation(true);
    }

    const removeTicket = () => {
        ticketService.removeTicket(ticketId)
        .then(data => {
            setDisplayConfirmation(false);
            setFailed(false);
            setAlert("Ticket remove successfully!");
            getTicketsList();
        });
    }

    const createTicket = () => {
        history.push("/tickets/create");
    }

    const ticketsTableHeader = (
        <div className="table-header">
            <h4>Tickets</h4>
            <div className="p-grid">
                <div className="p-col-12 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-12 p-md-5 p-pl-md-6 p-pl-lg-6">
                    <Button label="Add Ticket"onClick={() => createTicket()} icon="pi pi-ticket-plus"/>
                </div>
            </div>
        </div>
    );

    const dialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="submit" label="Save" onClick={(e) => submitTicket(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    const assignDialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="submit" label="Save" onClick={(e) => assignTicket(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayAssignForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const ticketBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Name</span>
                <img alt={data.fullName} src={`assets/layout/images/avatar/ticket.png`} width="32" style={{ verticalAlign: 'middle' }} />
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.fullName}</span>
            </>
        );
    };

    const dueDateTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {dateTimeFormat(data.dueDate)}
            </>
        );
    };

    const statusBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Status</span>
                <span className={`customer-badge status-${data.status}`}>{data.status}</span>
                {/* <Link to="#" className={`customer-badge status-${data.enabled ? 'unqualified' : 'qualified'}`}>
                    <i className={`pi pi-${data.enabled ? 'times' : 'check'}`}></i>
                </Link> */}
            </>
        )
    };

    const actionTemplate = (data) => {
        return (
            <div className="p-d-flex">
                <Button type="button" icon="pi pi-eye" className="p-button-sm" onClick={() => onViewButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="View Ticket"/>
                {hasRole("ROLE_ADMIN") && <Button type="button" icon="pi pi-user-plus" className="p-button-success p-button-sm" onClick={() => onAssignTicketClick(data)} style={{ marginRight: '.25em' }} tooltip="Assign agent"/>}
                {hasRole("ROLE_ADMIN") && <Button type="button" icon="pi pi-pencil" className="p-button-warning p-button-sm" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="Edit Ticket"/>}
                {hasRole("ROLE_ADMIN") && <Button type="button" icon="pi pi-trash" className="p-button-danger p-button-sm" onClick={() => onDeleteButtonClick(data)} tooltip="Delete Ticket"></Button>}
            </div>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={() => removeTicket()} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">
                    {/* RESPONSE ALERTS */}
                    <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />

                    {/* LIST TICKET DATATABLE */}
                    { tickets.length > 0 &&
                        <div className="p-col-12">
                            <DataTable value={tickets} paginator={false} className="p-datatable-gridlines p-datatable-striped p-datatable-sm p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedTicket} onSelectionChange={(e) => setSelectedTicket(e.value)}
                                sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                                globalFilter={globalFilter} emptyMessage="No tickets found." loading={loading} header={ticketsTableHeader}>
                                <Column field="name" header="Ticket" sortable body={bodyTemplate}></Column>
                                <Column field="topic.serviceLevelAgreement.name" header="SLA" sortable></Column>
                                <Column field="description" header="Description" sortable body={bodyTemplate}></Column>
                                <Column field="user.fullName" header="Agent" sortable></Column>
                                <Column field="dueDate" header="Due Date" sortable body={dueDateTemplate}></Column>
                                <Column field="status" header="Status" sortable body={statusBodyTemplate}></Column>
                                <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                            </DataTable>
                            <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                                first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                                paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                            </Paginator>
                        </div>
                    }
                    { tickets.length <= 0 &&
                        <div className="p-col-12 p-pl-3 p-text-center">
                            <h5 className="p-text-bold p-text-center">You don't have any ticket</h5>
                            <span>Add your first ticket and start to explore the beauty of supporting your customers.</span>
                            <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                                <img src="assets/layout/images/dashboard/tickets.svg" alt="Max Desk" className="logo" style={{width: "20%"}} />
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="button" onClick={() => createTicket()} label="Add Ticket Now" icon="pi pi-users" />
                            </div>
                        </div>
                    }

                    {/* EDIT TICKET RIGHT SIDE FORM */}
                    <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                        <form onSubmit={editTicket}>
                            <div className="p-grid">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit Ticket</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="firstname2">Topic</label>
                                                <InputText type="text" name="topic" value={topic} onChange={e => setTopic(e.target.value)} required placeholder="Enter Full Name" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="firstname2">Email Address</label>
                                                <InputTextarea type="text" name="content" value={ticketContent} onChange={e => setTicketContent(e.target.value)} required placeholder="Enter Email" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE TICKET CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteTicket}?</span>
                        </div>
                    </Dialog>

                    {/* ADD TICKET DIALOG */}
                    <Dialog header="New Ticket" visible={displayForm} className="p-col-6" modal={true}
                    footer={dialogFooter}
                        onHide={() => setDisplayForm(false)} maximizable={true} blockScroll={true}>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-2 p-md-4">
                                <label htmlFor="name">Ticket</label>
                                <InputText type="text" name="name" value={ticketJson.name} onChange={e => setTicketJson({...ticketJson, name: e.target.value})} required placeholder="Enter ticket content" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="topic">Topic</label>
                                <Dropdown id="topic" name="topic" value={ticketJson.topic.id} options={topics} onChange={(e) => setTicketJson({...ticketJson, topic : {id: e.value}})} optionLabel="name" optionValue="id" filter filterBy="name" placeholder="Select Topic" appendTo={document.body}></Dropdown>
                            </div>
                            <div className="p-field p-col-12 p-md-offset-2 p-md-8">
                                <label htmlFor="customer">Customer</label>
                                <div className="p-inputgroup">
                                    <Dropdown id="customer" name="customer" value={ticketJson.topic.id} options={topics} onChange={(e) => setTicketJson({...ticketJson, topic : {id: e.value}})} optionLabel="name" optionValue="id" filter filterBy="name" placeholder="Select Product" appendTo={document.body}></Dropdown>
                                    <Button label="+" onClick={() => setDisplayClientForm(true)}/>
                                </div>
                            </div>
                            <div className="p-field p-col-12 p-md-offset-2 p-md-8">
                                <label htmlFor="product">Product</label>
                                <div className="p-inputgroup">
                                    <Dropdown id="product" name="product" value={ticketJson.topic.id} options={topics} onChange={(e) => setTicketJson({...ticketJson, topic : {id: e.value}})} optionLabel="name" optionValue="id" filter filterBy="name" placeholder="Select Product" appendTo={document.body}></Dropdown>
                                    <Button label="+" />
                                </div>
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-2 p-md-8">
                                <label htmlFor="description">Description</label>
                                <InputTextarea type="text" name="description" value={ticketJson.description} onChange={e => setTicketJson({...ticketJson, description: e.target.value})} required placeholder="Enter description" />
                            </div>
                        </div>
                    </Dialog>

                    {/* ASSIGN TICKET DIALOG */}
                    <Dialog header="Assign ticket to agent" visible={displayAssignForm} className="p-col-4" modal={true} footer={assignDialogFooter} onHide={() => setDisplayAssignForm(false)} maximizable={true} blockScroll={true}>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-1 p-md-10">
                                <label htmlFor="topic">Agent</label>
                                <Dropdown id="topic" name="topic" value={ticketAssignJson.agentId} options={agents} onChange={(e) => setTicketAssignJson({...ticketAssignJson, agentId: e.value})} optionLabel="fullName" optionValue="id" filter filterBy="fullName" placeholder="Select Agent" appendTo={document.body}></Dropdown>
                            </div>
                        </div>
                    </Dialog>


                    <Dialog header="New Customer" visible={displayClientForm} className="p-col-4 p-offset-2" modal footer={

                        <div className="p-grid">
                            <div className="p-col-4 p-offset-2">
                                <Button type="submit" label="Save" onClick={(e) => submitClient(e)} icon="pi pi-check-circle" className="p-button-success" />
                            </div>
                            <div className="p-col-4">
                                <Button type="button" label="Cancel" onClick={() => setDisplayClientForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
                            </div>
                        </div>

                    } onHide={() => setDisplayClientForm(false)}>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-12">
                                <label htmlFor="name">Customer name</label>
                                <InputText type="text" name="name" value={clientJson.name} onChange={e => setClientJson({...clientJson, name: e.target.value})} required placeholder="Enter customer name" />
                            </div>
                            <div className="p-field p-col-12 p-md-12">
                                <label htmlFor="email">Email</label>
                                <InputText type="text" name="email" value={clientJson.email} onChange={e => setClientJson({...clientJson, email: e.target.value})} required placeholder="Enter Email Address" />
                            </div>
                            <div className="p-field p-col-12 p-md-12">
                                <label htmlFor="phone">Phone Number</label>
                                <InputText type="text" name="phone" value={clientJson.phone} onChange={e => setClientJson({...clientJson, phone: e.target.value})} required placeholder="Enter Phone Number" />
                            </div>
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    )
}

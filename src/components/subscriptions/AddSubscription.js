import React, { useEffect, useState } from 'react';
import { Button } from 'primereact/button';
import SubscriptionService from '../../service/SubscriptionService';
import { useHistory } from 'react-router';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import UserService from '../../service/UserService';
import { Dropdown } from 'primereact/dropdown';

export const AddSubscription = () => {

    const history = useHistory();

    const [clientPackage, setClientPackage] = useState(0);
    const [client, setClient] = useState(0);

    const [clients, setClients] = useState([])
    const [packages, setPackages] = useState([])

    const [roleId, setRoleId] = useState("ROLE_CLIENT")

    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const subscriptionService = new SubscriptionService();

    const submitSubscription = (e) => {
        e.preventDefault();
        const subscriptionJson = {
            clientPackage: clientPackage,
            client: client
        };

        subscriptionService.addSubscription(subscriptionJson)
        .then(response => {
            setFailed(false);
            setAlert("Subscription added successfully!");
            history.push('/subscriptions/list');
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add Subscription! " + error.error_description);
        });

    }

    useEffect(() => {
        const userService = new UserService()
        const paramsJson = { page: 0, size: 100, sort: "id", order: "asc" };
        subscriptionService.getPackages(paramsJson).then(data => {
            setPackages(data.content)
        })
        userService.getUsersByRole(roleId).then(data => {
            setClients(data)
        })
    }, [])

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{ padding: "2%" }}>
                <form onSubmit={submitSubscription}>
                    <div className="card">
                        <h5>Add Subscription</h5>
                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="firstname2">Clients</label>
                                <Dropdown
                                    id="state"
                                    value={client}
                                    onChange={(e) => setClient(e.value)}
                                    options={clients}
                                    optionLabel="user.fullName"
                                    optionValue="user.id"
                                    placeholder="Select Client"></Dropdown>
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="firstname2">Packages</label>
                                <Dropdown
                                    id="state"
                                    value={clientPackage}
                                    onChange={(e) => setClientPackage(e.value)}
                                    options={packages}
                                    optionLabel="name"
                                    optionValue="id"
                                    placeholder="Select Package"></Dropdown>
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label>&nbsp;&nbsp;</label>
                                <Button type="submit" label="Add to Subscriptions" icon="pi pi-check" autoFocus={true} />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

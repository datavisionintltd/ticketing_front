import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { useHistory } from 'react-router';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import SubscriptionService from '../../service/SubscriptionService';

export const AddPackage = () => {

    const history = useHistory();

    const [ packageJson, setPackageJson] = useState({})

    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const subscriptionService = new SubscriptionService();

    const submitPackage = (e) => {
        e.preventDefault();

        subscriptionService.addPackage(packageJson)
        .then(response => {
            setFailed(false);
            setAlert("Package added successfully!");
            history.push('/subscriptions/list');
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add Package! " + error.error_description);
        });
    }

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{ padding: "2%" }}>
                <form onSubmit={submitPackage}>
                    <div className="card">
                        <h5>Add Package</h5>
                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-2 p-md-4">
                                <label htmlFor="name">Name</label>
                                <InputText type="text" name="name" onChange={e => setPackageJson({...packageJson, name: e.target.value})} required placeholder="Enter Name" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="name">Durations</label>
                                <InputText type="number" name="duration" onChange={e => setPackageJson({...packageJson, duration: e.target.value})} required placeholder="Enter Name" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-2 p-md-4">
                                <label htmlFor="name">Number of Agents</label>
                                <InputText type="number" name="number-of-agents" onChange={e => setPackageJson({...packageJson, numberOfAgents: e.target.value})} required placeholder="Enter Name" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="name">Number of Projects</label>
                                <InputText type="number" name="number-of-projects" onChange={e => setPackageJson({...packageJson, numberOfProjects: e.target.value})} required placeholder="Enter Name" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-col-12 p-offset-4 p-md-4 text-left">
                                <Button type="submit" label="Add to Packages" icon="pi pi-check" autoFocus={true} />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

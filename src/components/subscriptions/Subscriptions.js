import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import SubscriptionService from '../../service/SubscriptionService';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';

export const Subscriptions = () => {

    const [subscriptions, setSubscriptions] = useState(null);
    const [selectedSubscription, setSelectedSubscription] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const [visibleRight, setVisibleRight] = useState(false);
    const [subscriptionId, setSubscriptionId] = useState("");
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [rolesValue, setRolesValue] = useState([]);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteSubscription, setDeleteSubscription] = useState(0);

    const subscriptionService = new SubscriptionService();

    const getSubscriptionsList = () => {
        const paramsJson = { page: "page=" + pageIndex, size: "&size=" + pageSize, sort: "&sort=" + sortField, order: "&order=" + (sortOrder === 1 ? 'asc' : 'desc') };
        subscriptionService.getSubscriptions(paramsJson).then(data => {
            setSubscriptions(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
        })
        .catch(error => {
            setSubscriptions([]);
            setLoading(false);
        });
    }

    useEffect(() => {
        getSubscriptionsList();
    }, [pageIndex, sortField, sortOrder, first, pageSize]);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const onStatusButtonClick = (data) => {
        const enableJson = {
            subscriptionId: data.id,
            enable: !data.enabled
        };
        subscriptionService.enableSubscription(enableJson)
        .then(response => {
            setFailed(false);
            setAlert(response.message + "!");
            getSubscriptionsList();
        }).catch(error => {
            setFailed(true);
            setAlert(error.message + "!");
        });
    }

    const onEditButtonClick = (data) => {
        setSubscriptionId(data.id);
        setName(data.name);
        setDescription(data.description);
        setVisibleRight(true);
    }

    const updateSubscription = (e) => {
        e.preventDefault();
        const subscriptionJson = {
            id: subscriptionId,
            name: name,
            description: description
        }
        if (description !== "") {
            subscriptionJson["description"] = description;
        }
        subscriptionService.editSubscription(subscriptionJson, subscriptionId)
        .then(response => {
            setFailed(false);
            setAlert("Subscription updated successfully!");
            getSubscriptionsList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update subscription!");
        });
    }

    const removeSubscription = (e) => {
        e.preventDefault();
        setDisplayConfirmation(false);
        const subscriptionJson = {
            id: subscriptionId,
            name: name,
            description: description
        }
        subscriptionService.removeSubscription(subscriptionJson, subscriptionId)
        .then(response => {
            setFailed(false);
            setAlert("Subscription deleted successfully!");
            getSubscriptionsList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete subscription!");
            getSubscriptionsList();
        });
    }


    const onDeleteButtonClick = (data) => {
        setDeleteSubscription(data.name);
        setSubscriptionId(data.id);
        setDisplayConfirmation(true);
    }

    const subscriptionsTableHeader = (
        <div className="table-header">
            List of Subscriptions
            <div className="p-grid">
                <div className="p-col-8 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-8 p-md-6">
                    <Link to="/subscriptions/create" style={{float: "right"}}><Button label="Add Subscription" icon="pi pi-user-plus"/></Link>
                </div>
            </div>
        </div>
    );

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const subscriptionBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Name</span>
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.name}</span>
            </>
        );
    };

    const statusBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Status</span>
                <span className={`customer-badge status-${data.enabled ? 'qualified' : 'unqualified'}`}>{data.enabled ? 'Enabled' : 'Disabled'}</span>
                <Link to="#" onClick={() => onStatusButtonClick(data)} className={`customer-badge status-${data.enabled ? 'unqualified' : 'qualified'}`}>
                    <i className={`pi pi-${data.enabled ? 'times' : 'check'}`}></i>
                </Link>
            </>
        )
    };

    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-user-edit" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="Edit Subscription" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} tooltip="Delete Tak"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeSubscription(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">

                    {/* RESPONSE ALERTS */}
                    { failed === false &&
                        <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                <span className="p-message-text">{alert}</span>
                            </div>
                        </div>
                    }
                    { failed === true &&
                        <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                <span className="p-message-text">{alert}</span>
                            </div>
                        </div>
                    }

                    {/* LIST SUBSCRIPTION DATATABLE */}
                    <DataTable value={subscriptions} paginator={false} className="p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedSubscription} onSelectionChange={(e) => setSelectedSubscription(e.value)}
                        sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                        globalFilter={globalFilter} emptyMessage="No subscriptions found." loading={loading} header={subscriptionsTableHeader}>
                        <Column selectionMode="multiple" headerStyle={{ width: '3em' }}></Column>
                        <Column field="name" header="Name" sortable body={subscriptionBodyTemplate}></Column>
                        <Column field="description" header="Description" sortable body={bodyTemplate}></Column>
                        <Column field="enabled" header="Status" sortable body={statusBodyTemplate}></Column>
                        <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>
                    <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                        first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                    </Paginator>

                    {/* EDIT SUBSCRIPTION RIGHT SIDE FORM */}
                    <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                        <form onSubmit={updateSubscription}>
                            <div className="p-grid">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit Subscription</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Name</label>
                                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter Subscription Name" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="description">Description</label>
                                                <InputText type="text" name="description" value={description} onChange={e => setDescription(e.target.value)} required placeholder="Enter Decsription" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE SUBSCRIPTION CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteSubscription}?</span>
                        </div>
                    </Dialog>

                </div>
            </div>
        </div>
    )
}

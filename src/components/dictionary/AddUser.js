import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import UserService from '../../service/UserService';
import { MultiSelect } from 'primereact/multiselect';
import { useHistory } from 'react-router';

export const AddUser = () => {

	const history = useHistory();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [confirm, setConfirm] = useState("");
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [roles, setRoles] = useState(null);
    const [rolesValue, setRolesValue] = useState(null);
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const userService = new UserService();

    useEffect(() => {
        userService.getRoles()
        .then(data => {
            const options = data.map(d => ({
                name : d.name,
                code : d.id
            }));
            setRoles(options);
        });
    }, []);

    const submitUser = (e) => {
        e.preventDefault();
        const userJson = {
            username: username,
            password: password,
            fullName: fullName,
            email: email,
            phoneNumber: phoneNumber
        };
        const rolesJsonArray = rolesValue.map(r => ({
            id : r.code
        }));
        const jsonValue = {user: userJson, roles: rolesJsonArray};
        // console.log("user content: " + JSON.stringify(jsonValue));
        if(password === confirm && rolesJsonArray.length > 0) {
            userService.addUser(jsonValue)
            .then(response => {
                setFailed(false);
                setAlert("User added successfully!");
                history.push('/user/list');
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to add user! " + error.error_description);
            });
        } else if (rolesJsonArray.length <= 0) {
            setFailed(true);
            setAlert("Please choose roles!");
        } else {
            setFailed(true);
            setAlert("Password does not match!");
        }
    }

    const itemTemplate = (option) => {
        return (
            <div className="country-item">
                <span>{option.name}</span>
            </div>
        );
    };

    const selectedItemTemplate = (option) => {
        if (option) {
            return (
                <div className="country-item country-item-value">
                    <span>{option.name}</span>
                </div>
            );
        }

        return 'Select Roles';
    };

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{padding: "2%"}}>
                <form onSubmit={submitUser}>
                    <div className="card">
                        <h5>Add User</h5>
                        { failed === false &&
                            <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                                <div className="p-message-wrapper">
                                    <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                    <span className="p-message-text">{alert}.</span>
                                </div>
                            </div>
                        }
                        { failed === true &&
                            <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                                <div className="p-message-wrapper">
                                    <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                    <span className="p-message-text">{alert}.</span>
                                </div>
                            </div>
                        }
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="firstname2">Username</label>
                                <InputText type="text" name="username" value={username} onChange={e => setUsername(e.target.value)} required placeholder="Enter Username" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="lastname2">Password</label>
                                <InputText type="password" name="password" value={password} onChange={e => setPassword(e.target.value)} required placeholder="Enter Password" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="lastname2">Password</label>
                                <InputText type="password" name="confirm" value={confirm} onChange={e => setConfirm(e.target.value)} required placeholder="Confirm Password" />
                            </div>
                            <div className="p-field p-col-12 p-md-6">
                                <label htmlFor="firstname2">Full Name</label>
                                <InputText type="text" name="fullName" value={fullName} onChange={e => setFullName(e.target.value)} required placeholder="Enter Full Name" />
                            </div>
                            <div className="p-field p-col-12 p-md-6">
                                <label htmlFor="firstname2">Email Address</label>
                                <InputText type="text" name="email" value={email} onChange={e => setEmail(e.target.value)} required placeholder="Enter Email" />
                            </div>
                            <div className="p-field p-col-12 p-md-6">
                                <label htmlFor="firstname2">Phone Number</label>
                                <InputText type="text" name="phoneNumber" value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)} required placeholder="Enter Phone Number" />
                            </div>
                            <div className="p-field p-col-12 p-md-6">
                                <label htmlFor="roles">Roles</label>
                                <MultiSelect value={rolesValue} onChange={(e) => setRolesValue(e.value)} options={roles} optionLabel="name" placeholder="Select Roles"
                                filter itemTemplate={itemTemplate} selectedItemTemplate={selectedItemTemplate} className="multiselect-custom" />
                            </div>
                            <div className="p-col-12 p-offset-4 p-md-4">
                                <Button type="submit" label="Add to Users" icon="pi pi-check" autoFocus={true}/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

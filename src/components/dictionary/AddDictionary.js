import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import DictionaryService from '../../service/DictionaryService';
import { useHistory } from 'react-router';

export const AddDictionary = () => {

	const history = useHistory();

    const [name, setName] = useState("");
	const [code, setCode] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [add, setAdd] = useState(false);
    const [list, setList] = useState(false);
    const dictionaryService = new DictionaryService();

    const submitDictionary = (e) => {
        e.preventDefault();
        const dictionaryJson = {
            name: name,
            code: code
        };
        dictionaryService.addDictionary(dictionaryJson)
        .then(response => {
            setFailed(false);
            setName("");
            setCode("");
            setAlert("dictionary added successfully!");
            if(add){
                history.push('/dictionaries/create');
            }
            if(list){
                history.push('/dictionaries');
            }
           
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add role! " + error.error_description);
        });
    }

    return (
        <div className="p-grid">
            <div className="p-col-12">
                <form onSubmit={submitDictionary}>
                    <div className="card">
                        <h5>Add Dictionary</h5>
                        { failed === false &&
                            <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                                <div className="p-message-wrapper">
                                    <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                    <span className="p-message-text">{alert}.</span>
                                </div>
                            </div>
                        }
                        { failed === true &&
                            <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                                <div className="p-message-wrapper">
                                    <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                    <span className="p-message-text">{alert}.</span>
                                </div>
                            </div>
                        }
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-0 p-md-10">
                                <label htmlFor="name">Name</label>
                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter Name" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-0 p-md-10">
                                <label htmlFor="code">Code</label>
                                <InputText type="text" name="code" value={code} onChange={ev => setCode(ev.target.value)} required placeholder="Enter Code" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="submit">&nbsp;</label>
                                <Button type="submit" label="Save" icon="pi pi-check" onClick={() => (setList(true))} autoFocus={true}/>
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="submit">&nbsp;</label>
                                <Button type="submit" label="Save and insert next" icon="pi pi-check" onClick={() => (setAdd(true))} />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

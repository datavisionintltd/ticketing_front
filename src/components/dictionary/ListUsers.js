import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import UserService from '../../service/UserService';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';

export const ListUsers = () => {

    const [users, setUsers] = useState(null);
    const [selectedUser, setSelectedUser] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const [visibleRight, setVisibleRight] = useState(false);
    const [userId, setUserId] = useState("");
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [rolesValue, setRolesValue] = useState([]);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteUser, setDeleteUser] = useState(0);

    const userService = new UserService();

    const getUsersList = () => {
        const paramsJson = { page: "page=" + pageIndex, size: "&size=" + pageSize, sort: "&sort=" + sortField, order: "&order=" + (sortOrder === 1 ? 'asc' : 'desc') };
        userService.getUsers(paramsJson).then(data => {
            setUsers(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
        })
        .catch(error => {
            setUsers([]);
            setLoading(false);
        });
    }

    useEffect(() => {
        getUsersList();
    }, [pageIndex, sortField, sortOrder, first, pageSize]);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const onStatusButtonClick = (data) => {
        const enableJson = {
            userId: data.id,
            enable: !data.enabled
        };
        userService.enableUser(enableJson)
        .then(response => {
            setFailed(false);
            setAlert(response.message + "!");
            getUsersList();
        }).catch(error => {
            setFailed(true);
            setAlert(error.message + "!");
        });
    }

    const onEditButtonClick = (data) => {
        setUserId(data.id);
        setFullName(data.fullName);
        setEmail(data.email);
        setPhoneNumber(data.phoneNumber);
        setVisibleRight(true);
    }

    const submitUser = (e) => {
        e.preventDefault();
        const userJson = {
            id: userId,
            fullName: fullName,
            email: email,
            phoneNumber: phoneNumber
        }
        const rolesJsonArray = rolesValue.map(r => ({
            id : r.code
        }));
        const jsonValue = {user: userJson, roles: rolesJsonArray};
        userService.editUser(jsonValue)
        .then(response => {
            setFailed(false);
            setAlert("User updated successfully!");
            getUsersList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update user! " + error.error_description);
        });
    }

    const onDeleteButtonClick = (data) => {
        setDeleteUser(data.fullName);
        setDisplayConfirmation(true);
    }

    const usersTableHeader = (
        <div className="table-header">
            List of Users
            <div className="p-grid">
                <div className="p-col-8 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-8 p-md-6">
                    <Link to="/user/new" style={{float: "right"}}><Button label="Add User" icon="pi pi-user-plus"/></Link>
                </div>
            </div>
        </div>
    );

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const userBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Name</span>
                <img alt={data.fullName} src={`assets/layout/images/avatar/user.png`} width="32" style={{ verticalAlign: 'middle' }} />
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.fullName}</span>
            </>
        );
    };

    const statusBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Status</span>
                <span className={`customer-badge status-${data.enabled ? 'qualified' : 'unqualified'}`}>{data.enabled ? 'Enabled' : 'Disabled'}</span>
                <Link to="#" onClick={() => onStatusButtonClick(data)} className={`customer-badge status-${data.enabled ? 'unqualified' : 'qualified'}`}>
                    <i className={`pi pi-${data.enabled ? 'times' : 'check'}`}></i>
                </Link>
            </>
        )
    };

    const rolesBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Roles</span>
                {data.roles.map((role, index) => (
                    <span >{(index > 0 ? ", " : "") + role.name}</span>
                ))}
            </>
        )
    };

    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-user-edit" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="Edit User" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} tooltip="Delete User"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">

                    {/* RESPONSE ALERTS */}
                    { failed === false &&
                        <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                <span className="p-message-text">{alert}</span>
                            </div>
                        </div>
                    }
                    { failed === true &&
                        <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                <span className="p-message-text">{alert}</span>
                            </div>
                        </div>
                    }

                    {/* LIST USER DATATABLE */}
                    <DataTable value={users} paginator={false} className="p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedUser} onSelectionChange={(e) => setSelectedUser(e.value)}
                        sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                        globalFilter={globalFilter} emptyMessage="No users found." loading={loading} header={usersTableHeader}>
                        <Column selectionMode="multiple" headerStyle={{ width: '3em' }}></Column>
                        <Column field="fullName" header="Name" sortable body={userBodyTemplate}></Column>
                        <Column field="email" header="Email Address" sortable body={bodyTemplate}></Column>
                        <Column field="phoneNumber" header="Phone Number" sortable body={bodyTemplate}></Column>
                        <Column field="enabled" header="Status" sortable body={statusBodyTemplate}></Column>
                        <Column field="roles" header="Roles" sortable body={rolesBodyTemplate}></Column>
                        <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>
                    <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                        first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                    </Paginator>

                    {/* EDIT USER RIGHT SIDE FORM */}
                    <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                        <form onSubmit={submitUser}>
                            <div className="p-grid">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit User</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="firstname2">Full Name</label>
                                                <InputText type="text" name="fullName" value={fullName} onChange={e => setFullName(e.target.value)} required placeholder="Enter Full Name" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="firstname2">Email Address</label>
                                                <InputText type="text" name="email" value={email} onChange={e => setEmail(e.target.value)} required placeholder="Enter Email" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="firstname2">Phone Number</label>
                                                <InputText type="text" name="phoneNumber" value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)} required placeholder="Enter Phone Number" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE USER CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteUser}?</span>
                        </div>
                    </Dialog>

                </div>
            </div>
        </div>
    )
}

import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import DictionaryService from '../../service/DictionaryService';
import { Link } from 'react-router-dom';
import { Sidebar } from 'primereact/sidebar';
import { Dialog } from 'primereact/dialog';
import { useParams } from "react-router-dom";
import { Paginator } from 'primereact/paginator';
export const DictionaryContext = React.createContext()

export const DictionaryItemList = ({ match }) => {
    let params = useParams();
    const [dictionary, setDictionary] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [visibleRight, setVisibleRight] = useState(false);
    const [name, setName] = useState("");
	const [code, setCode] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [submitted, setSubmitted] = useState(false);

    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteDictionary, setDeleteDictionary] = useState(0);

    const dictionaryService = new DictionaryService();

    const [ dictionaryId, setDictionaryId] = useState(match.params.dictionaryId) ;
    const [itemName, setItemName] = useState(0);
    const [id, setId] = useState(null);



    //console.log(match);
    //setSubmitted(true);
    useEffect(() => {
        const paramsJson = { dictionaryId: "dictionaryId=" + dictionaryId, code: "&code=" + code, page: "&page=" + pageIndex, size: "&size=" + pageSize, sort: "&sort=" + sortField, order: "&order=" + (sortOrder === 1 ? 'asc' : 'desc') };
        dictionaryService.getDictionaryItems(paramsJson).then(data => {
            setDictionary(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
        })
        .catch(error => {
            setDictionary([]);
            setLoading(false);
        });
    }, [submitted,pageIndex, sortField, sortOrder, first, pageSize, globalFilter]);

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const submitDictionary = (e) => {
        e.preventDefault();
        const dictionaryJson = {
            name: name,
            code: code,
            itemId:id

        };
        dictionaryService.editDictionaryItem(dictionaryJson)
        .then(response => {
            setFailed(false);
            setAlert("dictionary Updated successfully!");
            setSubmitted(true);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update dictionary! ");
        });
    }
    const removeItem = (e) => {
        e.preventDefault();
        setDisplayConfirmation(false);
        dictionaryService.deleteDictionaryItem(id)
        .then(response => {
            setFailed(false);
            setAlert("dictionary Item Deleted successfully!");
            setSubmitted(true);
            setDictionary(response)
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete dictionary item! ");
        });
    }
    const onDeleteButtonClick = (data) => {
        setId(data.id);
        setDisplayConfirmation(true);
    }
    const onEditButtonClick = (data) => {
        //setId(data.id);
        setName(data.name);
        setCode(data.code);
        setId(data.id);
        setVisibleRight(true);
    }

    const dictionaryTableHeader = (
        <div className="table-header">
            Dictionary Items List
            <div className="p-grid">
                <div className="p-col-6 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>

                <div className="p-col-6 p-md-6">
                    <Link to={"/dictionary/item/create/"+dictionaryId} style={{float: "right"}}><Button  style={{float: "right"}}  label="Add Dictionary Item " icon="pi pi-unlock"/></Link>
                </div>



            </div>
        </div>
    );

    const userBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">name</span>
                <img alt={data.name} src={`assets/layout/images/avatar/user.png`} width="32" style={{ verticalAlign: 'middle' }} />
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.name}</span>
            </>
        );
    };
    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };
    const items = [
        {
            label: 'View',
            icon: 'pi pi-refresh'
        },
        {
            label: 'Edit',
            icon: 'pi pi-times'
        },
        {
            label: 'Delete',
            icon: 'pi pi-home'
        }
    ];
    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-pencil" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="Edit Dictionary" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} tooltip="Delete Dictionary"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeItem(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">
                    { failed === false &&
                        <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                <span className="p-message-text">{alert}.</span>
                            </div>
                        </div>
                    }
                    { failed === true &&
                        <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                <span className="p-message-text">{alert}.</span>
                            </div>
                        </div>
                    }

                    {/* LIST ROLES DATATABLE */}
                    <DataTable value={dictionary}  className="p-datatable-customers" rows={10} dataKey="id" rowHover
                        responsive={true} globalFilter={globalFilter} emptyMessage="No dictionaries found." loading={loading} header={dictionaryTableHeader}>
                        <Column field="name" header="Name" sortable body={bodyTemplate}></Column>
                        <Column field="code" header="Code" sortable body={bodyTemplate}></Column>
                        <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>
                    <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                        first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                    </Paginator>

                    {/* EDIT DICTIONARY RIGHT SIDE FORM */}
                    <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                        <form onSubmit={submitDictionary}>
                            <div className="p-grid m3">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit Dictionary</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Name</label>
                                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter name" />
                                            </div>
                                        </div>
                                       {/*  <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Code</label>
                                                <InputText type="text" name="name" value={code} onChange={e => setCode(e.target.value)} required placeholder="Enter name" />
                                            </div>
                                        </div> */}
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE USER CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteDictionary}?</span>
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    )
}

import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import DictionaryService from '../../service/DictionaryService';
import { useHistory } from 'react-router';
import { Dropdown } from 'primereact/dropdown';

export const UpdateTicketSettings = () => {

	const history = useHistory();
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const dictionaryService = new DictionaryService();
    const [roles, setRoles] = useState([]);
    const [status, setStatus] = useState([]);
    const [topic, setTopic] = useState([]);
    const [lockSemanticValue, setLockSemanticValue] = useState(null);
    const [defaultStatusValue, setDefaultStatusValue] = useState(null);
    const [defaultTopicValue, setDefaultTopicValue] = useState(null);
    const [defaultNumberFormatValue, setDefaultNumberFormatValue] = useState(null);
    const [id, setId] = useState(null);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [first, setFirst] = useState(0);
    const [globalFilter, setGlobalFilter] = useState('');

    const paramsJson = { name: globalFilter, page: pageIndex, size: pageSize, sort: sortField, order: (sortOrder === 1 ? 'asc' : 'desc') };

    useEffect(() => {
        dictionaryService.getTicketSettings()
        .then(data => {
            setLockSemanticValue({
                name : data.lockSemantic.name,
                code : data.lockSemantic.id
            });
            setDefaultStatusValue({
                name : data.defaultStatus.name,
                code : data.defaultStatus.id
            });
            setDefaultTopicValue({
                name : data.defaultTopic.name,
                code : data.defaultTopic.id
            });
            setId(data.id);
            setDefaultNumberFormatValue(data.defaultNumberFormat);
        });
        dictionaryService.getStatus(paramsJson)
        .then(data => {
            const statuses = data.content.map(d => ({
                name : d.name,
                code : d.id
            }));
            setStatus(statuses);
        });

        dictionaryService.getTopic(paramsJson)
        .then(data => {
            const topics = data.content.map(d => ({
                name : d.name,
                code : d.id
            }));
            setTopic(topics);
        });
        dictionaryService.getDictionaryItemsByCode("LCKSM")
        .then(data => {
            const options = data.map(d => ({
                name : d.name,
                code : d.id,
            }));
            setRoles(options);
        });
    }, []);
    const submitDictionary = (e) => {
        e.preventDefault();
        const dictionaryJson = {
            defaultTopic: defaultTopicValue.code,
            defaultNumberFormat: defaultNumberFormatValue,
            defaultStatus:defaultStatusValue.code,
            lockSemantic:lockSemanticValue.code,
            id:id
        };
        dictionaryService.updateTicketSettings(dictionaryJson)
        .then(response => {
            setFailed(false);
            setAlert("Ticket settings updated  successfully!");
            //history.push('/dictionary/create');
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update Ticket settings! ");
        });
    }
    const dropdownValues = roles;
    const dropdownStatus = status;
    const dropdownTopic = topic;
    return (
        <div className="p-grid">
            <div className="p-col-12">
                <form onSubmit={submitDictionary}>
                    <div className="card">
                        <h5>Ticket Settings</h5>
                        { failed === false &&
                            <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                                <div className="p-message-wrapper">
                                    <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                    <span className="p-message-text">{alert}.</span>
                                </div>
                            </div>
                        }
                        { failed === true &&
                            <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                                <div className="p-message-wrapper">
                                    <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                    <span className="p-message-text">{alert}.</span>
                                </div>
                            </div>
                        }

                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-2 p-md-4">
                                <label htmlFor="code">Ticket Number format(eg.123456)</label>
                                <InputText type="text" name="code" value={defaultNumberFormatValue} onChange={ev => setDefaultNumberFormatValue(ev.target.value)} required placeholder="######" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-2 p-md-4">
                                <label htmlFor="code">Default Status</label>
                                <Dropdown value={defaultStatusValue} onChange={(e) => setDefaultStatusValue(e.value)} options={dropdownStatus} optionLabel="name" placeholder="Select" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-2 p-md-4">
                                <label htmlFor="code">Default Topic</label>
                                <Dropdown value={defaultTopicValue} onChange={(e) => setDefaultTopicValue(e.value)} options={dropdownTopic} optionLabel="name" placeholder="Select" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-2 p-md-4">
                                <label htmlFor="code">Lock Semantic</label>
                                <Dropdown value={lockSemanticValue} onChange={(e) => setLockSemanticValue(e.value)} options={dropdownValues} optionLabel="name" placeholder="Select" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="submit">&nbsp;</label>
                                <Button type="submit" label="Update Ticket Settings" icon="pi pi-check" autoFocus={true}/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

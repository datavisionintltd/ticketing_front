import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import DictionaryService from '../../service/DictionaryService';
import { useHistory } from 'react-router';
import { Dropdown } from 'primereact/dropdown';

export const UpdateSystemSettings = () => {

	const history = useHistory();
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const dictionaryService = new DictionaryService();
    const [languageValue, setLanguageValue] = useState(null);
    const [languages, setLanguages] = useState(null);
    const [timeZoneValue, setTimeZoneValue] = useState(null);
    const [zones, setZones] = useState(null);
    const [pageSizeValue, setPageSizeValue] = useState(null);
    const [pageSizes, setPageSizes] = useState(null);
    const [fileSize, setFileSize] = useState(null);
    const [id, setId] = useState(null);

    useEffect(() => {
        dictionaryService.getSystemSettings()
        .then(data => {
            setFileSize(data.fileSize);
            setLanguageValue({
                name : data.language.name,
                code : data.language.id
            });
            setTimeZoneValue({
                name : data.defaultTimeZone.name,
                code : data.defaultTimeZone.id
            });
            setPageSizeValue({
                name : data.defaultPageSize.name,
                code : data.defaultPageSize.id
            });
            setId(data.id);
        });

        dictionaryService.getDictionaryItemsByCode("PGSZ")
        .then(data => {
            const options = data.map(d => ({
                name : d.name,
                code : d.id
            }));
            setPageSizes(options);
        });
        dictionaryService.getDictionaryItemsByCode("LNG")
        .then(data => {
            const options = data.map(d => ({
                name : d.name,
                code : d.id
            }));
            setLanguages(options);
        });
        dictionaryService.getDictionaryItemsByCode("ZON")
        .then(data => {
            const options = data.map(d => ({
                name : d.name,
                code : d.id
            }));
            setZones(options);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const submitSetting = (e) => {
        e.preventDefault();
        const settingJson = {
            language: languageValue.code,
            defaultTimeZone: timeZoneValue.code,
            defaultPageSize:pageSizeValue.code,
            fileSize:fileSize,
            id:id
        };
        dictionaryService.updateSystemSettings(settingJson)
        .then(response => {
            setFailed(false);
            setAlert("System Settings updated successfully!");
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to updated System settings! ");
        });
    }
    const pageSizeValues = pageSizes;
    const languageValues = languages;
    const timeZoneValues = zones;
    return (
        <div className="p-grid">
            <div className="p-col-12">
                <form onSubmit={submitSetting}>
                    <div className="card">
                        <h5>System Settings</h5>
                        { failed === false &&
                            <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                                <div className="p-message-wrapper">
                                    <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                    <span className="p-message-text">{alert}.</span>
                                </div>
                            </div>
                        }
                        { failed === true &&
                            <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                                <div className="p-message-wrapper">
                                    <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                    <span className="p-message-text">{alert}.</span>
                                </div>
                            </div>
                        }

                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-2 p-md-4">
                                <label htmlFor="code">File size limit(in MB)</label>
                                <InputText type="text" name="fileSize" value={fileSize} onChange={ev => setFileSize(ev.target.value)} required placeholder="" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-2 p-md-4">
                                <label htmlFor="code">Deafult Language</label>
                                <Dropdown value={languageValue} onChange={(e) => setLanguageValue(e.value)} options={languageValues} optionLabel="name" placeholder="Select" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-2 p-md-4">
                                <label htmlFor="code">Default Timezone</label>
                                <Dropdown value={timeZoneValue} onChange={(e) => setTimeZoneValue(e.value)} options={timeZoneValues} optionLabel="name" placeholder="Select" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-2 p-md-4">
                                <label htmlFor="code">Default Page Size</label>
                                <Dropdown value={pageSizeValue} onChange={(e) =>setPageSizeValue(e.value)} options={pageSizeValues} optionLabel="name" placeholder="Select" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="submit">&nbsp;</label>
                                <Button type="submit" label="Update System Settings" icon="pi pi-check" autoFocus={true}/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

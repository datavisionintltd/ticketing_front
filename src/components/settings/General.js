import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import DictionaryService from '../../service/DictionaryService';
import { Link } from 'react-router-dom';
import { Sidebar } from 'primereact/sidebar';
import { Dialog } from 'primereact/dialog';
import { Paginator } from 'primereact/paginator';


export const General = () => {
    //let params = useParams();
    const [setting, setSetting] = useState(null);
    const [id, setId] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [visibleRight, setVisibleRight] = useState(false);
    const [svalue, setValue] = useState("");
	const [skey, setKey] = useState("");
    const [name, setName] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [submitted, setSubmitted] = useState(false);

    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteDictionary, setDeleteDictionary] = useState(0);

    const settingService = new DictionaryService();

    useEffect(() => {
        const paramsJson = { name: globalFilter, page: pageIndex, size: pageSize, sort: sortField, order: sortOrder === 1 ? 'asc' : 'desc' };
        settingService.getSettings(paramsJson).then(data => {
            setSetting(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
        })
        .catch(error => {
            setSetting([]);
            setLoading(false);
        });
    }, [submitted]);

    const submitDictionary = (e) => {
        e.preventDefault();
        const settingJson = {
            id:id,
            svalue: svalue,
            skey: skey,
            name: name
        };
        settingService.editSetting(settingJson)
        .then(response => {
            setFailed(false);
            setAlert("setting Updated successfully!");
            setSubmitted(true);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update setting! " + error.error_description);
        });
    }
    const removeSetting=(e)=>{
        e.preventDefault();
        setDisplayConfirmation(false);
        settingService.deleteGeneralSetting(id)
        .then(response => {
            setFailed(false);
            setAlert("setting Deleted successfully!");
            setSubmitted(true);
            setSetting(response)
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete setting! " + error.error_description);
        });
    }
    const onDeleteButtonClick = (data) => {
        setDeleteDictionary(data.name);
        setId(id);
        setDisplayConfirmation(true);
    }
    const onEditButtonClick = (data) => {
        setId(data.id);
        setValue(data.svalue);
        setKey(data.skey);
        setName(data.name);
        setVisibleRight(true);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const settingTableHeader = (
        <div className="table-header">
           General Settings
            <div className="p-grid">
                <div className="p-col-6 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                {/* <div className="p-col-6 p-md-6">
                <Link to="setting/create" style={{float: "right"}}><Button  style={{float: "right"}}  label="Add Dictionary" icon="pi pi-unlock"/></Link>
                </div> */}
            </div>
        </div>
    );

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };
    const items = [
        {
            label: 'View',
            icon: 'pi pi-refresh'
        },
        {
            label: 'Edit',
            icon: 'pi pi-times'
        },
        {
            label: 'Delete',
            icon: 'pi pi-home'
        }
    ];
    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-pencil" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="Edit Dictionary" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} tooltip="Delete Dictionary"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeSetting(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">
                    { failed === false &&
                        <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                <span className="p-message-text">{alert}.</span>
                            </div>
                        </div>
                    }
                    { failed === true &&
                        <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                <span className="p-message-text">{alert}.</span>
                            </div>
                        </div>
                    }

                    {/* LIST ROLES DATATABLE */}
                    <DataTable value={setting} paginator={false} className="p-datatable-customers" rows={10} dataKey="id" rowHover
                        responsive={true} globalFilter={globalFilter} emptyMessage="No dictionaries found." loading={loading} header={settingTableHeader}>
                        <Column field="name" header="Name" sortable body={bodyTemplate}></Column>
                        <Column field="skey" header="Key" sortable body={bodyTemplate}></Column>
                        <Column field="svalue" header="Value" sortable body={bodyTemplate}></Column>
                        <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>
                    <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                        first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                    </Paginator>

                    {/* EDIT DICTIONARY RIGHT SIDE FORM */}
                    <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                        <form onSubmit={submitDictionary}>
                            <div className="p-grid m3">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit Setting</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Name</label>
                                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter name" />
                                            </div>
                                        </div>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Value</label>
                                                <InputText type="text" name="svalue" value={svalue} onChange={e => setValue(e.target.value)} required placeholder="Enter Value" />
                                            </div>
                                        </div>
                                        {/* <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Key</label>
                                                <InputText type="hidden" name="id" value={id} onChange={e => setKey(e.target.value)}  placeholder="" />
                                                <InputText type="text" name="skey" value={skey} onChange={e => setKey(e.target.value)} required placeholder="Enter name" />
                                            </div>
                                        </div>  */}
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE USER CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteDictionary}?</span>
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    )
}

import React, { useState, useEffect } from 'react';
import { format } from 'date-fns';
import ChartService from '../../service/ChartService';
import { Chart } from 'primereact/chart';
import { Button } from 'primereact/button';

export const BasicSupportStatus = () => {

    const [chartType, setChartType] = useState("line");
    const [statusJson, setStatusJson] = useState({labels: [], datasets: [{label: 'Tickets', data: [], fill: false, borderColor: '#8882bd', backgroundColor: 'rgb(136, 130, 189)'}]});
    const [since, setSince] = useState(new Date());
    const chartService = new ChartService();

    useEffect(() => {
        const thisTime = new Date();
        setSince(new Date(thisTime.getFullYear(), 0, 1));
        const newYear = format(new Date(thisTime.getFullYear(), 0, 1), "yyyy-MM-dd");
        chartService.getCharts(newYear).then(data => {
            setStatusJson({
                labels: data.statuses,
                datasets: [{label: 'Tickets', data: data.counts, fill: false, borderColor: '#8882bd', backgroundColor: 'rgb(136, 130, 189)'}]
            });
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const chartOptions = {
        title: {
          display: false,
          text: 'Tickets Statuses',
          fontSize: 16
        },
        legend: {
          position: 'bottom'
        },
        responsive: true,
        hoverMode: 'index',
        stacked: false,
        scales: {
            xAxes: [
                {
                  stacked: false,
                  ticks: {
                    autoSkip: false,
                    maxRotation: 30,
                    minRotation: 30
                  }
                }
            ],
            yAxes: [
              {
                type: 'linear',
                display: true,
                position: 'left',
                id: 'counts',
                stacked: false,
                gridLines: {
                  drawOnChartArea: true
                },
                ticks: {
                    min: 0
                }
              }
            ]
        },
        maintainAspectRatio: false
    };

    return (
        <div className="p-col-12 p-md-12 p-lg-12">
            <div className="card">
                <div className="p-col-12 p-md-12 p-lg-12 p-d-flex p-d-md-flex p-d-lg-flex">
                    <div className="p-col-8 p-md-8 p-lg-8">
                        <h6 className="centerText">Support Performance Starting From {format(since, 'MMMM d, yyyy')}</h6>
                    </div>
                    <div className="p-col-3 p-md-3 p-lg-3 p-text-right">
                        <Button icon="pi pi-chart-line" onClick={() => setChartType('line')} style={ chartType === 'line' ? { backgroundColor: '#8882bd', color: '#FFFFFF' } : { backgroundColor: '#FFFFFF', color: '#8882bd' } } />
                        <Button icon="pi pi-chart-bar" onClick={() => setChartType('bar')} style={ chartType === 'bar' ? { backgroundColor: '#8882bd', color: '#FFFFFF' } : { backgroundColor: '#FFFFFF', color: '#8882bd' } } />
                    </div>
                </div>
                <Chart type={chartType} data={statusJson} options={chartOptions} height="350px" />
            </div>
        </div>
    )
}

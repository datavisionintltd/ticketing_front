import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import  PaymentService from '../../service/PaymentService';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';

export const Transactions = () => {

    const [txns, setTxns] = useState(null);
    const [selectedTxn, setSelectedTxn] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const paymentService = new PaymentService();

	const history = useHistory();

    useEffect(() => {
        const paramsJson = { searchKey: "searchKey=" + globalFilter, page: "&page=" + pageIndex, size: "&size=" + pageSize, sort: "&sort=" + sortField, order: "&order=" + (sortOrder === 1 ? 'asc' : 'desc') };
        paymentService.getTransactions(paramsJson).then(data => {
            setTxns(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
        })
        .catch(error => {
            setTxns([]);
            setLoading(false);
        });
    }, []);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const viewLog = (e, data) => {
        e.preventDefault();
        history.push('/payments/transactions/logs/' + JSON.stringify(data.paymentLog));
    }

    const rolesTableHeader = (
        <div className="table-header">
            List of Transactions
            <div className="p-grid">
                <div className="p-col-6 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                {/* <div className="p-col-6 p-md-6">
                    <Button onClick={() => setVisibleRight(true)} style={{float: "right"}}  label="Add Role" icon="pi pi-unlock"/>
                </div> */}
            </div>
        </div>
    );

    const dateTemplate = (data) => {
        return new Intl.DateTimeFormat('sw', {year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(Date.parse(data['timestamp']));
    }

    const actionTemplate = (data) => {
        return (
            <>
                <Link to={"/payments/logs/" + data.paymentLog.id}><Button type="button" icon="pi pi-list" className="p-button-warning"style={{ marginRight: '.25em' }} tooltip="View Logs" /></Link>
            </>
        )
    };

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">
                    { failed === false &&
                        <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                <span className="p-message-text">{alert}.</span>
                            </div>
                        </div>
                    }
                    { failed === true &&
                        <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                <span className="p-message-text">{alert}.</span>
                            </div>
                        </div>
                    }

                    {/* LIST ROLES DATATABLE */}
                    <DataTable value={txns} paginator={false} className="p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedTxn} onSelectionChange={(e) => setSelectedTxn(e.value)}
                        sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                        globalFilter={globalFilter} emptyMessage="No roles found." loading={loading} header={rolesTableHeader}>
                        <Column field="timestamp" header="Request Time" sortable body={dateTemplate}></Column>
                        <Column field="amount" header="Amount" sortable></Column>
                        <Column field="charges" header="Charges" sortable></Column>
                        <Column field="msisdn" header="Phone Number" sortable></Column>
                        <Column field="reference" header="Reference" sortable></Column>
                        <Column field="receipt" header="Receipt" sortable></Column>
                        <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>
                    <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                        first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                    </Paginator>
                </div>
            </div>
        </div>
    )
}

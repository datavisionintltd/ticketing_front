import { InputTextarea } from 'primereact/inputtextarea';
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router';
import  PaymentService from '../../service/PaymentService';

export const TransactionsLogs = (props) => {

    const [txnsLgs, setTxnsLogs] = useState("");
    const { id } = useParams();

    const paymentService = new PaymentService();

    useEffect(() => {
        paymentService.getTransactionLog(id)
        .then(data => {
            setTxnsLogs(data.paymentJson);
        })
    }, []);

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{padding: "10%"}}>
                <div className="card">
                    {/* <h4 style={{textAlign: "center"}}>My Profile</h4> */}
                    <div className="divider-container"><div className="divider-border" />
                        <span className="divider-content">
                            <h4 style={{textAlign: "center", paddingBottom: "3%"}}>Transactions Logs</h4>
                        </span>
                    <div className="divider-border" /></div>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-col-12 p-md-12 p-grid" style={{paddingBottom: "2%", paddingTop: "2%"}}>
                            <div className="p-col-12 p-md-12">
                                {/* <span style={{float: "left", fontSize: "16px"}}>{txnsLgs}</span> */}
                                <InputTextarea id="textarea" rows="3" cols="30" value={txnsLgs}></InputTextarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

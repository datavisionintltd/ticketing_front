import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import CompanyService from '../../service/CompanyService';
import { MultiSelect } from 'primereact/multiselect';
import { useHistory } from 'react-router';
import { AddUser } from '../../components/user/AddUser'
import { ResponseAlert } from '../../utilities/components/ResponseAlert';

export const AddCompany = () => {

    const history = useHistory();

    const [companyJson, setCompanyJson] = useState({})
    const [adminJson, setAdminJson] = useState({})
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [list, setList] = useState(false)
    const [add, setAdd] = useState(false)

    const companyService = new CompanyService();

    const submitCompany = (e) => {
        e.preventDefault();
        console.log(companyJson, adminJson);
        companyService.addCompany({ company: companyJson, admin: adminJson }).then(res => {
            if (add) {
                setFailed(false);
                history.push('/clients/create');
            } else if (list) {
                setFailed(false);
                history.push('/clients');
            }
        })

    }

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{ padding: "2%" }}>
                <form onSubmit={submitCompany}>
                    <div className="card">
                        <h5>Add Client (Company) </h5>
                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                        <div className="p-grid">
                            <div className="p-col-6 p-formgrid">
                                <h6><strong>Company Details</strong></h6>
                                <hr/>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-offset-0 p-md-12">
                                        <label htmlFor="name">Name</label>
                                        <InputText type="text" name="name" value={companyJson.name} onChange={e => setCompanyJson({ ...companyJson, name: e.target.value })} required placeholder="Enter Name" />
                                    </div>
                                </div>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-offset-0 p-md-12">
                                        <label htmlFor="code">Phone</label>
                                        <InputText type="text" name="phone" value={companyJson.phone} onChange={e => setCompanyJson({ ...companyJson, phone: e.target.value })} required placeholder="Enter Phone" />
                                    </div>
                                </div>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-offset-0 p-md-12">
                                        <label htmlFor="name">Email</label>
                                        <InputText type="email" name="email" value={companyJson.email} onChange={e => setCompanyJson({ ...companyJson, email: e.target.value })} required placeholder="Enter Email" />
                                    </div>
                                </div>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-offset-0 p-md-12">
                                        <label htmlFor="code">Address</label>
                                        <InputText type="text" name="address" value={companyJson.address} onChange={e => setCompanyJson({ ...companyJson, address: e.target.value })} placeholder="Enter Address" />
                                    </div>
                                </div>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-offset-0 p-md-12">
                                        <label htmlFor="code">Website</label>
                                        <InputText type="text" name="website" value={companyJson.website} onChange={e => setCompanyJson({ ...companyJson, website: e.target.value })} placeholder="Enter Website" />
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-6">
                                <h6><strong>Admin Details</strong></h6>
                                <hr/>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-md-12">
                                        <label htmlFor="firstname2">Username</label>
                                        <InputText type="text" name="username" value={adminJson.username || ''} onChange={e => setAdminJson({ ...adminJson, username: e.target.value })} required placeholder="Enter Username" />
                                    </div>
                                </div>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-md-12">
                                        <label htmlFor="lastname2">Password</label>
                                        <InputText type="password" name="password" value={adminJson.password || ''} onChange={e => setAdminJson({ ...adminJson, password: e.target.value })} required placeholder="Enter Password" />
                                    </div>
                                </div>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-md-12">
                                        <label htmlFor="lastname2">Confirm</label>
                                        <InputText type="password" name="confirm" value={adminJson.confirm || ''} onChange={e => setAdminJson({ ...adminJson, confirm: e.target.value })} required placeholder="Confirm Password" />
                                    </div>
                                </div>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-md-12">
                                        <label htmlFor="firstname2">Full Name</label>
                                        <InputText type="text" name="fullName" value={adminJson.fullName || ''} onChange={e => setAdminJson({ ...adminJson, fullName: e.target.value })} required placeholder="Enter Full Name" />
                                    </div>
                                </div>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-md-12">
                                        <label htmlFor="firstname2">Email Address</label>
                                        <InputText type="text" name="email" value={adminJson.email || ''} onChange={e => setAdminJson({ ...adminJson, email: e.target.value })} required placeholder="Enter Email" />
                                    </div>
                                </div>
                                <div className="p-fluid p-formgrid p-grid">
                                    <div className="p-field p-col-12 p-md-12">
                                        <label htmlFor="firstname2">Phone Number</label>
                                        <InputText type="text" name="phoneNumber" value={adminJson.phoneNumber || ''} onChange={e => setAdminJson({ ...adminJson, phoneNumber: e.target.value })} required placeholder="Enter Phone Number" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <hr></hr>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="submit">&nbsp;</label>
                                <Button type="submit" label="Save" icon="pi pi-check" onClick={() => (setList(true))} autoFocus={true} />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="submit">&nbsp;</label>
                                <Button type="submit" label="Save and insert next" icon="pi pi-check" onClick={() => (setAdd(true))} />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

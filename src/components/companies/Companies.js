import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import CompanyService from '../../service/CompanyService';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';

export const Companies = () => {

    const [companies, setCompanies] = useState(null);
    const [selectedCompany, setSelectedCompany] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const [visibleRight, setVisibleRight] = useState(false);
    const [companyId, setCompanyId] = useState("");
    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");
    const [website, setWebsite] = useState("");
    const [logo, setLogo] = useState("");
    const [email, setEmail] = useState("");
    const [description, setDescription] = useState("");
    const [rolesValue, setRolesValue] = useState([]);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteCompany, setDeleteCompany] = useState(0);

    const companyService = new CompanyService();

    const getCompaniesList = () => {
        const paramsJson = { page: "page=" + pageIndex, size: "&size=" + pageSize, sort: "&sort=" + sortField, order: "&order=" + (sortOrder === 1 ? 'asc' : 'desc') };
        companyService.getCompanies(paramsJson).then(data => {
            setCompanies(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
        })
        .catch(error => {
            setCompanies([]);
            setLoading(false);
        });
    }

    useEffect(() => {
        getCompaniesList();
    }, [pageIndex, sortField, sortOrder, first, pageSize]);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const onStatusButtonClick = (data) => {
        const enableJson = {
            subscriptionId: data.id,
            enable: !data.enabled
        };
        CompanyService.enableCompany(enableJson)
        .then(response => {
            setFailed(false);
            setAlert(response.message + "!");
            getCompaniesList();
        }).catch(error => {
            setFailed(true);
            setAlert(error.message + "!");
        });
    }

    const onEditButtonClick = (data) => {
        setCompanyId(data.id);
        setName(data.name);
        setDescription(data.description);
        setPhone(data.phone);
        setEmail(data.email);
        setAddress(data.address);
        setWebsite(data.website);
        setVisibleRight(true);
    }

    const updateCompany = (e) => {
        e.preventDefault();
        const companyJson = {
            id: companyId,
            name: name,
            email:email,
            address:address,
            website:website,
            phone:phone
        }
        if (description !== "") {
            companyJson["description"] = description;
        }
        companyService.editCompany(companyJson, companyId)
        .then(response => {
            setFailed(false);
            setAlert("Company updated successfully!");
            getCompaniesList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update company!");
        });
    }

    const removeCompany = (e) => {
        e.preventDefault();
        setDisplayConfirmation(false);
        const subscriptionJson = {
            id: companyId
        }
        companyService.removeCompany(subscriptionJson, companyId)
        .then(response => {
            setFailed(false);
            setAlert("Company deleted successfully!");
            getCompaniesList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete company!");
            getCompaniesList();
        });
    }


    const onDeleteButtonClick = (data) => {
        setDeleteCompany(data.name);
        setCompanyId(data.id);
        setDisplayConfirmation(true);
    }

    const companiesTableHeader = (
        <div className="table-header">
            List of Companies
            <div className="p-grid">
                <div className="p-col-8 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-8 p-md-6">
                    <Link to="/tenants/create" style={{float: "right"}}><Button label="Add Company" icon="pi pi-user-plus"/></Link>
                </div>
            </div>
        </div>
    );

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const subscriptionBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Name</span>
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.name}</span>
            </>
        );
    };

    const statusBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Status</span>
                <span className={`customer-badge status-${data.enabled ? 'qualified' : 'unqualified'}`}>{data.enabled ? 'Enabled' : 'Disabled'}</span>
                <Link to="#" onClick={() => onStatusButtonClick(data)} className={`customer-badge status-${data.enabled ? 'unqualified' : 'qualified'}`}>
                    <i className={`pi pi-${data.enabled ? 'times' : 'check'}`}></i>
                </Link>
            </>
        )
    };

    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-user-edit" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="Edit Company" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} tooltip="Delete Company"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeCompany(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">

                    {/* RESPONSE ALERTS */}
                    { failed === false &&
                        <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                <span className="p-message-text">{alert}</span>
                            </div>
                        </div>
                    }
                    { failed === true &&
                        <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                <span className="p-message-text">{alert}</span>
                            </div>
                        </div>
                    }

                    {/* LIST SUBSCRIPTION DATATABLE */}
                    <DataTable value={companies} paginator={false} className="p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedCompany} onSelectionChange={(e) => setSelectedCompany(e.value)}
                        sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                        globalFilter={globalFilter} emptyMessage="No companies found." loading={loading} header={companiesTableHeader}>
                        <Column selectionMode="multiple" headerStyle={{ width: '3em' }}></Column>
                        <Column field="name" header="Name" sortable body={bodyTemplate}></Column>
                        <Column field="phone" header="Phone" sortable body={bodyTemplate}></Column>
                        <Column field="email" header="Email" sortable body={bodyTemplate}></Column>
                        <Column field="address" header="Address" sortable body={bodyTemplate}></Column>
                        <Column field="website" header="Website" sortable body={bodyTemplate}></Column>
                        <Column header="Actions" headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>
                    <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                        first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                    </Paginator>

                    {/* EDIT SUBSCRIPTION RIGHT SIDE FORM */}
                    <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                        <form onSubmit={updateCompany}>
                            <div className="p-grid">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit Company</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Name</label>
                                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter Company Name" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="description">Phone</label>
                                                <InputText type="text" name="phone" value={phone} onChange={ev => setPhone(ev.target.value)} required placeholder="Enter Phone" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Email</label>
                                                <InputText type="email" name="email" value={email} onChange={e => setEmail(e.target.value)} required placeholder="Enter Email" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="description">address</label>
                                                <InputText type="text" name="address" value={address} onChange={ev => setAddress(ev.target.value)}  placeholder="Enter Address" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="description">Website</label>
                                                <InputText type="text" name="website" value={website} onChange={ev => setWebsite(ev.target.value)}  placeholder="Enter Website" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE SUBSCRIPTION CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteCompany}?</span>
                        </div>
                    </Dialog>

                </div>
            </div>
        </div>
    )
}

import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import ClientService from '../../service/ClientService';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';

export const ListClients = () => {

    const [clients, setClients] = useState([]);
    const [selectedClient, setSelectedClient] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const [clientJson, setClientJson] = useState({name: "", email: "N/A", phone: "N/A"});
    const [displayForm, setDisplayForm] = useState(false);
    const [visibleRight, setVisibleRight] = useState(false);
    const [clientId, setClientId] = useState(0);
    const [submitted, setSubmitted] = useState(false);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteUser, setDeleteUser] = useState(0);

    const clientService = new ClientService();

    const getClientsList = () => {
        const paramsJson = { name: globalFilter, page: pageIndex, size: pageSize, sort: sortField, order: (sortOrder === 1 ? 'asc' : 'desc') };
        clientService.getClients(paramsJson).then(data => {
            setClients(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
            setClientId(0);
        })
        .catch(error => {
            setClients([]);
            setLoading(false);
        });
    }

    useEffect(() => {
        getClientsList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pageIndex, sortField, sortOrder, first, pageSize, globalFilter, submitted]);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const onChangeGlobalFilter = (e) => {
        e.preventDefault();
        setGlobalFilter(e.target.value);
    }

    const submitClient = (e) => {
        e.preventDefault();
        clientService.addClient(clientJson)
        .then(response => {
            setClientId(0);
            setClientJson({name: "", email: "N/A", phone: "N/A"});
            setFailed(false);
            setAlert("Customer added successfully!");
            setSubmitted(true);
            setDisplayForm(false);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add customer!");
        });
    }

    const onEditButtonClick = (data) => {
        setClientJson({id: data.id, name: data.name, email: data.email, phone: data.phone});
        setVisibleRight(true);
    }

    const updateClient = (e) => {
        e.preventDefault();
        clientService.editClient(clientJson)
        .then(response => {
            setFailed(false);
            setAlert("Customer updated successfully!");
            setSubmitted(true);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update customer!");
        });
    }

    const onDeleteButtonClick = (data) => {
        setDeleteUser(data.name);
        setClientId(data.id);
        setDisplayConfirmation(true);
    }

    const removeClient = (e) => {
        e.preventDefault();
        setDisplayConfirmation(false);
        clientService.removeClient(clientId)
        .then(response => {
            setFailed(false);
            setAlert("Customer deleted successfully!");
            getClientsList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete customer!");
        });
    }

    const usersTableHeader = (
        <div className="table-header">
            List of Customers
            <div className="p-grid">
                <div className="p-col-8 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => onChangeGlobalFilter(e)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-8 p-md-6">
                    <Link to="#" style={{float: "right"}}><Button label="Add Customer" onClick={() => setDisplayForm(true)} icon="pi pi-user-plus"/></Link>
                </div>
            </div>
        </div>
    );

    const dialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="submit" label="Save" onClick={(e) => submitClient(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const userBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Customer</span>
                {/* <img alt={data.fullName} src={`assets/layout/images/avatar/user.png`} width="32" style={{ verticalAlign: 'middle' }} /> */}
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.name}</span>
            </>
        );
    };

    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-user-edit" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} title="Edit Customer" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} title="Delete Customer"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeClient(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">

                {/* RESPONSE ALERTS */}
                <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />

                {/* LIST CLIENT DATATABLE */}
                { clients.length > 0 &&
                    <div className="card">
                        <DataTable value={clients} paginator={false} className="p-datatable-gridlines p-datatable-striped p-datatable-sm p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedClient} onSelectionChange={(e) => setSelectedClient(e.value)}
                            sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                            globalFilter={globalFilter} emptyMessage="No users found." loading={loading} header={usersTableHeader}>
                            <Column field="name" header="Customer" sortable body={userBodyTemplate}></Column>
                            <Column field="email" header="Email Address" sortable body={bodyTemplate}></Column>
                            <Column field="phone" header="Phone Number" sortable body={bodyTemplate}></Column>
                            <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                        </DataTable>
                        <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                            first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                            paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                        </Paginator>
                    </div>
                }
                { clients.length <= 0 &&
                    <div className="p-col-12 p-pl-3 p-text-center">
                        <h5 className="p-text-bold p-text-center">You don't have any customer</h5>
                        <span>For your teams to be able to submit inquiries, there must be some customers associated with your products where issues arise from.</span>
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                            <img src="assets/layout/images/dashboard/clients.svg" alt="Max Desk" className="logo" style={{width: "20%"}} />
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="button" onClick={() => setDisplayForm(true)} label="Add Customer Now" icon="pi pi-users" />
                        </div>
                    </div>
                }

                {/* EDIT CLIENT RIGHT SIDE FORM */}
                <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                    <form onSubmit={updateClient}>
                        <div className="p-grid">
                            <div className="p-col-12">
                                <div className="card">
                                    <h5>Edit Customer</h5>
                                    <div className="p-fluid p-formgrid p-grid">
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="name">Customer name</label>
                                            <InputText type="text" name="name" value={clientJson.name} onChange={e => setClientJson({...clientJson, name: e.target.value})} required placeholder="Enter customer name" />
                                        </div>
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="email">Email</label>
                                            <InputText type="text" name="email" value={clientJson.email} onChange={e => setClientJson({...clientJson, email: e.target.value})} required placeholder="Enter Email Address" />
                                        </div>
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="phone">Phone Number</label>
                                            <InputText type="text" name="phone" value={clientJson.phone} onChange={e => setClientJson({...clientJson, phone: e.target.value})} required placeholder="Enter Phone Number" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                            <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                        </div>
                    </form>
                </Sidebar>

                {/* DELETE CLIENT CONFIRMATION DIALOG */}
                <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                        <span>Are you sure you want to delete {deleteUser}?</span>
                    </div>
                </Dialog>

                {/* ADD CLIENT DIALOG */}
                <Dialog header="New Customer" visible={displayForm} className="p-col-4" modal={true} footer={dialogFooter} onHide={() => setDisplayForm(false)} maximizable={true} blockScroll={true}>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="name">Customer name</label>
                            <InputText type="text" name="name" value={clientJson.name} onChange={e => setClientJson({...clientJson, name: e.target.value})} required placeholder="Enter customer name" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="email">Email</label>
                            <InputText type="text" name="email" value={clientJson.email} onChange={e => setClientJson({...clientJson, email: e.target.value})} required placeholder="Enter Email Address" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="phone">Phone Number</label>
                            <InputText type="text" name="phone" value={clientJson.phone} onChange={e => setClientJson({...clientJson, phone: e.target.value})} required placeholder="Enter Phone Number" />
                        </div>
                    </div>
                </Dialog>
            </div>
        </div>
    )
}

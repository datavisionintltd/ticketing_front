import React, { useEffect, useState } from 'react';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { ClientsDashboard } from './home/ClientsDashboard';
import { ProjectsDashboard } from './home/ProjectsDashboard';
import { AgentsDashboard } from './home/AgentsDashboard';
import { ResponseAlert } from '../utilities/components/ResponseAlert';
import { BasicSupportStatus } from './charts/BasicSupportStatus';
import { Link, useHistory } from 'react-router-dom';
import { hasRole } from '../utilities/Helpers';

export const Dashboard = (props) => {

    const history = useHistory();

    const [displayPasswordForm, setDisplayPasswordForm] = useState(false);
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    useEffect(() => {
        if (props.notSecured === "false") {
            setDisplayPasswordForm(true);
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[]);

    const resetPassword = () => {
        history.push("/user/reseting/password");
    }

    const panelHeader = <span className="p-text-center"><i className="pi pi-exclamation-triangle" style={{color: "#E00000"}}></i>&nbsp;&nbsp;&nbsp;Your account is not safe</span>;

    const dialogFooter = <div className="p-grid">
        <div className="p-col-6 p-text-right">
            <Button type="button" label="Reset now" onClick={() => resetPassword()} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-6 p-text-left">
            <Button type="button" label="Ask me later" onClick={() => setDisplayPasswordForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    return (
        <div className="p-grid dashboard">
            {/* { props.notSecured && setDisplayPasswordForm(true) } */}
            <div className="p-col-12 loader"><span></span></div>
            <div className="p-col-12 p-md-12 p-lg-12">
                <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
            </div>
            { hasRole("ROLE_ADMIN") &&
                <ClientsDashboard setFailed={setFailed} setAlert={setAlert} />
            }
            { hasRole("ROLE_ADMIN") &&
                <ProjectsDashboard setFailed={setFailed} setAlert={setAlert} />
            }
            { hasRole("ROLE_ADMIN") &&
                <AgentsDashboard setFailed={setFailed} setAlert={setAlert} />
            }
            { hasRole("ROLE_USER") &&
                <BasicSupportStatus />
            }
            <Dialog header={panelHeader} visible={displayPasswordForm} className="p-col-12 p-md-6 p-lg-6 p-md-offset-1 p-lg-offset-2" footer={dialogFooter} modal onHide={() => setDisplayPasswordForm(false)}>
                <div className="p-fluid p-formgrid p-grid">
                    <div className="p-field p-col-12 p-md-12 p-text-center">
                        <Link to="/user/reseting/password">
                            <strong>
                                <span className="p-text-secondary">Reset password to secure your account</span>
                            </strong>
                        </Link>
                    </div>
                </div>
            </Dialog>
        </div>
    )

}

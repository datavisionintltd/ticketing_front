import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { InputNumber } from 'primereact/inputnumber';
import { Button } from 'primereact/button';
import { format } from 'date-fns';
import DictionaryService from '../../service/DictionaryService';
import { useHistory } from 'react-router';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import { ACCESS_TOKEN } from '../../constants';

export const QuickSla = () => {

	const history = useHistory();

    const [slas, setSlas] = useState([{name: "High Risk", gracePeriod: 1, description: "N/A"}, {name: "Low Risk", gracePeriod: 3, description: "N/A"}]);
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const dictionaryService = new DictionaryService();

    useEffect(() => {
        if (!localStorage.getItem(ACCESS_TOKEN)) {
            history.push("/login");
        }
        window.addEventListener("popstate", () => {
            history.go(1);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitSla = (e) => {
        e.preventDefault();
        dictionaryService.addSlas(slas)
        .then(response => {
            setFailed(false);
            setAlert("Slas added successfully!");
            history.push('/quick/departments');
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add sla! " + error.error_description);
        });
    }

    const handleSlas = (e, index) => {
        const items = [...slas];
        items[index][e.target.name] = e.target.value;
        setSlas(items);
    }

    const addSla = () => {
        setSlas(prevSlas => [...prevSlas, {name: "", gracePeriod: 1, description: "N/A"}]);
    }

    const removeSla = (index) => {
        if (slas.length > 1) {
            setSlas(slas.filter((item, i) => i !== index));
        }
    }

    return (
        <div className="p-grid">
            <div className="p-grid p-col-12 p-md-12 p-lg-12 p-text-left p-pt-6 p-pt-md-6 p-pt-lg-6" >
                <div className="p-col-12 p-md-3 p-lg-3 p-text-center">
                    <img src="assets/layout/images/logo.png" alt="Logo" className="logo" style={{ width: "20%" }} />
                </div>
            </div>
            <div className="p-col-12 p-md-offset-4 p-md-4 p-lg-offset-4 p-lg-4">
                <h3 style={{textAlign: "center", padding: "0.5em"}}>In hours, how long will it take for your team to solve complaints?</h3>
                <form onSubmit={submitSla}>
                    <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                    { slas.map((item, index) => (
                        <div className="p-fluid p-formgrid p-grid p-col-12 p-pb-3" key={index}>
                            <div className="p-col-6 p-md-5">
                                <InputText type="text" data-id={index} name="name" value={item.name} onChange={e => handleSlas(e, index)} required placeholder="Enter Priority Policy" className="p-inputtext p-inputtext-sm" />
                            </div>
                            {slas.length === 1 &&
                                <div className="p-col-6 p-md-6 p-d-flex">
                                    <InputNumber data-id={index} name="gracePeriod" value={item.gracePeriod} onValueChange={(e) => handleSlas(e, index)} showButtons mode="decimal" className="p-inputnumber p-inputnumber-sm" />
                                </div>
                            }
                            {slas.length > 1 &&
                                <div className="p-col-4 p-md-5 p-d-flex">
                                    <InputNumber data-id={index} name="gracePeriod" value={item.gracePeriod} onValueChange={(e) => handleSlas(e, index)} showButtons mode="decimal" className="p-inputnumber p-inputnumber-sm" />
                                </div>
                            }
                            {slas.length > 1 &&
                                <div className="p-col-2 p-md-1">
                                    <Button type="button" icon="pi pi-minus" onClick={() => removeSla(index)} className="p-button p-button-sm"/>
                                </div>
                            }
                        </div>
                    ))}
                    <div className="p-fluid p-formgrid p-grid p-col-12">
                        <div className="p-col-6 p-md-5 p-d-flex">
                            <Button type="button" icon="pi pi-plus" className="p-button-sm" label="Add another priority" onClick={addSla}/>
                        </div>
                    </div>
                    {/* <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-offset-4 p-md-4">
                            <label htmlFor="submit">&nbsp;</label>
                            <Button type="submit" label="Save" icon="pi pi-check" autoFocus={true}/>
                        </div>
                    </div> */}
                    <div className="p-col-12 p-text-center p-pt-5 p-pt-md-5 p-pt-lg-5">
                        <Button type="submit" label="Save" className="p-button-sm button-rounded p-col-2 p-md-2 p-lg-2" style={{ marginRight: '.25em', backgroundColor: "#333333" }} autoFocus={true} />
                    </div>
                </form>
            </div>
            <div className="p-grid p-col-12 p-md-12 p-lg-12 p-text-center" style={{position: "absolute", bottom: "0"}}>
                <span className="p-text-center p-offset-4 p-col-4 p-md-offset-4 p-md-4 p-lg-offset-4 p-lg-4" style={{color: "#8882bd"}}><small>&copy; {format(new Date(), "yyyy")}, Software Galaxy. All rights reserved.</small></span>
            </div>
        </div>
    )
}

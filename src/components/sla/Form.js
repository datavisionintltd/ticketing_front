import { InputText } from 'primereact/inputtext';
import { Calendar } from 'primereact/calendar';
import { InputTextarea } from 'primereact/inputtextarea';
import { InputNumber } from 'primereact/inputnumber';
import { useHistory } from 'react-router';
import React, { useState } from 'react';

export const Form = (props) => {
    const history = useHistory();

    const [name, setName] = useState("");
	const [gracePeriod, setGracePeriod] = useState("");
    const [description, setDescription] = useState("");
    const [dueDate, setCalendarValue] = useState(null);
 
return (
<>                             <div className="p-fluid p-formgrid p-grid"> 
                            <div className="p-field p-col-12  p-md-12">
                                <label htmlFor="name">Name</label>
                                <InputText type="text" name="name" value={props.sla.name} onChange={e => setName(e.target.value)} required placeholder="Enter Name" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-0 p-md-12">
                                <label htmlFor="code">Due date</label>
                                <Calendar showIcon name="dueDate" showButtonBar value={dueDate} onChange={(e) => setCalendarValue(e.value)}></Calendar>
                            </div>
                        </div>
                    <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-12">
                                <label htmlFor="code">Grace Period</label>
                                <InputNumber name="gracePeriod" value={gracePeriod} onValueChange={(e) => setGracePeriod(e.value)} showButtons mode="decimal"></InputNumber>
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12  p-md-12">
                                <label htmlFor="code">Description</label>
                                <InputTextarea name="description" value={description} onChange={ev => setDescription(ev.target.value)} placeholder="Enter Description" autoResize rows="3" cols="30" />

                            </div>
                        </div>
                        
                                       
</>
);
}

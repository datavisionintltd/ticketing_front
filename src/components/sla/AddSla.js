import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Calendar } from 'primereact/calendar';
import { InputTextarea } from 'primereact/inputtextarea';
import { InputNumber } from 'primereact/inputnumber';
import { Button } from 'primereact/button';
import { format } from 'date-fns';
import DictionaryService from '../../service/DictionaryService';
import { useHistory } from 'react-router';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';

export const AddSla = () => {

	const history = useHistory();

    const [name, setName] = useState("");
	const [gracePeriod, setGracePeriod] = useState(0);
    const [alert, setAlert] = useState("");
    const [description, setDescription] = useState("");
    const [failed, setFailed] = useState(null);
    const [dueDate, setCalendarValue] = useState(new Date());
    const [inputNumberValue, setInputNumberValue] = useState(null);
    const dictionaryService = new DictionaryService();

    const submitSla = (e) => {
        e.preventDefault();
        const dictionaryJson = {
            name: name,
            gracePeriod: gracePeriod,
            description: description
        };
        dictionaryService.addSla(dictionaryJson)
        .then(response => {
            setFailed(false);
            setAlert("Sla added successfully!");
            history.push('/settings/slas');
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add sla! " + error.error_description);
        });
    }

    return (
        <div className="p-grid">
            <div className="p-col-12">
                <form onSubmit={submitSla}>
                    <div className="card">
                        <div className="p-card-title" style={{backgroundColor: "#8882BD", color: "#FFFFFF"}}>
                            <h5 style={{textAlign: "center", padding: "0.5em"}}>New Priority</h5>
                        </div>
                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-2 p-md-4">
                                <label htmlFor="name">Priority Name</label>
                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter Name" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="code">Due Time</label>
                                <InputNumber name="gracePeriod" value={gracePeriod} onValueChange={(e) => setGracePeriod(e.value)} showButtons mode="decimal"></InputNumber>
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-2 p-md-8">
                                <label htmlFor="code">Description</label>
                                <InputTextarea name="description" value={description} onChange={ev => setDescription(ev.target.value)} placeholder="Enter Description" autoResize rows="3" cols="30" />

                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-4 p-md-4">
                                <label htmlFor="submit">&nbsp;</label>
                                <Button type="submit" label="Add Priority" icon="pi pi-check" autoFocus={true}/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import ProjectService from '../../service/ProjectService';
import ClientService from '../../service/ClientService';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';

export const ListProjects = () => {

    const [projects, setProjects] = useState([]);
    const [selectedProject, setSelectedProject] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const [projectJson, setProjectJson] = useState({name: "", clientId: 0, description: "N/A"});
    const [displayProjectForm, setDisplayProjectForm] = useState(false);
    const [clientList, setClientList] = useState([]);

    const [projectId, setProjectId] = useState("");

    const [visibleRight, setVisibleRight] = useState(false);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteProject, setDeleteProject] = useState(0);

    const projectService = new ProjectService();
    const clientService = new ClientService();

    const getProjectsList = () => {
        const paramsJson = { page: pageIndex, size: pageSize, sort: sortField, order: (sortOrder === 1 ? 'asc' : 'desc') };
        projectService.getProjects(paramsJson).then(data => {
            setProjects(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
        })
        .catch(error => {
            setProjects([]);
            setLoading(false);
        });
    }

    const getClients = () => {
        const listParamsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        clientService.getClients(listParamsJson).then(data => {
            setClientList(data.content);
        });
    }

    useEffect(() => {
        getProjectsList();
        getClients();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pageIndex, sortField, sortOrder, first, pageSize]);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const addProjectButton = () => {
        setDisplayProjectForm(true);
    }

    const submitProject = (e) => {
        e.preventDefault();
        projectService.addProject(projectJson)
        .then(response => {
            setFailed(false);
            setAlert("Product added successfully!");
            setDisplayProjectForm(false);
            getProjectsList();
            setDisplayProjectForm(false);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add product! " + error.error_description);
            setDisplayProjectForm(false);
        });
    }

    const onEditButtonClick = (data) => {
        setProjectJson({id: data.id, name: data.name, clientId: data.client.id, description: data.description});
        setVisibleRight(true);
    }

    const editProject = (e) => {
        e.preventDefault();
        projectService.editProject(projectJson)
        .then(response => {
            setFailed(false);
            setAlert("Product updated successfully!");
            getProjectsList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update product! " + error.error_description);
        });
    }

    const onDeleteButtonClick = (data) => {
        setDeleteProject(data.name);
        setProjectId(data.id);
        setDisplayConfirmation(true);
    }

    const removeProject = (e) => {
        e.preventDefault();
        setDisplayConfirmation(false);
        projectService.removeProject(projectId)
        .then(response => {
            setFailed(false);
            setAlert("Product deleted successfully!");
            getProjectsList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete product!");
            getProjectsList();
        });
    }

    const projectsTableHeader = (
        <div className="table-header">
            Products
            <div className="p-grid">
                <div className="p-col-8 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-8 p-md-6">
                    <Link to="#" style={{float: "right"}}><Button label="Add Product" onClick={() => addProjectButton()} icon="pi pi-project-plus"/></Link>
                </div>
            </div>
        </div>
    );

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const clientTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data.client != null ? data.client.name : 'Public'}
            </>
        );
    };

    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-pencil" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="Edit Product" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} tooltip="Delete Product"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeProject(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    const dialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="submit" label="Save" onClick={(e) => submitProject(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayProjectForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                {/* RESPONSE ALERTS */}
                <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />

                {/* LIST PROJECT DATATABLE */}
                { projects.length > 0 &&
                    <div  className="card">
                        <DataTable value={projects} paginator={false} className="p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedProject} onSelectionChange={(e) => setSelectedProject(e.value)}
                            sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                            globalFilter={globalFilter} emptyMessage="No products found." loading={loading} header={projectsTableHeader}>
                            <Column field="name" header="Product" sortable body={bodyTemplate}></Column>
                            <Column field="client" header="Customer" sortable body={clientTemplate}></Column>
                            <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                        </DataTable>
                        <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                            first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                            paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                        </Paginator>
                    </div>
                }
                { projects.length <= 0 &&
                    <div className="p-col-12 p-pl-3 p-text-center">
                        <h5 className="p-text-bold p-text-center">You don't have any product</h5>
                        <span>For your teams to be able to submit tickets, there must be some products to be selected so that inquiries can be identified.</span>
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                            <img src="assets/layout/images/dashboard/projects.svg" alt="Max Desk" className="logo" style={{width: "20%"}} />
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="button" onClick={() => addProjectButton()} label="Add Product Now" icon="pi pi-plus-circle" />
                        </div>
                    </div>
                }

                {/* EDIT PROJECT RIGHT SIDE FORM */}
                <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                    <form onSubmit={editProject}>
                        <div className="p-grid">
                            <div className="p-col-12">
                                <div className="card">
                                    <h5>Edit Product</h5>
                                    <div className="p-fluid p-formgrid p-grid">
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="name">Product name</label>
                                            <InputText type="text" name="name" value={projectJson.name} onChange={e => setProjectJson({...projectJson, name: e.target.value})} required placeholder="Enter product name" />
                                        </div>
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="client">Customer</label>
                                            <Dropdown id="client" name="client" value={projectJson.clientId} onChange={(e) => setProjectJson({...projectJson, clientId: e.value})} options={clientList} optionLabel="name" optionValue="id" filter filterBy="name" required placeholder="Select Customer" appendTo={document.body}></Dropdown>
                                        </div>
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="description">Other Details</label>
                                            <InputText type="text" name="description" value={projectJson.description} onChange={e => setProjectJson({...projectJson, description: e.target.value})} required placeholder="Enter Description" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                            <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                        </div>
                    </form>
                </Sidebar>

                {/* DELETE PROJECT CONFIRMATION DIALOG */}
                <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                        <span>Are you sure you want to delete {deleteProject}?</span>
                    </div>
                </Dialog>
                <Dialog header="New Product" visible={displayProjectForm} className="p-col-4 p-offset-2" modal footer={dialogFooter} onHide={() => setDisplayProjectForm(false)}>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="name">Product name</label>
                            <InputText type="text" name="name" value={projectJson.name} onChange={e => setProjectJson({...projectJson, name: e.target.value})} required placeholder="Enter product name" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="client">Customer</label>
                            <Dropdown id="client" name="client" value={projectJson.clientId} onChange={(e) => setProjectJson({...projectJson, clientId: e.value})} options={clientList} optionLabel="name" optionValue="id" filter filterBy="name" required placeholder="Select Customer" appendTo={document.body}></Dropdown>
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="description">Other Details</label>
                            <InputText type="text" name="description" value={projectJson.description} onChange={e => setProjectJson({...projectJson, description: e.target.value})} required placeholder="Enter Description" />
                        </div>
                    </div>
                </Dialog>
            </div>
        </div>
    )
}

import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import ProjectService from '../../service/ProjectService';
import { useHistory } from 'react-router';

export const CreateProject = () => {

    const history = useHistory();

    const [name, setName] = useState("")

    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const projectService = new ProjectService();

    useEffect(() => {
    }, []);

    const submitProject = (e) => {
        e.preventDefault();
        const projectjson = {
            name: name
        };
        projectService.addProject(projectjson)
        .then(response => {
            setFailed(false);
            setAlert("Project added successfully!");
            history.push('/projects/');
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add project! " + error.error_description);
        });
    }

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{ padding: "2%" }}>
                <form onSubmit={submitProject}>
                    <div className="card">
                        <h5>Add New Project</h5>
                        {failed === false &&
                            <div className="p-message p-component p-message-success" style={{ margin: '0 0 1em 0', display: 'block' }}>
                                <div className="p-message-wrapper">
                                    <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                    <span className="p-message-text">{alert}.</span>
                                </div>
                            </div>
                        }
                        {failed === true &&
                            <div className="p-message p-component p-message-warn" style={{ margin: '0 0 1em 0', display: 'block' }}>
                                <div className="p-message-wrapper">
                                    <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                    <span className="p-message-text">{alert}.</span>
                                </div>
                            </div>
                        }
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-12">
                                <label htmlFor="firstname2">Project name</label>
                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter project name" />
                            </div>
                            <div className="p-col-12 p-md-2">
                                <Button type="submit" label="Add Project" icon="pi pi-check" autoFocus={true} />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

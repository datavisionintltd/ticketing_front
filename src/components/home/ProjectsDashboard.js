import { DataView } from 'primereact/dataview';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Panel } from 'primereact/panel';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import ProjectService from '../../service/ProjectService';
import ClientService from '../../service/ClientService';
import { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

export const ProjectsDashboard = props => {

    const history = useHistory();

    const [layout, setLayout] = useState('list');

    const [projects, setProjects] = useState([]);
    const [loading, setLoading] = useState(true);

    const [projectJson, setProjectJson] = useState({name: "", clientId: 0, description: "N/A"});
    const [displayProjectForm, setDisplayProjectForm] = useState(false);
    const [clientList, setClientList] = useState([]);

    const projectService = new ProjectService();
    const clientService = new ClientService();

    const getProjects = () => {
        const paramsJson = { page: 0, size: 5, sort: "status", order: "desc" };
        projectService.getProjectTicketStatuses(paramsJson).then(data => {
            setProjects(data);
            setLoading(false);
        });
    }

    const getClients = () => {
        const listParamsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        clientService.getClients(listParamsJson).then(data => {
            setClientList(data.content);
        });
    }

    useEffect(() => {
        getProjects();
        getClients();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitProject = (e) => {
        e.preventDefault();
        projectService.addProject(projectJson)
        .then(response => {
            props.setFailed(false);
            props.setAlert("Product added successfully!");
            setDisplayProjectForm(false);
            history.push("/projects");
        }).catch(error => {
            props.setFailed(true);
            props.setAlert("Failed to add product! " + error.error_description);
            setDisplayProjectForm(false);
        });
    }

    const addProjectButton = () => {
        setDisplayProjectForm(true);
        getClients();
    }

    const getStatusCounts = (data) => {
        let statusCounts = [];
        for (var i = 0; i < data.length; i++ ) {
            statusCounts.push(<div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1 p-text-center" key={[i]}  style={{color: data[i].status === "opened" ? "#8882BD" : (data[i].status === "pending" ? "#D32F2F" : (data[i].status === "closed" ? "#689F38" : ""))}}>
                {data[i].count}
                </div>);
        }
        return statusCounts;
    }

    const dataView = (data) => (
        <div className="p-col-12 p-grid p-fluid p-pt-1 p-pl-1">
            <div className="p-fluid p-col-6 p-md-6 p-lg-6">{data.project.name}</div>
            <div className="p-fluid p-grid p-col-6 p-md-6 p-lg-6">{getStatusCounts(data.statusCounts)}</div>
        </div>
    );

    const itemTemplate = (data, layout) => {
        if (!data) {
            return;
        }
        if (layout === 'list') {
            return dataView(data);
        }
    };

    const dialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="submit" label="Save" onClick={(e) => submitProject(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayProjectForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    return (
        <div className="p-col-12 p-md-6 p-lg-4 task-list">
            <Panel header="Complaints are raised based on your products">
                { projects.length > 0 &&
                    <div className="p-col-12" style={{maxHeight: '300px', overflow: "auto"}}>
                        <div className="p-col-12 p-pt-2 p-d-flex">
                            <div className="p-col-8 p-md-8 p-lg-8 p-text-left">
                                <h5 className="p-text-bold">Products' Complaints</h5>
                            </div>
                            <div className="p-col-4 p-md-4 p-lg-4 p-text-right">
                                <Link to="/tickets" className="p-button p-button-link" title="More Complaints"><i className="p-text-bold pi pi-ellipsis-v"></i></Link>
                            </div>
                        </div>
                        <div className="p-col-12 p-grid p-fluid p-pt-1 p-text-center">
                            <div className="p-fluid p-col-6 p-md-6 p-lg-6"></div>
                            <div className="p-fluid p-grid p-col-6 p-md-6 p-lg-6">
                                <div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1" style={{color: "#8882BD"}}><small>Opened</small></div>
                                <div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1" style={{color: "#D32F2F"}}><small>Pending</small></div>
                                <div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1" style={{color: "#689F38"}}><small>Closed</small></div>
                            </div>
                        </div>
                        <DataView value={projects} layout={layout} rows={5} itemTemplate={itemTemplate} lazy loading={loading}></DataView>
                        <div className="p-col-12 p-text-center p-pt-5">
                            <Button type="button" onClick={() => addProjectButton()} label="Add More Products" icon="pi pi-plus-circle" />
                        </div>
                    </div>
                }
                { projects.length <= 0 &&
                    <div className="p-col-12 p-pl-3" style={{maxHeight: '300px', overflow: "auto"}}>
                        <h5 className="p-text-bold p-text-center">You don't have any product</h5>
                        <span>For your teams to be able to submit tickets, there must be some products to be selected so that inquiries can be identified.</span>
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                            <img src="assets/layout/images/dashboard/projects.svg" alt="Max Desk" className="logo" style={{width: "40%"}} />
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="button" onClick={() => addProjectButton()} label="Add Product Now" icon="pi pi-plus-circle" />
                        </div>
                    </div>
                }
            </Panel>
            <Dialog header="New Product" visible={displayProjectForm} className="p-col-4 p-offset-2" modal footer={dialogFooter} onHide={() => setDisplayProjectForm(false)}>
                <div className="p-fluid p-formgrid p-grid">
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="name">Product name</label>
                        <InputText type="text" name="name" value={projectJson.name} onChange={e => setProjectJson({...projectJson, name: e.target.value})} required placeholder="Enter product name" />
                    </div>
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="client">Customer</label>
                        <Dropdown id="client" name="client" value={projectJson.clientId} onChange={(e) => setProjectJson({...projectJson, clientId: e.value})} options={clientList} optionLabel="name" optionValue="id" filter filterBy="name" required placeholder="Select Customer" appendTo={document.body}></Dropdown>
                    </div>
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="description">Other Details</label>
                        <InputText type="text" name="description" value={projectJson.description} onChange={e => setProjectJson({...projectJson, description: e.target.value})} required placeholder="Enter Description" />
                    </div>
                </div>
            </Dialog>
        </div>
    )
}

import { DataView } from 'primereact/dataview';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Panel } from 'primereact/panel';
import { Button } from 'primereact/button';
import ClientService from '../../service/ClientService';
import { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

export const ClientsDashboard = props => {

    const history = useHistory();

    const [layout, setLayout] = useState('list');

    const [clients, setClients] = useState([]);
    const [loading, setLoading] = useState(true);

    const [clientJson, setClientJson] = useState({name: "", email: "N/A", phone: "N/A"});
    const [displayClientForm, setDisplayClientForm] = useState(false);

    const clientService = new ClientService();

    const getClients = () => {
        const paramsJson = { page: 0, size: 5, sort: "status", order: "desc" };
        clientService.getClientTicketStatuses(paramsJson).then(data => {
            setClients(data);
            setLoading(false);
        });
    }

    useEffect(() => {
        getClients();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitClient = (e) => {
        e.preventDefault();
        clientService.addClient(clientJson)
        .then(response => {
            props.setFailed(false);
            props.setAlert("Customer added successfully!");
            setDisplayClientForm(false);
            history.push("/clients");
        }).catch(error => {
            props.setFailed(true);
            props.setAlert("Failed to add customer! " + error.error_description);
            setDisplayClientForm(false);
        });
    }

    const getStatusCounts = (data) => {
        let statusCounts = [];
        for (var i = 0; i < data.length; i++ ) {
            statusCounts.push(<div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1 p-text-center" key={[i]}  style={{color: data[i].status === "opened" ? "#8882BD" : (data[i].status === "pending" ? "#D32F2F" : (data[i].status === "closed" ? "#689F38" : ""))}}>
                {data[i].count}
                </div>);
        }
        return statusCounts;
    }

    const dataView = (data) => (
        <div className="p-col-12 p-grid p-fluid p-pt-1 p-pl-1">
            <div className="p-fluid p-col-6 p-md-6 p-lg-6">{data.client.name}</div>
            <div className="p-fluid p-grid p-col-6 p-md-6 p-lg-6">{getStatusCounts(data.statusCounts)}</div>
        </div>
    );

    const itemTemplate = (data, layout) => {
        if (!data) {
            return;
        }
        if (layout === 'list') {
            return dataView(data);
        }
    };

    const dialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="submit" label="Save" onClick={(e) => submitClient(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayClientForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    return (
        <div className="p-col-12 p-md-6 p-lg-4 task-list">
            <Panel header="Complaints are raised from your customers">
                { clients.length > 0 &&
                    <div className="p-col-12" style={{maxHeight: '300px', overflow: "auto"}}>
                        <div className="p-col-12 p-pt-2 p-d-flex">
                            <div className="p-col-8 p-md-8 p-lg-8 p-text-left">
                                <h5 className="p-text-bold">Customers' Complaints</h5>
                            </div>
                            <div className="p-col-4 p-md-4 p-lg-4 p-text-right">
                                <Link to="/tickets" className="p-button p-button-link" title="More Complaints"><i className="p-text-bold pi pi-ellipsis-v"></i></Link>
                            </div>
                        </div>
                        <div className="p-col-12 p-grid p-fluid p-pt-1 p-text-center">
                            <div className="p-fluid p-col-6 p-md-6 p-lg-6"></div>
                            <div className="p-fluid p-grid p-col-6 p-md-6 p-lg-6">
                                <div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1" style={{color: "#8882BD"}}><small>Opened</small></div>
                                <div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1" style={{color: "#D32F2F"}}><small>Pending</small></div>
                                <div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1" style={{color: "#689F38"}}><small>Closed</small></div>
                            </div>
                        </div>
                        <DataView value={clients} layout={layout} rows={5} itemTemplate={itemTemplate} lazy loading={loading}></DataView>
                        <div className="p-col-12 p-text-center p-pt-5">
                            <Button type="button" onClick={() => setDisplayClientForm(true)} label="Add More Customers" icon="pi pi-plus-circle" />
                        </div>
                    </div>
                }
                { clients.length <= 0 &&
                    <div className="p-col-12 p-pl-3" style={{maxHeight: '300px', overflow: "auto"}}>
                        <h5 className="p-text-bold p-text-center">You don't have any customer</h5>
                        <span>For your teams to be able to submit inquiries, there must be some customers associated with your products where issues arise from.</span>
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                            <img src="assets/layout/images/dashboard/clients.svg" alt="Max Desk" className="logo" style={{width: "40%"}} />
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="button" onClick={() => setDisplayClientForm(true)} label="Add Customer Now" icon="pi pi-plus-circle" />
                        </div>
                    </div>
                }
            </Panel>
            <Dialog header="New Customer" visible={displayClientForm} className="p-col-4 p-offset-2" modal footer={dialogFooter} onHide={() => setDisplayClientForm(false)}>
                <div className="p-fluid p-formgrid p-grid">
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="name">Customer name</label>
                        <InputText type="text" name="name" value={clientJson.name} onChange={e => setClientJson({...clientJson, name: e.target.value})} required placeholder="Enter customer name" />
                    </div>
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="email">Email</label>
                        <InputText type="text" name="email" value={clientJson.email} onChange={e => setClientJson({...clientJson, email: e.target.value})} required placeholder="Enter Email Address" />
                    </div>
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="phone">Phone Number</label>
                        <InputText type="text" name="phone" value={clientJson.phone} onChange={e => setClientJson({...clientJson, phone: e.target.value})} required placeholder="Enter Phone Number" />
                    </div>
                </div>
            </Dialog>
        </div>
    )
}

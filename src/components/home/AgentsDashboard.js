import { DataView } from 'primereact/dataview';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Panel } from 'primereact/panel';
import { Button } from 'primereact/button';
import CompanyService from '../../service/CompanyService';
import { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

export const AgentsDashboard = props => {

    const history = useHistory();

    const [layout, setLayout] = useState('list');

    const [agents, setAgents] = useState([]);
    const [loading, setLoading] = useState(true);

    const [agentJson, setAgentJson] = useState({fullName: "", email: "", phoneNumber: "", password: "", confirm: ""});
    const [displayAgentForm, setDisplayAgentForm] = useState(false);

    const companyService = new CompanyService();

    const getAgents = () => {
        const paramsJson = { page: 0, size: 5, sort: "id", order: "desc" };
        companyService.getAgentTicketStatuses(paramsJson).then(data => {
            setAgents(data);
            setLoading(false);
        });
    }

    useEffect(() => {
        getAgents();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitAgent = (e) => {
        e.preventDefault();
        if (agentJson.password === agentJson.confirm) {
            companyService.addAgent(agentJson)
            .then(response => {
                props.setFailed(false);
                props.setAlert("Agent added successfully!");
                setDisplayAgentForm(false);
                history.push("/agents");
            }).catch(error => {
                props.setFailed(true);
                props.setAlert("Failed to add agent!");
                setDisplayAgentForm(false);
            });
        } else {
            props.setFailed(true);
            props.setAlert("Password does not match!");
        }
    }

    const getStatusCounts = (data) => {
        let statusCounts = [];
        for (var i = 0; i < data.length; i++ ) {
            statusCounts.push(<div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1 p-text-center" key={[i]}  style={{color: data[i].status === "opened" ? "#8882BD" : (data[i].status === "pending" ? "#D32F2F" : (data[i].status === "closed" ? "#689F38" : ""))}}>
                {data[i].count}
                </div>);
        }
        return statusCounts;
    }

    const dataView = (data) => (
        <div className="p-col-12 p-grid p-fluid p-pt-1 p-pl-1">
            <div className="p-fluid p-col-6 p-md-6 p-lg-6">{data.user.fullName}</div>
            <div className="p-fluid p-grid p-col-6 p-md-6 p-lg-6">{getStatusCounts(data.statusCounts)}</div>
        </div>
    );

    const itemTemplate = (data, layout) => {
        if (!data) {
            return;
        }
        if (layout === 'list') {
            return dataView(data);
        }
    };

    const dialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="submit" label="Save" onClick={(e) => submitAgent(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayAgentForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    return (
        <div className="p-col-12 p-md-6 p-lg-4 task-list">
            <Panel header="Agents submit complaints from email, phone, etc">
                { agents.length > 0 &&
                    <div className="p-col-12" style={{maxHeight: '300px', overflow: "auto"}}>
                        <div className="p-col-12 p-pt-2 p-d-flex">
                            <div className="p-col-8 p-md-8 p-lg-8 p-text-left">
                                <h5 className="p-text-bold">Agents' Complaints</h5>
                            </div>
                            <div className="p-col-4 p-md-4 p-lg-4 p-text-right">
                                <Link to="/tickets" className="p-button p-button-link" title="More Complaints"><i className="p-text-bold pi pi-ellipsis-v"></i></Link>
                            </div>
                        </div>
                        <div className="p-col-12 p-grid p-fluid p-pt-1 p-text-center">
                            <div className="p-fluid p-col-6 p-md-6 p-lg-6"></div>
                            <div className="p-fluid p-grid p-col-6 p-md-6 p-lg-6">
                            {/* FBC02D */}
                                <div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1" style={{color: "#8882BD"}}><small>Opened</small></div>
                                <div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1" style={{color: "#D32F2F"}}><small>Pending</small></div>
                                <div className="p-fluid p-col-4 p-md-4 p-lg-4 p-p-1 p-p-md-1 p-p-lg-1" style={{color: "#689F38"}}><small>Closed</small></div>
                            </div>
                        </div>
                        <DataView value={agents} layout={layout} rows={5} itemTemplate={itemTemplate} lazy loading={loading}></DataView>
                        <div className="p-col-12 p-text-center p-pt-5">
                            <Button type="button" onClick={() => setDisplayAgentForm(true)} label="Add More Agents" icon="pi pi-user-plus" />
                        </div>
                    </div>
                }
                { agents.length <= 0 &&
                    <div className="p-col-12 p-pl-3" style={{maxHeight: '300px', overflow: "auto"}}>
                        <h5 className="p-text-bold p-text-center">You don't have any Agent</h5>
                        <span>Inquiries will be submitted by agents to this system. Agents will also be assigned with various tasks to support your customers</span>
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                            <img src="assets/layout/images/dashboard/agents.svg" alt="Max Desk" className="logo" style={{width: "40%"}} />
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="button" onClick={() => setDisplayAgentForm(true)} label="Add Agent Now" icon="pi pi-plus-circle" />
                        </div>
                    </div>
                }
            </Panel>
            <Dialog header="New Agent" visible={displayAgentForm} className="p-col-4 p-offset-2" modal footer={dialogFooter} onHide={() => setDisplayAgentForm(false)}>
                <div className="p-fluid p-formgrid p-grid">
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="name">Full Name</label>
                        <InputText type="text" name="name" value={agentJson.fullName} onChange={e => setAgentJson({...agentJson, fullName: e.target.value})} required placeholder="Enter Agent Name" />
                    </div>
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="email">Email Address</label>
                        <InputText type="text" name="email" value={agentJson.email} onChange={e => setAgentJson({...agentJson, email: e.target.value})} required placeholder="Enter Email Address" />
                    </div>
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="phoneNumber">Phone Number</label>
                        <InputText type="text" name="phoneNumber" value={agentJson.phoneNumber} onChange={e => setAgentJson({...agentJson, phoneNumber: e.target.value})} required placeholder="Enter Phone Number" />
                    </div>
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="password">Password</label>
                        <InputText type="password" name="password" value={agentJson.password} onChange={e => setAgentJson({...agentJson, password: e.target.value})} required placeholder="Enter Password" />
                    </div>
                    <div className="p-field p-col-12 p-md-12">
                        <label htmlFor="confirm">Confirm</label>
                        <InputText type="password" name="confirm" value={agentJson.confirm} onChange={e => setAgentJson({...agentJson, confirm: e.target.value})} required placeholder="Confirm Password" />
                    </div>
                </div>
            </Dialog>
        </div>
    )
}

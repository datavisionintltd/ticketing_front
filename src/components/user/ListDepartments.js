import React, { useState, useEffect, useRef } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import UserService from '../../service/UserService';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';
import { OverlayPanel } from 'primereact/overlaypanel';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';

export const ListDepartments = () => {

    const [departments, setDepartments] = useState(null);
    const [selectedDepartment, setSelectedDepartment] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const [visibleRight, setVisibleRight] = useState(false);
    const [departmentId, setDepartmentId] = useState(0);
    const [departmentName, setDepartmentName] = useState("");
    const [description, setDescription] = useState("");
    const op = useRef(null);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteUser, setDeleteUser] = useState(0);

    const userService = new UserService();

    const getUsersList = () => {
        const paramsJson = { name: globalFilter, page: pageIndex, size: pageSize, sort: sortField, order: (sortOrder === 1 ? 'asc' : 'desc') };
        userService.getDepartments(paramsJson).then(data => {
            setDepartments(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
            setDepartmentId(0);
        })
        .catch(error => {
            setDepartments([]);
            setLoading(false);
        });
    }

    useEffect(() => {
        getUsersList();
    }, [pageIndex, sortField, sortOrder, first, pageSize, globalFilter]);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const onEditButtonClick = (data) => {
        setDepartmentId(data.id);
        setDepartmentName(data.name);
        setDescription(data.description);
        setVisibleRight(true);
    }

    const submitDepartment = (e) => {
        e.preventDefault();
        const userJson = {
            name: departmentName
        }
        if (description !== "") {
            userJson["description"] = description;
        }
        userService.addDepartment(userJson)
        .then(response => {
            setFailed(false);
            setAlert("Department added successfully!");
            getUsersList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add department!");
        });
    }

    const updateDepartment = (e) => {
        e.preventDefault();
        const userJson = {
            id: departmentId,
            name: departmentName
        }
        if (description !== "") {
            userJson["description"] = description;
        }
        userService.editDepartment(userJson)
        .then(response => {
            setFailed(false);
            setAlert("Department updated successfully!");
            getUsersList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update department!");
        });
    }

    const removeDepartment = (e) => {
        e.preventDefault();
        setDisplayConfirmation(false);
        userService.removeDepartment(departmentId)
        .then(response => {
            setFailed(false);
            setAlert("Department deleted successfully!");
            getUsersList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete department!");
        });
    }

    const onDeleteButtonClick = (data) => {
        setDeleteUser(data.name);
        setDepartmentId(data.id);
        setDisplayConfirmation(true);
    }

    const toggle = (event) => {
        op.current.toggle(event);
    };

    const usersTableHeader = (
        <div className="table-header">
            List of Departments
            <div className="p-grid">
                <div className="p-col-8 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-8 p-md-6">
                    <Link to="#" style={{float: "right"}}><Button label="Add Department" onClick={toggle} icon="pi pi-user-plus"/></Link>
                </div>
            </div>
        </div>
    );

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const userBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Department</span>
                {/* <img alt={data.fullName} src={`assets/layout/images/avatar/user.png`} width="32" style={{ verticalAlign: 'middle' }} /> */}
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.name}</span>
            </>
        );
    };

    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-user-edit" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} title="Edit User" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} title="Delete User"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeDepartment(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">

                    {/* RESPONSE ALERTS */}
                    <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />

                    {/* LIST USER DATATABLE */}
                    <DataTable value={departments} paginator={false} className="p-datatable-gridlines p-datatable-striped p-datatable-sm p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedDepartment} onSelectionChange={(e) => setSelectedDepartment(e.value)}
                        sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                        globalFilter={globalFilter} emptyMessage="No users found." loading={loading} header={usersTableHeader}>
                        <Column field="name" header="Department" sortable body={userBodyTemplate}></Column>
                        <Column field="description" header="Description" sortable body={bodyTemplate}></Column>
                        <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>
                    <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                        first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                    </Paginator>

                    {/* ADD NEW DEPARTMENT */}
                    <OverlayPanel ref={op} appendTo={document.body} showCloseIcon>
                        <form onSubmit={submitDepartment}>
                            <div className="p-grid">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Add Department</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="firstname2">Department Name</label>
                                                <InputText type="text" name="fullName" value={departmentName} onChange={e => setDepartmentName(e.target.value)} required placeholder="Enter Department Name" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="firstname2">Description</label>
                                                <InputText type="text" name="email" value={description} onChange={e => setDescription(e.target.value)} required placeholder="Enter More Details" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                            </div>
                        </form>
                    </OverlayPanel>

                    {/* EDIT USER RIGHT SIDE FORM */}
                    <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                        <form onSubmit={updateDepartment}>
                            <div className="p-grid">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit Department</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="firstname2">Department Name</label>
                                                <InputText type="text" name="fullName" value={departmentName} onChange={e => setDepartmentName(e.target.value)} required placeholder="Enter Department Name" />
                                            </div>
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="firstname2">Description</label>
                                                <InputText type="text" name="email" value={description} onChange={e => setDescription(e.target.value)} required placeholder="Enter More Details" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE USER CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteUser}?</span>
                        </div>
                    </Dialog>

                </div>
            </div>
        </div>
    )
}

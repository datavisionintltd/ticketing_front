import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import UserService from '../../service/UserService';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';

export const ListTeams = () => {

    const [departments, setDepartments] = useState([]);
    const [teams, setTeams] = useState([]);
    const [selectedTeam, setSelectedTeam] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const [displayForm, setDisplayForm] = useState(false);
    const [visibleRight, setVisibleRight] = useState(false);
    const [departmentId, setDepartmentId] = useState(0);
    const [depId, setDepId] = useState(0);
    const [teamId, setTeamId] = useState(0);
    const [teamName, setTeamName] = useState("");
    const [description, setDescription] = useState("");
    const [submitted, setSubmitted] = useState(false);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteUser, setDeleteUser] = useState(0);

    const userService = new UserService();

    const getTeamsList = () => {
        const paramsJson = { departmentId: depId, name: globalFilter, page: pageIndex, size: pageSize, sort: sortField, order: (sortOrder === 1 ? 'asc' : 'desc') };
        userService.getTeams(paramsJson).then(data => {
            setTeams(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
            setTeamId(0);
        })
        .catch(error => {
            setTeams([]);
            setLoading(false);
        });
        const departmentParamsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        userService.getDepartments(departmentParamsJson)
        .then(data => {
            setDepartments(data.content);
        });
    }

    useEffect(() => {
        getTeamsList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pageIndex, sortField, sortOrder, first, pageSize, globalFilter, submitted]);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const onChangeGlobalFilter = (e) => {
        e.preventDefault();
        setGlobalFilter(e.target.value);
    }

    const submitTeam = (e) => {
        e.preventDefault();
        const userJson = {
            name: teamName
        }
        if (departmentId !== 0) {
            userJson["departmentId"] = departmentId;
        }
        userService.addTeam(userJson)
        .then(response => {
            setDepartmentId(0);
            setTeamId(0);
            setTeamName("");
            setDescription("");
            setFailed(false);
            setAlert("team added successfully!");
            setSubmitted(true);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add team!");
        });
    }

    const onEditButtonClick = (data) => {
        setDepartmentId(data.department !== null ? data.department.id : 0);
        setTeamId(data.id);
        setTeamName(data.name);
        setDescription(data.description);
        setVisibleRight(true);
    }

    const updateTeam = (e) => {
        e.preventDefault();
        const userJson = {
            id: teamId,
            name: teamName
        }
        if (departmentId !== 0) {
            userJson["departmentId"] = departmentId;
        }
        userService.editTeam(userJson)
        .then(response => {
            setFailed(false);
            setAlert("Team updated successfully!");
            getTeamsList();
            setSubmitted(true);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update team!");
        });
    }

    const onDeleteButtonClick = (data) => {
        setDeleteUser(data.name);
        setTeamId(data.id);
        setDisplayConfirmation(true);
    }

    const removeTeam = (e) => {
        e.preventDefault();
        setDisplayConfirmation(false);
        userService.removeTeam(teamId)
        .then(response => {
            setFailed(false);
            setAlert("Team deleted successfully!");
            getTeamsList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete team!");
        });
    }

    const usersTableHeader = (
        <div className="table-header">
            List of Teams
            <div className="p-grid">
                <div className="p-col-8 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => onChangeGlobalFilter(e)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-8 p-md-6">
                    <Link to="#" style={{float: "right"}}><Button label="Add Team" onClick={() => setDisplayForm(true)} icon="pi pi-user-plus"/></Link>
                </div>
            </div>
        </div>
    );

    const dialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="submit" label="Save" onClick={(e) => submitTeam(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const departmentBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Department</span>
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.department !== null ? data.department.name : "General"}</span>
            </>
        );
    };

    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-user-edit" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} title="Edit Team" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} title="Delete Team"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeTeam(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">

                {/* RESPONSE ALERTS */}
                <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />

                {/* LIST TEAM DATATABLE */}
                { teams.length > 0 &&
                    <div className="card">
                        <DataTable value={teams} paginator={false} className="p-datatable-gridlines p-datatable-striped p-datatable-sm p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedTeam} onSelectionChange={(e) => setSelectedTeam(e.value)}
                            sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                            globalFilter={globalFilter} emptyMessage="No users found." loading={loading} header={usersTableHeader}>
                            <Column field="name" header="Team" sortable body={bodyTemplate}></Column>
                            <Column field="department" header="Department" sortable body={departmentBodyTemplate}></Column>
                            <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                        </DataTable>
                        <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                            first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                            paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                        </Paginator>
                    </div>
                }
                { teams.length <= 0 &&
                    <div className="p-col-12 p-pl-3 p-text-center">
                        <h5 className="p-text-bold p-text-center">You don't have any Team</h5>
                        <span className="p-text-center">In order to measure performance of your support, group your agents into teams depending on the type of issue that can be raised.</span>
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                            <img src="assets/layout/images/dashboard/teams.svg" alt="Max Desk" className="logo" style={{width: "30%"}} />
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="button" onClick={() => setDisplayForm(true)} label="Add Team Now" icon="pi pi-users" />
                        </div>
                    </div>
                }

                {/* EDIT TEAM RIGHT SIDE FORM */}
                <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                    <form onSubmit={updateTeam}>
                        <div className="p-grid">
                            <div className="p-col-12">
                                <div className="card">
                                    <h5>Edit Team</h5>
                                    <div className="p-fluid p-formgrid p-grid">
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="firstname2">Team Name</label>
                                            <InputText type="text" name="fullName" value={teamName} onChange={e => setTeamName(e.target.value)} required placeholder="Enter Team Name" />
                                        </div>
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="firstname2">Department</label>
                                            <Dropdown id="state" value={departmentId} onChange={(e) => setDepartmentId(e.value)} options={departments} optionLabel="name" optionValue="id" placeholder="Select Department"></Dropdown>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                            <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                        </div>
                    </form>
                </Sidebar>

                {/* DELETE TEAM CONFIRMATION DIALOG */}
                <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                        <span>Are you sure you want to delete {deleteUser}?</span>
                    </div>
                </Dialog>
                <Dialog header="New Team" visible={displayForm} className="p-col-4" modal={true} footer={dialogFooter} onHide={() => setDisplayForm(false)} maximizable={true} blockScroll={true}>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="fullName">Team Name</label>
                            <InputText type="text" name="fullName" value={teamName} onChange={e => setTeamName(e.target.value)} required placeholder="Enter Tema Name" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="department">Department</label>
                            <Dropdown id="department" name="department" value={departmentId} onChange={(e) => setDepartmentId(e.value)} options={departments} optionLabel="name" optionValue="id" filter filterBy="name" placeholder="Select Department" autoWidth={false} appendTo={document.body}></Dropdown>
                        </div>
                    </div>
                </Dialog>
            </div>
        </div>
    )
}

import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import UserService from '../../service/UserService';
import { MultiSelect } from 'primereact/multiselect';
import { useHistory } from 'react-router';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
// import {Editor} from 'primereact/editor';

export const AddUser = () => {

    const history = useHistory();

    const [userJson, setUserJson] = useState({});
    const [roles, setRoles] = useState([]);
    const [rolesValue, setRolesValue] = useState([]);
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const userService = new UserService();

    useEffect(() => {
        userService.getRoles()
            .then(data => {
                setRoles(data);
            });
    }, [rolesValue]);

    const submitUser = (e) => {
        e.preventDefault();
        const rolesJsonArray = rolesValue.map(r => ({
            id: r.id
        }));
        const jsonValue = { user: userJson, roles: rolesJsonArray };
        // console.log("user content: " + JSON.stringify(jsonValue));
        if (userJson.password === userJson.confirm && rolesJsonArray.length > 0) {
            userService.addUser(jsonValue)
                .then(response => {
                    if (!response) {
                        // eslint-disable-next-line no-throw-literal
                        throw "Something went wrong"
                    }
                    if (response.status !== "success") {
                        setFailed(true)
                        setAlert("Failed to add user! Error:" + response.message);
                    } else {
                        setFailed(false);
                        setAlert("User added successfully!");
                        history.push('/users');
                    }
                }).catch(error => {
                    setFailed(true);
                    setAlert("Failed to add user! " + error);
                });
        } else if (rolesJsonArray.length <= 0) {
            setFailed(true);
            setAlert("Please choose roles!");
        } else {
            setFailed(true);
            setAlert("Password does not match!");
        }
    }

    const itemTemplate = (option) => {
        return (
            <div className="country-item">
                <span>{option.name}</span>
            </div>
        );
    };

    const selectedItemTemplate = (option) => {
        if (option) {
            return (
                <div className="country-item country-item-value">
                    <span>{option.name}</span>
                </div>
            );
        }

        return 'Select Roles';
    };

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{ padding: "2%" }}>
                <form onSubmit={submitUser}>
                    <div className="card">
                        <h5>Add User</h5>
                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="firstname2">Username</label>
                                <InputText type="text" name="username" value={userJson.username || ''} onChange={e => setUserJson({ ...userJson, username: e.target.value })} required placeholder="Enter Username" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="lastname2">Password</label>
                                <InputText type="password" name="password" value={userJson.password || ''} onChange={e => setUserJson({ ...userJson, password: e.target.value })} required placeholder="Enter Password" />

                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="lastname2">Confirm</label>
                                <InputText type="password" name="confirm" value={userJson.confirm || ''} onChange={e => setUserJson({ ...userJson, confirm: e.target.value })} required placeholder="Confirm Password" />
                            </div>
                            <div className="p-field p-col-12 p-md-6">
                                <label htmlFor="firstname2">Full Name</label>
                                <InputText type="text" name="fullName" value={userJson.fullName || ''} onChange={e => setUserJson({ ...userJson, fullName: e.target.value })} required placeholder="Enter Full Name" />
                            </div>
                            <div className="p-field p-col-12 p-md-6">
                                <label htmlFor="firstname2">Email Address</label>
                                <InputText type="text" name="email" value={userJson.email || ''} onChange={e => setUserJson({ ...userJson, email: e.target.value })} required placeholder="Enter Email" />
                            </div>
                            <div className="p-field p-col-12 p-md-6">
                                <label htmlFor="firstname2">Phone Number</label>
                                <InputText type="text" name="phoneNumber" value={userJson.phoneNumber || ''} onChange={e => setUserJson({ ...userJson, phoneNumber: e.target.value })} required placeholder="Enter Phone Number" />
                            </div>
                            <div className="p-field p-col-12 p-md-6">
                                <label htmlFor="roles">Roles</label>
                                <MultiSelect value={rolesValue || []} onChange={(e) => setRolesValue(e.value)} options={roles} optionLabel="name" placeholder="Select Roles" filter />
                            </div>
                            <div className="p-col-12 p-offset-4 p-md-4">
                                <Button type="submit" label="Add to Users" icon="pi pi-check" autoFocus={true} />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            {/* <div className="p-col-12">
                <div className="card card-w-title">
                    <h1>Editor</h1>
                    <Editor style={{height:'320px'}} />
                </div>
            </div> */}
        </div>
    )
}

import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import UserService from '../../service/UserService';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import { useHistory } from 'react-router';
import { SECURED } from '../../constants';

export const ResetingPassword = () => {

    const history = useHistory();

    const [password, setPassword] = useState("");
    const [confirm, setConfirm] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const userService = new UserService();

    const submitUser = (e) => {
        e.preventDefault();
        const userJson = {
            password: password,
            confirm: confirm
        };
        if(password === confirm) {
            userService.resetPassword(userJson)
            .then(response => {
                setFailed(false);
                setAlert("Password updated successfully!");
                localStorage.setItem(SECURED, "true");
                history.push("/");
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to reset password!");
            });
        } else {
            setFailed(true);
            setAlert("Password does not match!");
        }
    }

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{padding: "2%"}}>
                <form onSubmit={submitUser}>
                    <div className="card">
                        <div className="divider-container"><div className="divider-border" />
                            <span className="divider-content">
                                <h4 style={{textAlign: "center", paddingBottom: "5%"}}>Reset Password</h4>
                            </span>
                        <div className="divider-border" /></div>
                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-grid p-col-12 p-md-offset-2 p-md-8">
                                <div className="p-field p-col-12 p-md-6">
                                    <label htmlFor="password">Password</label>
                                    <InputText type="password" name="password" value={password} onChange={e => setPassword(e.target.value)} required placeholder="Enter Password" />
                                </div>
                                <div className="p-field p-col-12 p-md-6">
                                    <label htmlFor="confirm">Confirm Password</label>
                                    <InputText type="password" name="confirm" value={confirm} onChange={e => setConfirm(e.target.value)} required placeholder="Confirm Password" />
                                </div>
                            </div>
                            <div className="p-col-12 p-md-offset-4 p-md-4">
                                <Button type="submit" label="Reset" icon="pi pi-check" autoFocus={true}/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

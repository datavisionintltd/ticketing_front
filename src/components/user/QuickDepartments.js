import React, { useState, useEffect } from 'react';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { format } from 'date-fns';
import UserService from '../../service/UserService';
import { useHistory } from 'react-router-dom';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import { ACCESS_TOKEN } from '../../constants';

export const QuickDepartments = ({ navigation }) => {

    const history = useHistory();

    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const [departments, setDepartments] = useState([{name: "Technical", description: "Deals with technical Issues"}]);

    const userService = new UserService();

    useEffect(() => {
        if (!localStorage.getItem(ACCESS_TOKEN)) {
            history.push("/login");
        }
        window.addEventListener("popstate", () => {
            history.go(1);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitDepartments = (e) => {
        e.preventDefault();
        userService.addDepartments(departments)
        .then(response => {
            setFailed(false);
            setAlert("Departments added successfully!");
            history.push('/quick/topics');
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add departments!");
        });
    }

    const handleDepartments = (e, index) => {
        const items = [...departments];
        items[index][e.target.name] = e.target.value;
        setDepartments(items);
    }

    const addDepartment = () => {
        setDepartments(prevDepartments => [...prevDepartments, {name: "", description: ""}]);
    }

    const removeDepartment = (index) => {
        if (departments.length > 1) {
            setDepartments(departments.filter((item, i) => i !== index));
        }
    }

    return (
        <div className="p-grid dashboard">

            <div className="p-grid p-col-12 p-md-12 p-lg-12 p-text-left p-pt-6 p-pt-md-6 p-pt-lg-6" >
                <div className="p-col-12 p-md-3 p-lg-3 p-text-center">
                    <img src="assets/layout/images/logo.png" alt="Logo" className="logo" style={{ width: "20%" }} />
                </div>
            </div>
            <div className="p-col-12 p-md-offset-4 p-md-4 p-lg-offset-4 p-lg-4">
                <h3 style={{textAlign: "center", padding: "0.5em"}}>What are the departments involved in solving complaints?</h3>
                <form onSubmit={submitDepartments}>
                    <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                    { departments.map((item, index) => (
                        <div className="p-fluid p-formgrid p-grid p-col-12 p-pb-3" key={index}>
                            <div className="p-field p-col-12 p-md-5">
                                <InputText type="text" data-id={index} name="name" value={item.name} onChange={e => handleDepartments(e, index)} required placeholder="Enter Department Name" />
                            </div>
                            <div className="p-field p-col-12 p-md-5">
                                <InputText type="text" data-id={index} name="description" value={item.description} onChange={e => handleDepartments(e, index)} required placeholder="Enter department functions" />
                            </div>
                            {departments.length > 1 &&
                                <div className="p-col-12 p-md-1">
                                    <Button type="button" icon="pi pi-minus" onClick={() => removeDepartment(index)}/>
                                </div>
                            }
                        </div>
                    ))}
                    <div className="p-fluid p-formgrid p-grid p-col-12">
                        <div className="p-col-12 p-md-5 p-d-flex">
                            <Button type="button" icon="pi pi-plus" className="p-button-sm" label="Another department" onClick={addDepartment}/>
                        </div>
                    </div>
                    <div className="p-col-12 p-text-center p-pt-5 p-pt-md-5 p-pt-lg-5">
                        <Button type="submit" label="Save" className="p-button-sm button-rounded p-col-2 p-md-2 p-lg-2" style={{ marginRight: '.25em', backgroundColor: "#333333" }} autoFocus={true} />
                    </div>
                </form>
            </div>
            <div className="p-grid p-col-12 p-md-12 p-lg-12 p-text-center" style={{position: "absolute", bottom: "0"}}>
                <span className="p-text-center p-offset-4 p-col-4 p-md-offset-4 p-md-4 p-lg-offset-4 p-lg-4" style={{color: "#8882bd"}}><small>&copy; {format(new Date(), "yyyy")}, Software Galaxy. All rights reserved.</small></span>
            </div>
        </div>
    )
}

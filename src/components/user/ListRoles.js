import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import UserService from '../../service/UserService';
import { Link } from 'react-router-dom';
import { Sidebar } from 'primereact/sidebar';
import { Dialog } from 'primereact/dialog';

export const ListRoles = () => {

    const [roles, setRoles] = useState(null);
    const [selectedUser, setSelectedUser] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);

    const [visibleAdd, setVisibleAdd] = useState(false);
    const [visibleEdit, setVisibleEdit] = useState(false);
    const [roleId, setRoleId] = useState(0);
    const [authority, setAuthority] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [submitted, setSubmitted] = useState(false);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteRole, setDeleteRole] = useState(0);

    const userService = new UserService();

    useEffect(() => {
        userService.getRoles().then(data => {
            setRoles(data);
            setLoading(false);
        })
        .catch(error => {
            setRoles([]);
            setLoading(false);
        });
    }, [submitted]);

    const addRole = (e) => {
        e.preventDefault();
        const roleJson = {
            name: authority
        };
        userService.addRole(roleJson)
        .then(response => {
            setFailed(false);
            setAlert("Role added successfully!");
            setSubmitted(true);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add role! " + error.error_description);
        });
    }

    const onEditButtonClick = (data) => {
        setAuthority(data.name);
        setRoleId(data.id);
        setVisibleEdit(true);
    }

    const editRole = (e) => {
        e.preventDefault();
        const roleJson = {
            id: roleId,
            name: authority
        };
        userService.editRole(roleJson)
        .then(response => {
            setFailed(false);
            setAlert("Role updated successfully!");
            setSubmitted(true);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update role! ");
        });
    }

    const onDeleteButtonClick = (data) => {
        setRoleId(data.id);
        setDeleteRole(data.name);
        setDisplayConfirmation(true);
    }

    const removeRole = (e) => {
        e.preventDefault();
        setDisplayConfirmation(false);
        userService.removeRole(roleId)
        .then(response => {
            setFailed(false);
            setAlert("Role deleted successfully!");
            setSubmitted(true);
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete role!");
        });
    }

    const rolesTableHeader = (
        <div className="table-header">
            List of Roles
            <div className="p-grid">
                <div className="p-col-6 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-6 p-md-6">
                    <Button onClick={() => setVisibleAdd(true)} style={{float: "right"}}  label="Add Role" icon="pi pi-unlock"/>
                </div>
            </div>
        </div>
    );

    const userBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Authority</span>
                <i width="32" className="pi pi-user-edit" style={{ verticalAlign: 'middle', color: "#4F4A9E" }} />
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.name}</span>
            </>
        );
    };

    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-pencil" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="Edit Role" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} tooltip="Delete Role"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeRole(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                <div className="card">
                    { failed === false &&
                        <div className="p-message p-component p-message-success" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                                <span className="p-message-text">{alert}.</span>
                            </div>
                        </div>
                    }
                    { failed === true &&
                        <div className="p-message p-component p-message-warn" style={{margin: '0 0 1em 0', display: 'block'}}>
                            <div className="p-message-wrapper">
                                <span className="p-message-icon pi pi-fw pi-2x pi-times"></span>
                                <span className="p-message-text">{alert}.</span>
                            </div>
                        </div>
                    }

                    {/* LIST ROLES DATATABLE */}
                    <DataTable value={roles} paginator className="p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedUser} onSelectionChange={(e) => setSelectedUser(e.value)}
                        responsive={true} globalFilter={globalFilter} emptyMessage="No roles found." loading={loading} header={rolesTableHeader}>
                        <Column field="name" header="Authority" sortable body={userBodyTemplate}></Column>
                        <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                    </DataTable>

                    {/* EDIT USER RIGHT SIDE FORM */}
                    <Sidebar visible={visibleAdd} onHide={() => setVisibleAdd(false)} baseZIndex={1000} position="right">
                        <form onSubmit={addRole}>
                            <div className="p-grid m3">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Add Role</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Authority</label>
                                                <InputText type="text" name="name" value={authority} onChange={e => setAuthority(e.target.value)} required placeholder="Enter Authority" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleAdd(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleAdd(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>
                    <Sidebar visible={visibleEdit} onHide={() => setVisibleEdit(false)} baseZIndex={1000} position="right">
                        <form onSubmit={editRole}>
                            <div className="p-grid m3">
                                <div className="p-col-12">
                                    <div className="card">
                                        <h5>Edit Role</h5>
                                        <div className="p-fluid p-formgrid p-grid">
                                            <div className="p-field p-col-12 p-md-12">
                                                <label htmlFor="name">Authority</label>
                                                <InputText type="text" name="name" value={authority} onChange={e => setAuthority(e.target.value)} required placeholder="Enter Authority" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-col-12 p-text-center">
                                <Button type="submit" onClick={() => setVisibleEdit(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                                <Button type="button" onClick={() => setVisibleEdit(false)} label="Cancel" className="p-button-secondary" />
                            </div>
                        </form>
                    </Sidebar>

                    {/* DELETE USER CONFIRMATION DIALOG */}
                    <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                        <div className="confirmation-content">
                            <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                            <span>Are you sure you want to delete {deleteRole}?</span>
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    )
}

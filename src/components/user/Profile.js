import React, { useEffect, useState } from 'react';
import UserService from '../../service/UserService';

export const Profile = () => {

    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [roles, setRoles] = useState(null);

    const userService = new UserService();

    useEffect(() => {
        userService.getUser()
        .then(data => {
            setFullName(data.fullName);
            setEmail(data.email);
            setPhoneNumber(data.phoneNumber);
            setRoles(data.roles);
        });
    }, []);

    const itemTemplate = (option) => {
        return (
            <div className="country-item">
                <span>{option.name}</span>
            </div>
        );
    };

    const selectedItemTemplate = (option) => {
        if (option) {
            return (
                <div className="country-item country-item-value">
                    <span>{option.name}</span>
                </div>
            );
        }

        return 'Select Roles';
    };

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{padding: "10%"}}>
                <div className="card">
                    {/* <h4 style={{textAlign: "center"}}>My Profile</h4> */}
                    <div className="divider-container"><div className="divider-border" />
                        <span className="divider-content">
                            <h4 style={{textAlign: "center", paddingBottom: "3%"}}>My Profile</h4>
                        </span>
                    <div className="divider-border" /></div>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-col-12 p-md-12 p-grid" style={{paddingBottom: "2%", paddingTop: "2%"}}>
                            <div className="p-col-12 p-md-6">
                                <span style={{float: "right", fontSize: "16px"}}>Full Name:</span>
                            </div>
                            <div className="p-col-12 p-md-6">
                                <span style={{float: "left", fontSize: "16px"}}>{fullName}</span>
                            </div>
                        </div>
                        <div className="p-col-12 p-md-12 p-grid" style={{paddingBottom: "2%"}}>
                            <div className="p-col-12 p-md-6">
                                <span style={{float: "right", fontSize: "16px"}}>Phone Number:</span>
                            </div>
                            <div className="p-col-12 p-md-6">
                                <span style={{float: "left", fontSize: "16px"}}>{phoneNumber}</span>
                            </div>
                        </div>
                        <div className="p-col-12 p-md-12 p-grid" style={{paddingBottom: "2%"}}>
                            <div className="p-col-12 p-md-6">
                                <span style={{float: "right", fontSize: "16px"}}>Email Addresss:</span>
                            </div>
                            <div className="p-col-12 p-md-6">
                                <span style={{float: "left", fontSize: "16px"}}>{email}</span>
                            </div>
                        </div>
                    </div>
                    <div className="divider-container"><div className="divider-border" />
                        <span className="divider-content">
                            <h4 style={{textAlign: "center", paddingBottom: "3%"}}>My Roles</h4>
                        </span>
                    <div className="divider-border" /></div>
                    <div className="p-fluid p-formgrid p-grid">
                        { roles.map(role => (
                            <div className="p-col-12 p-md-12" style={{textAlign: "center", paddingBottom: "2%"}}>
                                <span style={{fontSize: "16px"}}>{role.name}</span>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}

import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import UserService from '../../service/UserService';
import CompanyService from '../../service/CompanyService';
import { Link } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';

export const ListAgents = () => {

    const [users, setUsers] = useState([]);
    const [selectedUser, setSelectedUser] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');

    const [agentJson, setAgentJson] = useState({fullName: "", email: "", phoneNumber: "", password: "", confirm: ""});
    const [displayAgentForm, setDisplayAgentForm] = useState(false);
    const [submitted, setSubmitted] = useState(false);
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const [visibleRight, setVisibleRight] = useState(false);
    const [userJson, setUserJson] = useState({});

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteUser, setDeleteUser] = useState(0);

    const userService = new UserService();
    const companyService = new CompanyService();

    const getUsersList = () => {
        const paramsJson = { page: pageIndex, size: pageSize, sort: sortField, order: (sortOrder === 1 ? 'asc' : 'desc') };
        companyService.getAgents(paramsJson).then(data => {
            setUsers(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            // setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
        })
        .catch(error => {
            setUsers([]);
            setLoading(false);
        });
    }

    useEffect(() => {
        getUsersList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pageIndex, sortField, sortOrder, first, pageSize, submitted]);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const submitAgent = (e) => {
        e.preventDefault();
        if (agentJson.password === agentJson.confirm) {
            companyService.addAgent(agentJson)
            .then(response => {
                setFailed(false);
                setAlert("Agent added successfully!");
                setDisplayAgentForm(false);
                setSubmitted(!submitted);
            }).catch(error => {
                setFailed(true);
                setAlert("Failed to add agent!");
                setDisplayAgentForm(false);
            });
        } else {
            setFailed(true);
            setAlert("Password does not match!");
        }
    }

    const onStatusButtonClick = (data) => {
        const enableJson = {
            userId: data.id,
            enable: !data.enabled
        };
        userService.enableUser(enableJson)
        .then(response => {
            setFailed(false);
            setAlert(response.message + "!");
            getUsersList();
        }).catch(error => {
            setFailed(true);
            setAlert(error.message + "!");
        });
    }

    const onEditButtonClick = (data) => {
        setUserJson({
            ...userJson,
            id: data.id,
            fullName: data.fullName,
            email: data.email,
            phoneNumber: data.phoneNumber
        });
        setVisibleRight(true);
    }

    const submitUser = (e) => {
        e.preventDefault();
        userService.editUser(userJson)
        .then(response => {
            setFailed(false);
            setAlert("User updated successfully!");
            getUsersList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update user! " + error.error_description);
        });
    }

    const onDeleteButtonClick = (data) => {
        setDeleteUser(data.fullName);
        setDisplayConfirmation(true);
    }

    const usersTableHeader = (
        <div className="table-header">
            List of Agents
            <div className="p-grid">
                <div className="p-col-8 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-8 p-md-6">
                    <Link to="#" style={{float: "right"}}><Button label="Add Agent" onClick={() => setDisplayAgentForm(true)} icon="pi pi-user-plus"/></Link>
                </div>
            </div>
        </div>
    );

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const userBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Name</span>
                <img alt={data.fullName} src={`assets/layout/images/avatar/user.png`} width="32" style={{ verticalAlign: 'middle' }} />
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.fullName}</span>
            </>
        );
    };

    const statusBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Status</span>
                <span className={`customer-badge status-${data.enabled ? 'qualified' : 'unqualified'}`}>{data.enabled ? 'Enabled' : 'Disabled'}</span>
                <Link to="#" onClick={() => onStatusButtonClick(data)} className={`customer-badge status-${data.enabled ? 'unqualified' : 'qualified'}`}>
                    <i className={`pi pi-${data.enabled ? 'times' : 'check'}`}></i>
                </Link>
            </>
        )
    };

    const actionTemplate = (data) => {
        return (
            <>
                <Button type="button" icon="pi pi-user-edit" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} title="Edit User" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)} title="Delete User"></Button>
            </>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    const dialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="submit" label="Save" onClick={(e) => submitAgent(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayAgentForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                {/* RESPONSE ALERTS */}
                <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />

                {/* LIST USER DATATABLE */}
                { users.length > 0 &&
                    <>
                        <DataTable value={users} paginator={false} className="p-datatable-gridlines p-datatable-striped p-datatable-sm p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedUser} onSelectionChange={(e) => setSelectedUser(e.value)}
                            sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                            globalFilter={globalFilter} emptyMessage="No users found." loading={loading} header={usersTableHeader}>
                            <Column selectionMode="multiple" headerStyle={{ width: '3em' }}></Column>
                            <Column field="fullName" header="Name" sortable body={bodyTemplate}></Column>
                            <Column field="email" header="Email Address" sortable body={bodyTemplate}></Column>
                            <Column field="phoneNumber" header="Phone Number" sortable body={bodyTemplate}></Column>
                            <Column field="enabled" header="Status" sortable body={statusBodyTemplate}></Column>
                            <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                        </DataTable>
                        <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                            first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                            paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                        </Paginator>
                    </>
                }
                { users.length <= 0 &&
                    <div className="p-col-12 p-pl-3 p-text-center">
                        <h5 className="p-text-bold p-text-center">You don't have any Agent</h5>
                        <span>Inquiries will be submitted by agents to this system. Agents will also be assigned with various tasks to support your customers</span>
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                            <img src="assets/layout/images/dashboard/agents.svg" alt="Max Desk" className="logo" style={{width: "20%"}} />
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="button" onClick={() => setDisplayAgentForm(true)} label="Add Agent Now" icon="pi pi-plus-circle" />
                        </div>
                    </div>
                }

                {/* EDIT USER RIGHT SIDE FORM */}
                <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                    <form onSubmit={submitUser}>
                        <div className="p-grid">
                            <div className="p-col-12">
                                <div className="card">
                                    <h5>Edit User</h5>
                                    <div className="p-fluid p-formgrid p-grid">
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="firstname2">Full Name</label>
                                            <InputText type="text" name="fullName" value={userJson.fullName || ''} onChange={e => setUserJson({...userJson, fullName: e.target.value})} required placeholder="Enter Full Name" />
                                        </div>
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="firstname2">Email Address</label>
                                            <InputText type="text" name="email" value={userJson.email || ''} onChange={e => setUserJson({...userJson, email: e.target.value})} required placeholder="Enter Email" />
                                        </div>
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="firstname2">Phone Number</label>
                                            <InputText type="text" name="phoneNumber" value={userJson.phoneNumber || ''} onChange={e => setUserJson({...userJson, phoneNumber: e.target.value})} required placeholder="Enter Phone Number" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                            <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                        </div>
                    </form>
                </Sidebar>

                {/* DELETE USER CONFIRMATION DIALOG */}
                <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                        <span>Are you sure you want to delete {deleteUser}?</span>
                    </div>
                </Dialog>
                <Dialog header="New Agent" visible={displayAgentForm} className="p-col-4 p-offset-2" modal footer={dialogFooter} onHide={() => setDisplayAgentForm(false)}>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="name">Full Name</label>
                            <InputText type="text" name="name" value={agentJson.fullName} onChange={e => setAgentJson({...agentJson, fullName: e.target.value})} required placeholder="Enter Agent Name" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="email">Email Address</label>
                            <InputText type="text" name="email" value={agentJson.email} onChange={e => setAgentJson({...agentJson, email: e.target.value})} required placeholder="Enter Email Address" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="phoneNumber">Phone Number</label>
                            <InputText type="text" name="phoneNumber" value={agentJson.phoneNumber} onChange={e => setAgentJson({...agentJson, phoneNumber: e.target.value})} required placeholder="Enter Phone Number" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="password">Password</label>
                            <InputText type="password" name="password" value={agentJson.password} onChange={e => setAgentJson({...agentJson, password: e.target.value})} required placeholder="Enter Password" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="confirm">Confirm</label>
                            <InputText type="password" name="confirm" value={agentJson.confirm} onChange={e => setAgentJson({...agentJson, confirm: e.target.value})} required placeholder="Confirm Password" />
                        </div>
                    </div>
                </Dialog>
            </div>
        </div>
    )
}

import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import TaskService from '../../service/TaskService';
import TicketService from '../../service/TicketService';
import UserService from '../../service/UserService';
import { useHistory } from 'react-router';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';
import { Dropdown } from 'primereact/dropdown';


export const AddTask = () => {

    const history = useHistory();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);
    const [ticketId, setTicketId] = useState(0);
    const [tickets, setTickets] = useState([]);
    const [agentId, setAgentId] = useState(0);
    const [users, setUsers] = useState([]);

    const taskService = new TaskService();
    const ticketService = new TicketService();
    const userService = new UserService();

    useEffect(() => {
        const paramsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        ticketService.getTickets(paramsJson)
        .then(data => {
            setTickets(data.content);
        });
        userService.getUsers(paramsJson)
        .then(data => {
            setUsers(data.content);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const submitTask = (e) => {
        e.preventDefault();
        const taskJson = {
            name: name,
            description: description,
            ticket: {
                id: ticketId
            },
            agent: {
                id: agentId
            }
        };
        taskService.addTask(taskJson)
        .then(response => {
            setFailed(false);
            setAlert(response.message);
            history.push('/tasks');
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add Task! " + error.error_description);
        });
    }

    return (
        <div className="p-grid">
            <div className="p-col-12" style={{ padding: "2%" }}>
                <form onSubmit={submitTask}>
                    <div className="card">
                        <div className="p-card-title" style={{backgroundColor: "#8882BD", color: "#FFFFFF"}}>
                            <h5 style={{textAlign: "center", padding: "0.5em"}}>New Task</h5>
                        </div>
                        <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-2 p-md-4">
                                <label htmlFor="name">Task</label>
                                <InputText type="text" name="name" value={name} onChange={e => setName(e.target.value)} required placeholder="Enter Name" />
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="description">Description</label>
                                <InputText type="text" name="description" value={description} onChange={e => setDescription(e.target.value)} required placeholder="Enter Description" />
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-md-offset-2 p-md-4">
                                <label htmlFor="ticketId">Ticket</label>
                                <Dropdown id="ticketId" value={ticketId} onChange={(e) => setTicketId(e.value)} options={tickets} optionLabel="name" optionValue="id" filter filterBy="name" placeholder="Select Ticket"></Dropdown>
                            </div>
                            <div className="p-field p-col-12 p-md-4">
                                <label htmlFor="agentId">Agent</label>
                                <Dropdown id="agentId" value={agentId} onChange={(e) => setAgentId(e.value)} options={users} optionLabel="fullName" optionValue="id" filter filterBy="fullName" placeholder="Select Agent"></Dropdown>
                            </div>
                        </div>
                        <div className="p-fluid p-formgrid p-grid">
                            <div className="p-field p-col-12 p-offset-4 p-md-4">
                                <Button type="submit" label="Add to Tasks" icon="pi pi-check" autoFocus={true} />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

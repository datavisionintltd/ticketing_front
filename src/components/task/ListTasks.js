import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Paginator } from 'primereact/paginator';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import TaskService from '../../service/TaskService';
import TicketService from '../../service/TicketService';
import UserService from '../../service/UserService';
import { Link, useHistory } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Sidebar } from 'primereact/sidebar';
import { ResponseAlert } from '../../utilities/components/ResponseAlert';

export const ListTasks = () => {

    const history = useHistory();

    const [tasks, setTasks] = useState([]);
    const [selectedTask, setSelectedTask] = useState(null);
    const [globalFilter, setGlobalFilter] = useState('');
    const [loading, setLoading] = useState(true);
    const [first, setFirst] = useState(0);
    const [pageIndex, setPageIndex] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [numberOfElements, setNumberOfElements] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const [sortOrder, setSortOrder] = useState(-1);
    const [sortField, setSortField] = useState('id');
    const [alert, setAlert] = useState("");
    const [failed, setFailed] = useState(null);

    const [visibleRight, setVisibleRight] = useState(false);
    const [taskId, setTaskId] = useState("");
    const [tickets, setTickets] = useState([]);
    const [users, setUsers] = useState([]);

    const [taskJson, setTaskJson] = useState({name: "", description: "", ticket: {id: 0}, agent: {id: 0}});
    const [displayTaskForm, setDisplayTaskForm] = useState(false);
    const [submitted, setSubmitted] = useState(false);

    const [displayConfirmation, setDisplayConfirmation] = useState(false);
    const [deleteTask, setDeleteTask] = useState(0);

    const taskService = new TaskService();
    const ticketService = new TicketService();
    const userService = new UserService();

    const getTasksList = () => {
        const paramsJson = { page: pageIndex, size: pageSize, sort: sortField, order: (sortOrder === 1 ? 'asc' : 'desc') };
        taskService.getTasks(paramsJson).then(data => {
            setTasks(data.content);
            setLoading(false);
            setPageIndex(data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number);
            setTotalPages(data.totalPages);
            setNumberOfElements(data.numberOfElements);
            setTotalElements(data.totalElements);
            setFirst(((data.pageable.pageNumber != null ? data.pageable.pageNumber : data.number) * data.totalPages) + 1);
        })
        .catch(error => {
            setTasks([]);
            setLoading(false);
        });
    }

    useEffect(() => {
        getTasksList();
        const paramsJson = { name: "", page: 0, size: 100, sort: "id", order: "asc" };
        ticketService.getTickets(paramsJson)
        .then(data => {
            setTickets(data.content);
        });
        userService.getUsers(paramsJson)
        .then(data => {
            setUsers(data.content);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pageIndex, sortField, sortOrder, first, pageSize, submitted]);

    const onSortChange = (e) => {
        setSortOrder(e.sortOrder);
        setSortField(e.sortField);
    }

    const onPageChange = (e) => {
        setPageIndex(e.page);
        setFirst(e.first);
        setPageSize(e.rows);
    }

    const onStatusButtonClick = (data) => {
        const enableJson = {
            taskId: data.id,
            enable: !data.enabled
        };
        taskService.enableTask(enableJson)
        .then(response => {
            setFailed(false);
            setAlert(response.message + "!");
            getTasksList();
        }).catch(error => {
            setFailed(true);
            setAlert(error.message + "!");
        });
    }

    const submitTask = (e) => {
        e.preventDefault();
        taskService.addTask(taskJson)
        .then(response => {
            setFailed(false);
            setAlert(response.message);
            setDisplayTaskForm(false);
            getTasksList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to add Task! " + error.error_description);
        });
    }

    const onEditButtonClick = (data) => {
        setTaskJson({id: data.id, name: data.name, description: data.description, ticket: {id: data.ticket.id}, agent: {id: data.agent.id}});
        setVisibleRight(true);
    }

    const updateTask = (e) => {
        e.preventDefault();
        taskService.editTask(taskJson, taskId)
        .then(response => {
            setFailed(false);
            setAlert("Task updated successfully!");
            getTasksList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to update task!");
        });
    }

    const removeTask = (e) => {
        e.preventDefault();
        setDisplayConfirmation(false);
        taskService.removeTask(taskJson, taskId)
        .then(response => {
            setFailed(false);
            setAlert(response.message);
            getTasksList();
        }).catch(error => {
            setFailed(true);
            setAlert("Failed to delete task!");
            getTasksList();
        });
    }

    const onDeleteButtonClick = (data) => {
        setDeleteTask(data.name);
        setTaskId(data.id);
        setDisplayConfirmation(true);
    }

    const onViewButtonClick = (data) => {
        history.push(`/tickets/show/${data.ticket.id}`)
    }

    const tasksTableHeader = (
        <div className="table-header">
            List of Tasks
            <div className="p-grid">
                <div className="p-col-8 p-md-6">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText value={globalFilter} onChange={(e) => setGlobalFilter(e.target.value)} placeholder="Global Search" />
                    </span>
                </div>
                <div className="p-col-8 p-md-6">
                    <Link to="/tasks/create" style={{float: "right"}}><Button label="Add Task" icon="pi pi-user-plus"/></Link>
                </div>
            </div>
        </div>
    );

    const bodyTemplate = (data, props) => {
        return (
            <>
                <span className="p-column-title">{props.header}</span>
                {data[props.field]}
            </>
        );
    };

    const taskBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Name</span>
                <span style={{ marginLeft: '.5em', verticalAlign: 'middle' }} className="image-text">{data.name}</span>
            </>
        );
    };

    const statusBodyTemplate = (data) => {
        return (
            <>
                <span className="p-column-title">Status</span>
                <span className={`customer-badge status-${data.enabled ? 'qualified' : 'unqualified'}`}>{data.enabled ? 'Open' : 'Closed'}</span>
                <Link to="#" onClick={() => onStatusButtonClick(data)} className={`customer-badge status-${data.enabled ? 'unqualified' : 'qualified'}`}>
                    <i className={`pi pi-${data.enabled ? 'times' : 'check'}`}></i>
                </Link>
            </>
        )
    };

    const actionTemplate = (data) => {
        return (
            <div className="p-d-flex">
                <Button type="button" icon="pi pi-eye" onClick={() => onViewButtonClick(data)} style={{ marginRight: '.25em' }} tooltip="View Task" />
                <Button type="button" icon="pi pi-user-edit" className="p-button-warning" onClick={() => onEditButtonClick(data)} style={{ marginRight: '.25em' }} title="Edit Task" />
                <Button type="button" icon="pi pi-trash" className="p-button-danger" onClick={() => onDeleteButtonClick(data)}></Button>
            </div>
        )
    };

    const confirmationDialogFooter = (
        <>
            <Button type="button" label="No" icon="pi pi-times" onClick={() => setDisplayConfirmation(false)} className="p-button-text p-button-success" />
            <Button type="button" label="Yes" icon="pi pi-check" onClick={(e) => removeTask(e)} className="p-button-text p-button-danger" autoFocus />
        </>
    );

    const dialogFooter = <div className="p-grid">
        <div className="p-col-4 p-offset-2">
            <Button type="submit" label="Save" onClick={(e) => submitTask(e)} icon="pi pi-check-circle" className="p-button-success" />
        </div>
        <div className="p-col-4">
            <Button type="button" label="Cancel" onClick={() => setDisplayTaskForm(false)} icon="pi pi-times-circle" className="p-button-warning" />
        </div>
    </div>;

    return (
        <div className="p-grid table-demo">
            <div className="p-col-12">
                {/* RESPONSE ALERTS */}
                <ResponseAlert failed={failed} alert={alert} setFailed={setFailed} />

                {/* LIST TASK DATATABLE */}
                { tasks.length > 0 &&
                    <>
                        <DataTable value={tasks} paginator={false} className="p-datatable-customers" rows={10} dataKey="id" rowHover selection={selectedTask} onSelectionChange={(e) => setSelectedTask(e.value)}
                            sortField={sortField} sortOrder={sortOrder} onSort={onSortChange} responsive={true}
                            globalFilter={globalFilter} emptyMessage="No tasks found." loading={loading} header={tasksTableHeader}>
                            <Column field="name" header="Name" sortable body={bodyTemplate}></Column>
                            <Column field="ticket.name" header="Ticket" sortable></Column>
                            <Column field="description" header="Description" sortable body={bodyTemplate}></Column>
                            <Column headerStyle={{ width: '8rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible', justifyContent: 'center' }} body={actionTemplate}></Column>
                        </DataTable>
                        <Paginator currentPageReportTemplate={"Showing " + ((pageIndex * pageSize) + 1) + " to " + ((pageIndex * pageSize) + pageSize) + " of " + totalElements}
                            first={first} rows={pageSize} rowsPerPageOptions={[10,20,30]} onPageChange={onPageChange} totalPages={totalPages} totalRecords={totalElements}
                            paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown">
                        </Paginator>
                    </>
                }
                { tasks.length <= 0 &&
                    <div className="p-col-12 p-pl-3 p-text-center">
                        <h5 className="p-text-bold p-text-center">You don't have any Task</h5>
                        <span>Agents will be assigned with various tasks to support your customers</span>
                        <div className="p-col-12 p-md-12 p-lg-12 p-text-center" >
                            <img src="assets/layout/images/dashboard/tasks.svg" alt="Max Desk" className="logo" style={{width: "20%"}} />
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="button" onClick={() => setDisplayTaskForm(true)} label="Add Task Now" icon="pi pi-plus-circle" />
                        </div>
                    </div>
                }

                {/* EDIT TASK RIGHT SIDE FORM */}
                <Sidebar visible={visibleRight} onHide={() => setVisibleRight(false)} baseZIndex={1000} position="right">
                    <form onSubmit={updateTask}>
                        <div className="p-grid">
                            <div className="p-col-12">
                                <div className="card">
                                    <h5>Edit Task</h5>
                                    <div className="p-fluid p-formgrid p-grid">
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="name">Task</label>
                                            <InputText type="text" name="name" value={taskJson.name} onChange={e => setTaskJson({...taskJson, name: e.target.value})} required placeholder="Enter Task Name" />
                                        </div>
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="description">Description</label>
                                            <InputText type="text" name="description" value={taskJson.description} onChange={e => setTaskJson({...taskJson, description: e.target.value})} required placeholder="Enter Description" />
                                        </div>
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="ticketId">Ticket</label>
                                            <Dropdown id="ticketId" value={taskJson.ticket.id} onChange={(e) => setTaskJson({...taskJson, ticket: {id: e.value}})} options={tickets} optionLabel="name" optionValue="id" filter filterBy="name" placeholder="Select Ticket"></Dropdown>
                                        </div>
                                        <div className="p-field p-col-12 p-md-12">
                                            <label htmlFor="agentId">Agent</label>
                                            <Dropdown id="agentId" value={taskJson.agent.id} onChange={(e) => setTaskJson({...taskJson, agent: {id: e.value}})} options={users} optionLabel="fullName" optionValue="id" filter filterBy="fullName" placeholder="Select Agent"></Dropdown>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="p-col-12 p-text-center">
                            <Button type="submit" onClick={() => setVisibleRight(false)} label="Save" className="p-button-success" style={{ marginRight: '.25em' }} autoFocus={true} />
                            <Button type="button" onClick={() => setVisibleRight(false)} label="Cancel" className="p-button-secondary" />
                        </div>
                    </form>
                </Sidebar>

                {/* DELETE TASK CONFIRMATION DIALOG */}
                <Dialog header="Confirmation" visible={displayConfirmation} onHide={() => setDisplayConfirmation(false)} style={{ width: '350px' }} modal footer={confirmationDialogFooter}>
                    <div className="confirmation-content">
                        <i className="pi pi-exclamation-triangle p-mr-3" style={{ fontSize: '2rem' }} />
                        <span>Are you sure you want to delete {deleteTask}?</span>
                    </div>
                </Dialog>
                <Dialog header="New Task" visible={displayTaskForm} className="p-col-4 p-offset-2" modal footer={dialogFooter} onHide={() => setDisplayTaskForm(false)}>
                    <div className="p-fluid p-formgrid p-grid">
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="name">Task</label>
                            <InputText type="text" name="name" value={taskJson.name} onChange={e => setTaskJson({...taskJson, name: e.target.value})} required placeholder="Enter Task Name" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="description">Description</label>
                            <InputText type="text" name="description" value={taskJson.description} onChange={e => setTaskJson({...taskJson, description: e.target.value})} required placeholder="Enter Description" />
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="ticketId">Ticket</label>
                            <Dropdown id="ticketId" value={taskJson.ticket.id} onChange={(e) => setTaskJson({...taskJson, ticket: {id: e.value}})} options={tickets} optionLabel="name" optionValue="id" filter filterBy="name" placeholder="Select Ticket" appendTo={document.body}></Dropdown>
                        </div>
                        <div className="p-field p-col-12 p-md-12">
                            <label htmlFor="agentId">Agent</label>
                            <Dropdown id="agentId" value={taskJson.agent.id} onChange={(e) => setTaskJson({...taskJson, agent: {id: e.value}})} options={users} optionLabel="fullName" optionValue="id" filter filterBy="fullName" placeholder="Select Agent" appendTo={document.body}></Dropdown>
                        </div>
                    </div>
                </Dialog>
            </div>
        </div>
    )
}

import { ACCESS_TOKEN, TENANT_ID } from '../constants';

export const loginRequest = async (options) => {
    const headers = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa('dvicore:admin@1'),
    })

    headers.append('tenantId', 0)

    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    const response = await fetch(options.url, options);
    const json = await response.json();
    if (!response.ok) {
        return Promise.reject(json);
    }
    return json;
};

export const postForms = async (options) => {
    const headers = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
    })

    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }
    if (localStorage.getItem(TENANT_ID)) {
        headers.append('tenantId', localStorage.getItem(TENANT_ID) || 0)
    } else {
        headers.append('tenantId', localStorage.getItem(TENANT_ID) || 0)
    }

    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            })
                // .catch(error => {
                //     isAuthorised(error);
                // })
        );
};

export const postFiles = async (options) => {
    const headers = new Headers({
    })

    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }
    if (localStorage.getItem(TENANT_ID)) {
        headers.append('tenantId', localStorage.getItem(TENANT_ID) || 0)
    }

    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            })
                // .catch(error => {
                //     isAuthorised(error);
                // })
        );
};

export const postRequest = async (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json',
    })

    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }

    if (localStorage.getItem(TENANT_ID)) {
        headers.append('tenantId', localStorage.getItem(TENANT_ID) || 0)
    } else {
        headers.append('tenantId', localStorage.getItem(TENANT_ID) || 0)
    }

    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            })
                // .catch(error => {
                //     isAuthorised(error);
                // })
        );
};

export const getRequest = async (options) => {
    const headers = new Headers({})
    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }
    if (localStorage.getItem(TENANT_ID) || 0) {
        headers.append('tenantId', localStorage.getItem(TENANT_ID) || 0)
    }
    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);
    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            })
        );
};

function isAuthorised(response) {
    if (response.error === "invalid_token") {
        localStorage.clear();
        return false;
    }
    return true;
}

export const queryParams = params => {
    if (!params)
        return ""

    let out = Object.keys(params).map((value, key) => {
        if (key === 0)
            return `?${value}=${params[value]}`
        else
            return `&${value}=${params[value]}`
    })
    return out.join("")
}

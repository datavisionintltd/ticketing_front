import { CLIENT } from '../api';
import { getRequest, postRequest, queryParams } from './CoreService';

export default class ClientService {

    addClient(clientJson) {
        return postRequest({
            url: CLIENT,
            method: 'POST',
            body: JSON.stringify(clientJson)
        });
    }

    editClient(clientJson) {
        return postRequest({
            url: CLIENT,
            method: 'PATCH',
            body: JSON.stringify(clientJson)
        });
    }

    removeClient(clientId) {
        return postRequest({
            url: CLIENT + "/" + clientId,
            method: 'DELETE'
        });
    }

    getClient(clientId) {
        return getRequest({
            url: CLIENT + "/" + clientId,
            method: 'GET'
        });
    }

    getClients(paramsJson) {
        return getRequest({
            url: CLIENT + queryParams(paramsJson),
            method: 'GET'
        });
    }

    getClientTicketStatuses(paramsJson) {
        return getRequest({
            url: CLIENT + "/ticket/statuses" + queryParams(paramsJson),
            method: 'GET'
        });
    }
}

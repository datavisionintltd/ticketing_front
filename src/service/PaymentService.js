import { PAYMENTS } from '../api';
import { getRequest, postRequest } from './CoreService';

export default class PaymentService {

    getTransactions(paramsJson) {
        return getRequest({
            url: PAYMENTS + "?" + paramsJson.searchKey + paramsJson.page + paramsJson.limit + paramsJson.sort + paramsJson.order,
            method: 'GET'
        });
    }

    getTransactionLog(logId) {
        return getRequest({
            url: PAYMENTS + "/log/" + logId,
            method: 'GET'
        });
    }
}

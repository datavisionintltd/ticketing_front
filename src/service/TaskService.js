import { TASKS } from '../api';
import { getRequest, postRequest, queryParams } from './CoreService';

export default class TaskService {

    addTask(taskJson) {
        return postRequest({
            url: TASKS + "/",
            method: 'POST',
            body: JSON.stringify(taskJson)
        });
    }

    getTask(taskId) {
        return getRequest({
            url: TASKS + "/" + taskId,
            method: 'GET'
        });
    }

    getTasks(paramsJson) {
        return getRequest({
            url: TASKS + queryParams(paramsJson),
            method: 'GET'
        });
    }

    editTask(taskJson, taskId) {
        return postRequest({
            url: TASKS + "/" + taskId,
            method: 'PUT',
            body: JSON.stringify(taskJson)
        });
    }

    removeTask(taskJson, taskId) {
        return postRequest({
            url: TASKS + "/" + taskId,
            method: 'DELETE',
            body: JSON.stringify(taskJson)
        });
    }

    getMessages(taskId) {
        return getRequest({
            url: `${TASKS}/${taskId}/messages`,
            method: 'GET'
        })
    }

    submitReply(taskId, replyJson) {
        return postRequest({
            url: `${TASKS}/${taskId}/messages`,
            method: 'POST',
            body: JSON.stringify(replyJson)
        })
    }

}

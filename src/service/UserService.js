import { DEPARTMENT, OAUTH, ROLE, TEAM, TEAM_USER, USER} from '../api';
import { getRequest, postRequest, queryParams } from './CoreService';

export default class UserService {

    signUp(userJson) {
        return postRequest({
            url: OAUTH + "/sign/up",
            method: 'POST',
            body: JSON.stringify(userJson)
        });
    }

    addUser(userJson) {
        return postRequest({
            url: USER,
            method: 'POST',
            body: JSON.stringify(userJson)
        });
    }

    editUser(userJson) {
        return postRequest({
            url: USER,
            method: 'PUT',
            body: JSON.stringify(userJson)
        });
    }

    enableUser(enableJson) {
        console.log(JSON.stringify(enableJson));
        return postRequest({
            url: USER + "/enable",
            method: 'POST',
            body: JSON.stringify(enableJson)
        });
    }

    resetPassword(userJson) {
        return postRequest({
            url: USER + "/reset/password",
            method: 'POST',
            body: JSON.stringify(userJson)
        });
    }

    removeUser(userId) {
        return postRequest({
            url: USER + "/" + userId,
            method: 'DELETE'
        });
    }

    getUser(userId) {
        return getRequest({
            url: USER + "/" + userId,
            method: 'GET'
        });
    }

    getUsersByRole(role_name) {
        return getRequest({
            url: `${ROLE}/${role_name}/users`,
            method: 'GET'
        })
    }

    getUserDetails() {
        return getRequest({
            url: OAUTH + "/details",
            method: 'GET'
        });
    }

    getUsers(paramsJson) {
        return getRequest({
            url: USER + queryParams(paramsJson),
            method: 'GET'
        });
    }

    addUserRole(userJson) {
        return postRequest({
            url: USER + "/role",
            method: 'POST',
            body: JSON.stringify(userJson)
        });
    }

    removeUserRole(userJson) {
        return postRequest({
            url: USER + "/role",
            method: 'DELETE',
            body: JSON.stringify(userJson)
        });
    }

    removeUserRoles(userJson) {
        return postRequest({
            url: USER + "/role",
            method: 'DELETE',
            body: JSON.stringify(userJson)
        });
    }

    addRole(roleJson) {
        return postRequest({
            url: ROLE,
            method: 'POST',
            body: JSON.stringify(roleJson)
        });
    }

    editRole(roleJson) {
        return postRequest({
            url: ROLE,
            method: 'PUT',
            body: JSON.stringify(roleJson)
        });
    }

    removeRole(roleId) {
        return postRequest({
            url: ROLE + "/" + roleId,
            method: 'DELETE'
        });
    }

    getRoles() {
        return getRequest({
            url: ROLE,
            method: 'GET'
        });
    }

    addDepartment(depJson) {
        return postRequest({
            url: DEPARTMENT,
            method: 'POST',
            body: JSON.stringify(depJson)
        });
    }

    addDepartments(depArray) {
        return postRequest({
            url: DEPARTMENT + "/list",
            method: 'POST',
            body: JSON.stringify(depArray)
        });
    }

    editDepartment(depJson) {
        return postRequest({
            url: DEPARTMENT,
            method: 'PUT',
            body: JSON.stringify(depJson)
        });
    }

    removeDepartment(depId) {
        return postRequest({
            url: DEPARTMENT + "/" + depId,
            method: 'DELETE'
        });
    }

    getDepartment(depId) {
        return getRequest({
            url: DEPARTMENT + "/" + depId,
            method: 'GET'
        });
    }

    getDepartments(paramsJson) {
        return getRequest({
            url: DEPARTMENT + queryParams(paramsJson),
            method: 'GET'
        });
    }

    addTeam(teamJson) {
        return postRequest({
            url: TEAM,
            method: 'POST',
            body: JSON.stringify(teamJson)
        });
    }

    editTeam(teamJson) {
        return postRequest({
            url: TEAM,
            method: 'PUT',
            body: JSON.stringify(teamJson)
        });
    }

    removeTeam(teamId) {
        return postRequest({
            url: TEAM + "/" + teamId,
            method: 'DELETE'
        });
    }

    getTeam(teamId) {
        return getRequest({
            url: TEAM + "/" + teamId,
            method: 'GET'
        });
    }

    getTeams(paramsJson) {
        return getRequest({
            url: TEAM + queryParams(paramsJson),
            method: 'GET'
        });
    }

    addTeamUser(teamMemberJson) {
        return postRequest({
            url: TEAM_USER,
            method: 'POST',
            body: JSON.stringify(teamMemberJson)
        });
    }

    removeTeamUser(teamMemberId) {
        return postRequest({
            url: TEAM_USER + "/" + teamMemberId,
            method: 'DELETE'
        });
    }

    getTeamUsers(paramsJson) {
        return getRequest({
            url: TEAM_USER + queryParams(paramsJson),
            method: 'GET'
        });
    }
}

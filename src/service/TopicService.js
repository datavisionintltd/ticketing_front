import {
    EMAIL_TEMPLATE,
    TOPICS,
    SLA,
    SETTINGS,
    COMMON,
    DICTIONARY,
    USER,
    SETTING,
    ITEMS,
    DICTIONARY_ITEMS_BY_CODES
} from '../api';
import { getRequest, postRequest, queryParams } from './CoreService';

export default class TopicService {

    addTopic(topicJson) {
        return postRequest({
            url: TOPICS,
            method: 'POST',
            body: JSON.stringify(topicJson)
        });
    }

    addTopics(topicArray) {
        return postRequest({
            url: TOPICS + "/list",
            method: 'POST',
            body: JSON.stringify(topicArray)
        });
    }

    editTopic(topicJson) {
        return postRequest({
            url: TOPICS,
            method: 'PUT',
            body: JSON.stringify(topicJson)
        });
    }

    getTopic(paramsJson) {
        return getRequest({
            url: COMMON + "/topics" + queryParams(paramsJson),
            method: 'GET'
        });
    }

    deleteTopic(topicId) {
        return postRequest({
            url: TOPICS + "/" + topicId,
            method: 'DELETE'
        });
    }

    getTopics(paramsJson) {
        return getRequest({
            url: TOPICS + queryParams(paramsJson),
            method: 'GET'
        });
    }
}

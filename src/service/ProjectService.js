import { PROJECTS } from '../api';
import { getRequest, postRequest, queryParams } from './CoreService';

export default class ProjectService {

    addProject(projectjson) {
        return postRequest({
            url: PROJECTS,
            method: 'POST',
            body: JSON.stringify(projectjson)
        });
    }

    editProject(projectjson) {
        return postRequest({
            url: PROJECTS,
            method: 'PUT',
            body: JSON.stringify(projectjson)
        });
    }

    removeProject(projectId) {
        return postRequest({
            url: PROJECTS + "/" + projectId,
            method: 'DELETE'
        });
    }

    getProject(projectId) {
        return getRequest({
            url: PROJECTS + "/" + projectId,
            method: 'GET'
        });
    }

    getProjects(paramsJson) {
        return getRequest({
            url: PROJECTS + queryParams(paramsJson),
            method: 'GET'
        });
    }

    getProjectTicketStatuses(paramsJson) {
        return getRequest({
            url: PROJECTS + "/ticket/statuses" + queryParams(paramsJson),
            method: 'GET'
        });
    }
}

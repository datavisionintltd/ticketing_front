import {
    EMAIL_TEMPLATE,
    TOPICS,
    SLA,
    SETTINGS,
    COMMON,
    DICTIONARY,
    USER,
    SETTING,
    ITEMS,
    DICTIONARY_ITEMS_BY_CODES
} from '../api';
import { getRequest, postRequest, queryParams } from './CoreService';

export default class DictionaryService {

    editDictionary(userJson) {
        return postRequest({
            url: DICTIONARY,
            method: 'PUT',
            body: JSON.stringify(userJson)
        });
    }
    editDictionaryItem(userJson) {
        return postRequest({
            url: ITEMS,
            method: 'PUT',
            body: JSON.stringify(userJson)
        });
    }
    editSla(slaJson) {
        return postRequest({
            url: SLA,
            method: 'PUT',
            body: JSON.stringify(slaJson)
        });
    }
    editTemplate(slaJson) {
        return postRequest({
            url: EMAIL_TEMPLATE ,
            method: 'PUT',
            body: JSON.stringify(slaJson)
        });
    }

    editSetting(settingJson) {
        return postRequest({
            url: SETTING,
            method: 'PUT',
            body: JSON.stringify(settingJson)
        });
    }

    deleteDictionary(dictionaryId) {
        return postRequest({
            url: DICTIONARY + "/" + dictionaryId,
            method: 'DELETE'
        });
    }
    deleteDictionaryItem(dictionaryItemId) {
        return postRequest({
            url: ITEMS + "/" + dictionaryItemId,
            method: 'DELETE'
        });
    }
    deleteSla(slaId) {
        return postRequest({
            url: SLA + "/" + slaId,
            method: 'DELETE'
        });
    }
    deleteTemplate(templateId) {
        return postRequest({
            url: EMAIL_TEMPLATE + "/" + templateId,
            method: 'DELETE'
        });
    }

    deleteGeneralSetting(settingId) {
        return postRequest({
            url: SETTING + "/" + settingId,
            method: 'DELETE'
        });
    }

    viewItems(dictionaryId) {
        return getRequest({
            url: DICTIONARY + "/item/" + dictionaryId,
            method: 'GET'
        });
    }

    addDictionary(dictionaryJson) {
        return postRequest({
            url: DICTIONARY,
            method: 'POST',
            body: JSON.stringify(dictionaryJson)
        });
    }
    addDictionaryItem(dictionaryJson) {
        return postRequest({
            url: ITEMS,
            method: 'POST',
            body: JSON.stringify(dictionaryJson)
        });
    }

    addSla(slaJson) {
        return postRequest({
            url: SLA,
            method: 'POST',
            body: JSON.stringify(slaJson)
        });
    }

    addSlas(slaArray) {
        return postRequest({
            url: SLA + "/list",
            method: 'POST',
            body: JSON.stringify(slaArray)
        });
    }

    updateSystemSettings(systemSettingsJson) {
        return postRequest({
            url: SETTINGS + "/systems",
            method: 'PUT',
            body: JSON.stringify(systemSettingsJson)
        });
    }
    updateTicketSettings(systemSettingsJson) {
        return postRequest({
            url: SETTINGS + "/tickets",
            method: 'PUT',
            body: JSON.stringify(systemSettingsJson)
        });
    }

    getDictionary(paramsJson) {
        return getRequest({
            url: DICTIONARY + queryParams(paramsJson),
            method: 'GET'
        });
    }
    getEmailTemplate() {
        return getRequest({
            url: EMAIL_TEMPLATE,
            method: 'GET'
        });
    }

    getSettings(paramsJson) {
        return getRequest({
            url: SETTING + queryParams(paramsJson),
            method: 'GET'
        });
    }

    getDictionaryItems(paramsJson) {
        return getRequest({
            url: DICTIONARY + "/item" + queryParams(paramsJson),
            method: 'GET'
        });
    }

    getStatus(paramsJson) {
        return getRequest({
            url: COMMON + "/statuses" + queryParams(paramsJson),
            method: 'GET'
        });
    }

    getSystemSettings() {
        return getRequest({
            url: SETTINGS + "/systems",
            method: 'GET'
        });
    }
    getTicketSettings() {
        return getRequest({
            url: SETTINGS + "/tickets",
            method: 'GET'
        });
    }
    getDictionaryItemsByCode(dictionaryCode) {
        return getRequest({
            url: ITEMS + "/"+dictionaryCode,
            method: 'GET'
        });
    }
    getSla(paramsJson) {
        return getRequest({
            url: SLA + queryParams(paramsJson),
            method: 'GET'
        });
    }
}

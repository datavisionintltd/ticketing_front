import { SUBSCRIPTIONS, PACKAGES } from '../api';
import { getRequest, postRequest, queryParams } from './CoreService';

export default class SubscriptionService {

    addSubscription(subscriptionjson) {
        return postRequest({
            url: SUBSCRIPTIONS,
            method: 'POST',
            body: JSON.stringify(subscriptionjson)
        });
    }

    editSubscription(subscriptionjson) {
        return postRequest({
            url: SUBSCRIPTIONS,
            method: 'PUT',
            body: JSON.stringify(subscriptionjson)
        });
    }

    removeSubscription(subscriptionId) {
        return postRequest({
            url: SUBSCRIPTIONS + "/" + subscriptionId,
            method: 'DELETE'
        });
    }

    getSubscription(subscriptionId) {
        return getRequest({
            url: SUBSCRIPTIONS + "/" + subscriptionId,
            method: 'GET'
        });
    }

    getSubscriptions(paramsJson) {
        return getRequest({
            url: SUBSCRIPTIONS + "?" + paramsJson.page + paramsJson.limit + paramsJson.sort + paramsJson.order,
            method: 'GET'
        });
    }

    addPackage(packagejson) {
        return postRequest({
            url: PACKAGES,
            method: 'POST',
            body: JSON.stringify(packagejson)
        });
    }

    editPackage(packagejson) {
        return postRequest({
            url: PACKAGES,
            method: 'PUT',
            body: JSON.stringify(packagejson)
        });
    }

    removePackage(packageId) {
        return postRequest({
            url: PACKAGES + "/" + packageId,
            method: 'DELETE'
        });
    }

    getPackage(packageId) {
        return getRequest({
            url: PACKAGES + "/" + packageId,
            method: 'GET'
        });
    }

    getPackages(params) {
        return getRequest({
            url: PACKAGES + queryParams(params),
            method: 'GET'
        });
    }

}

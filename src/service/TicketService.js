import { TICKETS } from '../api';
import { getRequest, postRequest, queryParams } from './CoreService';

export default class TicketService {

    addTicket(ticketjson) {
        return postRequest({
            url: TICKETS,
            method: 'POST',
            body: JSON.stringify(ticketjson)
        });
    }

    editTicket(ticketjson) {
        return postRequest({
            url: TICKETS,
            method: 'PUT',
            body: JSON.stringify(ticketjson)
        });
    }

    assignTicket(ticketjson) {
        return postRequest({
            url: TICKETS + "/assign",
            method: 'POST',
            body: JSON.stringify(ticketjson)
        });
    }

    removeTicket(ticketId) {
        return postRequest({
            url: TICKETS + "/" + ticketId,
            method: 'DELETE'
        });
    }

    getTicket(ticketId) {
        return getRequest({
            url: TICKETS + "/" + ticketId,
            method: 'GET'
        });
    }

    getTickets(paramsJson) {
        return getRequest({
            url: TICKETS + queryParams(paramsJson),
            method: 'GET'
        });
    }

    getTasksByTicket(ticketId, paramsJson) {
        return getRequest({
            url: `${TICKETS}/${ticketId}/tasks` + queryParams(paramsJson),
            method: 'GET'
        })
    }

    getMessages(ticketId) {
        return getRequest({
            url: `${TICKETS}/${ticketId}/messages`,
            method: 'GET'
        })
    }

    submitReply(ticketId, replyJson) {
        return postRequest({
            url: `${TICKETS}/${ticketId}/messages`,
            method: 'POST',
            body: JSON.stringify(replyJson)
        })
    }
}

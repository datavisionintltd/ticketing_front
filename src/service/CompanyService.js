import { COMPANIES } from '../api';
import { getRequest, postRequest, queryParams } from './CoreService';

export default class CompanyService {

    addCompany(companyJson) {
        return postRequest({
            url: COMPANIES,
            method: 'POST',
            body: JSON.stringify(companyJson)
        });
    }

    editCompany(companyJson) {
        return postRequest({
            url: COMPANIES,
            method: 'PUT',
            body: JSON.stringify(companyJson)
        });
    }

    removeCompany(companyId) {
        return postRequest({
            url: COMPANIES + "/" + companyId,
            method: 'DELETE'
        });
    }

    getCompany(companyId) {
        return getRequest({
            url: COMPANIES + "/" + companyId,
            method: 'GET'
        });
    }

    getCompanies(paramsJson) {
        return getRequest({
            url: COMPANIES + queryParams(paramsJson),
            method: 'GET'
        });
    }

    addAgent(agentJson) {
        return postRequest({
            url: COMPANIES + "/agents",
            method: 'POST',
            body: JSON.stringify(agentJson)
        });
    }

    getAgents(paramsJson) {
        return getRequest({
            url: COMPANIES + "/agents" + queryParams(paramsJson),
            method: 'GET'
        });
    }

    getAgentTicketStatuses(paramsJson) {
        return getRequest({
            url: COMPANIES + "/ticket/statuses" + queryParams(paramsJson),
            method: 'GET'
        });
    }

}

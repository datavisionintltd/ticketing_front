import { API_BASE_URL, OAUTH, USER } from '../api';
import { getRequest, loginRequest, postRequest, queryParams } from './CoreService';

export default class AuthService {

    logIn(username, password) {
        const formData = new URLSearchParams();
        formData.append('username', username);
        formData.append('password', password);
        formData.append('grant_type', "password");
        return loginRequest({
            url: API_BASE_URL + "/oauth/token",
            method: 'POST',
            body: formData
        });
    }

    signOut() {
        return getRequest({
            url: OAUTH + "/logout",
            method: 'GET'
        });
    }

    signUp(signupRequest) {
        return postRequest({
            url: OAUTH + "/sign/up",
            method: 'POST',
            body: JSON.stringify(signupRequest)
        });
    }

    activateUserAccount(signupRequest) {
        return postRequest({
            url: OAUTH + "/activate",
            method: 'POST',
            body: JSON.stringify(signupRequest)
        });
    }

    resendActivationCode(signupRequest) {
        return postRequest({
            url: OAUTH + "/resend/activation/code",
            method: 'POST',
            body: JSON.stringify(signupRequest)
        });
    }

    passwordResetRequest(restJson) {
        return postRequest({
            url: OAUTH + "/password/reset/request",
            method: 'POST',
            body: JSON.stringify(restJson)
        });
    }

    resetPassword(signupRequest) {
        return postRequest({
            url: OAUTH + "/reset/password",
            method: 'POST',
            body: JSON.stringify(signupRequest)
        });
    }

    registerCompany(signupRequest) {
        return postRequest({
            url: OAUTH + "/register",
            method: 'POST',
            body: JSON.stringify(signupRequest)
        });
    }

    getCountries(paramsJson) {
        return getRequest({
            url: OAUTH + "/countries" + queryParams(paramsJson),
            method: 'GET'
        });
    }
}

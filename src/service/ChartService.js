import { CHARTS } from '../api';
import { getRequest } from './CoreService';

export default class ChartService {

    getCharts(since) {
        return getRequest({
            url: CHARTS + "/status?since=" + since,
            method: 'GET'
        });
    }

}

import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import { Redirect, Route } from 'react-router-dom';

import AppTopbar from './AppTopbar';
import AppBreadcrumb from './AppBreadcrumb';
import AppFooter from './AppFooter';
import AppMenu from './AppMenu';
import AppConfig from './AppConfig';

import PrimeReact from 'primereact/utils';

import { ProgressBar } from 'primereact/progressbar'

import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import './App.scss';

import { ACCESS_TOKEN, FULL_NAME, SECURED, ROLES, TENANT } from './constants';
import { ListUsers } from './components/user/ListUsers';
import { AddUser } from './components/user/AddUser';
import { ListRoles } from './components/user/ListRoles';
import { ListTasks } from './components/task/ListTasks';
import { AddTask } from './components/task/AddTask';
import { ListDepartments } from './components/user/ListDepartments';
import { Profile } from './components/user/Profile';
import { ListTeams } from './components/user/ListTeam';
import { Transactions } from './components/payment/Transactions';
import { TransactionsLogs } from './components/payment/TransactionsLogs';
import { AddDictionary } from './components/dictionary/AddDictionary';
import { AddDictionaryItem } from './components/dictionary/AddDictionaryItem';
import { DictionaryList } from './components/dictionary/DictionaryList';
import { DictionaryItemList } from './components/dictionary/DictionaryItemList';
import { General } from './components/settings/General';
import { UpdateTicketSettings } from './components/settings/UpdateTicketSettings';
import { UpdateSystemSettings } from './components/settings/UpdateSystemSettings';
import { SlaList } from './components/sla/SlaList';
import { AddSla } from './components/sla/AddSla';
import { TopicList } from './components/topic/TopicList';
import { AddTopic } from './components/topic/AddTopic';
import { EmailTemplateList } from './components/emailTemplate/EmailTemplateList';

import { ListProjects } from './components/projects/ListProjects'
import { CreateProject } from './components/projects/CreateProject'

import { ListTickets } from './components/tickets/ListTickets'
import { CreateTicket } from './components/tickets/CreateTicket'
import { AddSubscription } from './components/subscriptions/AddSubscription';
import { Companies } from './components/companies/Companies';
import { AddCompany } from './components/companies/AddCompany';
import { AddPackage } from './components/subscriptions/AddPackage';
import { hasRole } from './utilities/Helpers';
import { Menu } from './Menu';
import { CustomRoute } from './utilities/components/CustomRoute';
import { Dashboard } from './components/Dashboard';
import { ResetingPassword } from './components/user/ResetingPassword';
import { ViewTicket } from './components/tickets/ViewTicket';
import { ListClients } from './components/companies/ListClients';
import { ListAgents } from './components/user/ListAgents';

const App = () => {

    const [layoutMode, setLayoutMode] = useState('static');
    const [overlayMenuActive, setOverlayMenuActive] = useState(false);
    const [staticMenuDesktopInactive, setStaticMenuDesktopInactive] = useState(false);
    const [staticMenuMobileActive, setStaticMenuMobileActive] = useState(false);
    const [topbarMenuActive, setTopbarMenuActive] = useState(false);
    const [activeTopbarItem, setActiveTopbarItem] = useState(null);
    const [menuActive, setMenuActive] = useState(false);
    const [themeColor, setThemeColor] = useState('blue');
    const [inputStyle, setInputStyle] = useState('outlined');
    const [ripple, setRipple] = useState(false);
    const [scheme, setScheme] = useState('light');

    let menuClick;
    let topbarItemClick;

    useEffect(() => {
        if (hasRole("ROLE_ADMIN", "ROLE_SUPER_ADMIN")) {
            setLayoutMode('static')
        } else if (hasRole("ROLE_AGENT")) {
            setLayoutMode('horizontal')
        } else if (hasRole("ROLE_USER")) {
            setLayoutMode('overlay')
        }
    }, [layoutMode])

    const onInputStyleChange = (inputStyle) => {
        setInputStyle(inputStyle);
    }

    const onRippleChange = (e) => {
        PrimeReact.ripple = e.value;
        setRipple(e.value);
    }

    const onMenuClick = (event) => {
        menuClick = true;
    }

    const onMenuButtonClick = (event) => {
        menuClick = true;
        setTopbarMenuActive(false);

        if (layoutMode === 'overlay' && !isMobile()) {
            setOverlayMenuActive(prevState => !prevState);
        } else {
            if (isDesktop())
                setStaticMenuDesktopInactive(prevState => !prevState);
            else
                setStaticMenuMobileActive(prevState => !prevState);
        }

        event.preventDefault();
    }

    const onTopbarMenuButtonClick = (event) => {
        topbarItemClick = true;
        setTopbarMenuActive(prevState => !prevState)
        hideOverlayMenu();
        event.preventDefault();
    }

    const onTopbarItemClick = (event) => {
        topbarItemClick = true;

        if (activeTopbarItem === event.item)
            setActiveTopbarItem(null);
        else
            setActiveTopbarItem(event.item);

        event.originalEvent.preventDefault();
    }

    const onMenuItemClick = (event) => {
        if (!event.item.items) {
            hideOverlayMenu();
        }
        if (!event.item.items && (isHorizontal() || isSlim())) {
            setMenuActive(false);
        }
    }

    const onRootMenuItemClick = (event) => {
        setMenuActive(prevState => !prevState);
    }

    const onDocumentClick = (event) => {
        if (!topbarItemClick) {
            setActiveTopbarItem(null)
            setTopbarMenuActive(false)
        }

        if (!menuClick) {
            if (isHorizontal() || isSlim()) {
                setMenuActive(false)
            }

            hideOverlayMenu();
        }

        topbarItemClick = false;
        menuClick = false;
    }

    const isMenuVisible = () => {
        if (isDesktop()) {
            if (layoutMode === 'static')
                return !staticMenuDesktopInactive;
            else if (layoutMode === 'overlay')
                return overlayMenuActive;
            else
                return true;
        }
        else {
            return true;
        }
    };

    const hideOverlayMenu = () => {
        setOverlayMenuActive(false);
        setStaticMenuMobileActive(false)
    }

    const isMobile = () => {
        return window.innerWidth < 1025;
    }

    const isDesktop = () => {
        return window.innerWidth > 1024;
    }

    const isHorizontal = () => {
        return layoutMode === 'horizontal';
    }

    const isSlim = () => {
        return layoutMode === 'slim';
    }

    const changeMenuMode = (event) => {
        setLayoutMode(event.menuMode);
        setStaticMenuDesktopInactive(false);
        setOverlayMenuActive(false);
    }

    const onSchemeChange = (color) => {
        setScheme(color);
        const themeLink = document.getElementById('theme-css');
        const href = themeLink.href;
        const themeFile = href.substring(href.lastIndexOf('/') + 1, href.lastIndexOf('.'));
        const themeTokens = themeFile.split('-');
        const themeName = themeTokens[1];
        changeTheme(themeName + '-' + color);
        changeLogo(color);
    }

    const changeTheme = (theme) => {
        setThemeColor(theme.split('-')[0]);
        changeStyleSheetUrl('layout-css', theme, 'layout');
        changeStyleSheetUrl('theme-css', theme, 'theme');
    }

    const onThemeChange = (theme) => {
        setThemeColor(theme)
        changeTheme(theme + '-' + scheme);
    }

    const changeStyleSheetUrl = (id, value, prefix) => {
        let element = document.getElementById(id);
        let urlTokens = element.getAttribute('href').split('/');
        urlTokens[urlTokens.length - 1] = prefix + '-' + value + '.css';
        let newURL = urlTokens.join('/');

        replaceLink(element, newURL);
    }

    const changeLogo = (scheme) => {
        const invoiceLogoLink = document.getElementById("invoice-logo");
        const logoUrl = `assets/layout/images/logo-${scheme === 'light' ? 'dark' : 'white'}.png`;

        if (invoiceLogoLink) {
            invoiceLogoLink.src = logoUrl;
        }
    };

    const isIE = () => {
        return /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent)
    }

    const replaceLink = (linkElement, href) => {
        if (isIE()) {
            linkElement.setAttribute('href', href);
        }
        else {
            const id = linkElement.getAttribute('id');
            const cloneLinkElement = linkElement.cloneNode(true);

            cloneLinkElement.setAttribute('href', href);
            cloneLinkElement.setAttribute('id', id + '-clone');

            linkElement.parentNode.insertBefore(cloneLinkElement, linkElement.nextSibling);

            cloneLinkElement.addEventListener('load', () => {
                linkElement.remove();
                cloneLinkElement.setAttribute('id', id);
            });
        }
    }

    const layoutClassName = classNames('layout-wrapper', {
        'layout-horizontal': layoutMode === 'horizontal',
        'layout-overlay': layoutMode === 'overlay',
        'layout-static': layoutMode === 'static',
        'layout-slim': layoutMode === 'slim',
        'layout-static-inactive': staticMenuDesktopInactive && layoutMode !== 'slim',
        'layout-mobile-active': staticMenuMobileActive,
        'layout-overlay-active': overlayMenuActive,
        'p-input-filled': inputStyle === 'filled'
    });

    const menuContainerClassName = classNames('layout-menu-container', { 'layout-menu-container-inactive': !isMenuVisible() })

    return (
        <div className={layoutClassName} onClick={onDocumentClick}>
            { !localStorage.getItem(ACCESS_TOKEN) ? <Redirect to="/login" /> : ""}
            <AppTopbar
                topbarMenuActive={topbarMenuActive} activeTopbarItem={activeTopbarItem}
                onMenuButtonClick={onMenuButtonClick}
                onTopbarMenuButtonClick={onTopbarMenuButtonClick}
                onTopbarItemClick={onTopbarItemClick}
                tenant={localStorage.getItem(TENANT)}
                fullName={localStorage.getItem(FULL_NAME)} />

            <div className={menuContainerClassName} onClick={onMenuClick}>
                <div className="layout-menu-content">
                    <div className="layout-menu-title">MENU</div>
                    <AppMenu model={Menu()} onMenuItemClick={onMenuItemClick}
                        onRootMenuItemClick={onRootMenuItemClick}
                        layoutMode={layoutMode} active={menuActive} />
                    {/* <div className="layout-menu-footer">
                        <div className="layout-menu-footer-title">TASKS</div>

                        <div className="layout-menu-footer-content">
                            <ProgressBar value={50} showValue={false}></ProgressBar>
                                Today
                            <ProgressBar value={80} showValue={false}></ProgressBar>
                            Overall
                        </div>
                    </div> */}
                </div>
            </div>

            <div className="layout-content">
                <AppBreadcrumb />

                <div className="layout-content-container">
                    <Route path="/" exact render={(props) => <Dashboard notSecured={localStorage.getItem(SECURED)} {...props}/>}></Route>
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_AGENT", "ROLE_SUPER_ADMIN"]} path="/payments/transactions" component={Transactions} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/payments/logs/:id" component={TransactionsLogs} />
                    <CustomRoute roles={["ROLE_USER", "ROLE_ADMIN", "ROLE_AGENT", "ROLE_SUPER_ADMIN"]} path="/users/profile" component={Transactions} />
                    <CustomRoute roles={["ROLE_USER", "ROLE_ADMIN", "ROLE_AGENT", "ROLE_SUPER_ADMIN"]} path="/user/reseting/password" component={ResetingPassword} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/users" component={ListUsers} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/users/new" component={AddUser} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/users/roles" component={ListRoles} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/agents" component={ListAgents} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_AGENT", "ROLE_SUPER_ADMIN"]} path="/tasks" component={ListTasks} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/tasks/create" component={AddTask} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/departments" component={ListDepartments} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/teams" component={ListTeams} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/dictionaries/create" component={Transactions} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/dictionaries" component={Transactions} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/dictionary/items" component={Transactions} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/dictionary/item/create/:dictionaryId" component={Transactions} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/dictionary/items/:dictionaryId" component={Transactions} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/settings" component={General} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/settings/tickets" component={UpdateTicketSettings} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/settings/systems" component={UpdateSystemSettings} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/settings/subjects" component={DictionaryList} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/settings/slas" component={SlaList} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/settings/emails" component={EmailTemplateList} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/settings/sla/create" component={AddSla} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/settings/topics" component={TopicList} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/settings/topic/create" component={AddTopic} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_USER", "ROLE_AGENT", "ROLE_SUPER_ADMIN"]} path="/tickets/show/:id" component={ViewTicket} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_USER", "ROLE_AGENT", "ROLE_SUPER_ADMIN"]} path="/tickets/create" component={CreateTicket} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_USER", "ROLE_AGENT", "ROLE_SUPER_ADMIN"]} path="/tickets/" component={ListTickets} />
                    <CustomRoute roles={["ROLE_SUPER_ADMIN"]} path="/subscriptions/create" component={AddSubscription} />
                    <CustomRoute roles={["ROLE_SUPER_ADMIN"]} path="/subscriptions/packages" component={CreateTicket} />
                    <CustomRoute roles={["ROLE_SUPER_ADMIN"]} path="/tenants" component={Companies} />
                    <CustomRoute roles={["ROLE_SUPER_ADMIN"]} path="/tenants/create" component={AddCompany} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/clients" component={ListClients} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/projects/" component={ListProjects} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/projects/create" component={CreateProject} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/subscriptions/packages/create" component={CreateTicket} />
                    <CustomRoute roles={["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]} path="/subscriptions/packages/create" component={AddPackage} />
                    <Route path="/profile" component={Profile} exact/>
                </div>

                <AppFooter />

                {staticMenuMobileActive && <div className="layout-mask"></div>}
            </div>


            <AppConfig themeColor={themeColor} onThemeChange={onThemeChange}
                inputStyle={inputStyle} onInputStyleChange={onInputStyleChange}
                layoutMode={layoutMode} changeMenuMode={changeMenuMode}
                ripple={ripple} onRippleChange={onRippleChange}
                scheme={scheme} onSchemeChange={onSchemeChange} />
        </div>
    );

}

export default App;

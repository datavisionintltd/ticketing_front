// const BASE_URL = 'http://154.74.133.74:61616'
const BASE_URL = 'http://localhost:8181'

export const API_BASE_URL = BASE_URL;

export const OAUTH = BASE_URL + '/auth';
export const USER = BASE_URL + '/users';
export const ROLE = BASE_URL + '/roles';
export const DICTIONARY = BASE_URL + '/dictionaries';
export const SETTING = BASE_URL + '/general/settings';
export const DICTIONARY_ITEMS_BY_CODES = BASE_URL + DICTIONARY + '/items/by/code';
export const ITEMS = BASE_URL  + '/items';
export const COMMON = BASE_URL + '/common';
export const TASKS = BASE_URL + '/tasks';
export const SETTINGS = BASE_URL + '/settings';
export const SLA = BASE_URL + '/slas';
export const TOPICS = BASE_URL + '/topics';
export const EMAIL_TEMPLATE = BASE_URL + '/email/templates';

export const DEPARTMENT = BASE_URL + '/departments';
export const TEAM = BASE_URL + '/teams';
export const TEAM_USER = BASE_URL + '/teams/users';
export const PAYMENTS = BASE_URL + '/payments';
export const CLIENT = BASE_URL + '/clients';
export const PROJECTS = BASE_URL + '/projects';
export const TICKETS = BASE_URL + '/tickets';
export const SUBSCRIPTIONS = BASE_URL + '/subscriptions';
export const COMPANIES = BASE_URL + '/tenants';
export const PACKAGES = BASE_URL + '/subscription/packages';
export const CHARTS = BASE_URL + '/charts';

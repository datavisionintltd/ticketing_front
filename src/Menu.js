import { hasRole } from './utilities/Helpers';

export const Menu = () => {

    let ticketMenu = { label: 'Tickets', icon: 'pi pi-fw pi-bookmark' ,to: '/tickets' };

    let userItems = [];
    if(hasRole("ROLE_ADMIN", "ROLE_SUPER_ADMIN")){
        userItems.push({ label: 'Agents', icon: 'pi pi-fw pi-users', to: '/agents' });
        userItems.push({ label: 'Departments', icon: 'pi pi-fw pi-table', to: '/departments' });
        userItems.push({ label: 'Teams', icon: 'pi pi-fw pi-table', to: '/teams' });
    }
    if(hasRole("ROLE_SUPER_ADMIN")){
        userItems.push({ label: 'Users', icon: 'pi pi-fw pi-users', to: '/users' });
        userItems.push({ label: 'Roles', icon: 'pi pi-fw pi-lock', to: '/users/roles' });
    }
    let usersMenu =
    {
        label: 'Agents', icon: 'pi pi-fw pi-users',
        items: userItems,
    };

    let clientsItems = [];
    if(hasRole("ROLE_SUPER_ADMIN")){
        clientsItems.push({ label: 'Clients', icon: 'pi pi-fw pi-th-large', to: '/tenants' });
    }
    if(hasRole("ROLE_ADMIN", "ROLE_SUPER_ADMIN")){
        clientsItems.push({ label: 'All Customers', icon: 'pi pi-fw pi-users', to: '/clients' });
        clientsItems.push({ label: 'Products', icon: 'pi pi-fw pi-book', to: '/projects' });
    }
    let clientsMenu =
    {
        label: 'Customers', icon: 'pi pi-fw pi-users',
        items: clientsItems,
    };

    let supportItems = [];
    let supportMenu =
    {
        label: 'Activities', icon: 'pi pi-fw pi-users',
        items: supportItems,
    };
    if(hasRole("ROLE_AGENT", "ROLE_ADMIN", "ROLE_SUPER_ADMIN")){
        supportItems.push({ label: 'Tasks', icon: 'pi pi-fw pi-print',to: '/tasks' });
    }

    let notificationsMenu =
    {
        label: 'Notifications', icon: 'pi pi-fw pi-bell',
        items: [
            { label: 'Create', icon: 'pi pi-fw pi-id-card', to: '/notifications/create' },
            { label: 'All', icon: 'pi pi-fw pi-id-card', to: '/notifications/' },
        ]
    };
    let subscriptionsMenu =
    {
        label: 'Subscriptions', icon: 'pi pi-fw pi-desktop',
        items: [
            { label: 'Create', icon: 'pi pi-fw pi-id-card', to: '/subscriptions/create' },
            { label: 'All', icon: 'pi pi-fw pi-id-card', to: '/subscriptions/' },
            { label: 'Packages', icon: 'pi pi-fw pi-id-card', to: '/subscriptions/packages/' },
            { label: 'Create Package', icon: 'pi pi-fw pi-id-card', to: '/subscriptions/packages/create' },
        ]
    };

    let settingItems = [];
    if(hasRole("ROLE_ADMIN", "ROLE_SUPER_ADMIN")){
        // settingItems.push({ label: 'Dictionary', icon: 'pi pi-fw pi-id-card', to: '/dictionaries' })
        // settingItems.push({ label: 'General Settings', icon: 'pi pi-fw pi-id-card', to: '/settings' })
        // settingItems.push({ label: 'Tickets', icon: 'pi pi-fw pi-id-card', to: '/settings/tickets' })
        // settingItems.push({ label: 'Sytem', icon: 'pi pi-fw pi-id-card', to: '/settings/systems' })
        settingItems.push({ label: 'Categories', icon: 'pi pi-fw pi-id-card', to: '/settings/topics' });
        settingItems.push({ label: 'Priorities', icon: 'pi pi-fw pi-id-card', to: '/settings/slas' });
        // settingItems.push({ label: 'Email template', icon: 'pi pi-fw pi-id-card', to: '/settings/emails' })
    }

    let settingsMenu =
    {
        label: 'Settings', icon: 'pi pi-fw pi-sitemap',
        items: settingItems
    };

    const menu = [
        { label: 'Dashboard', icon: 'pi pi-fw pi-home', to: '/' },
    ];

    if (hasRole("ROLE_SUPER_ADMIN")){
        menu.push(ticketMenu, supportMenu, usersMenu, clientsMenu, settingsMenu);
    } else if (hasRole("ROLE_ADMIN")){
        menu.push(ticketMenu, supportMenu, usersMenu, clientsMenu, settingsMenu);
    } else if (hasRole("ROLE_AGENT")){
        menu.push(ticketMenu);
        menu.push(supportMenu);
    } else if (hasRole("ROLE_USER")) {
        menu.push(ticketMenu);
    }

    return menu;
}

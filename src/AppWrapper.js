import React, { useEffect } from 'react';
import { Route, withRouter, useLocation } from 'react-router-dom';
import App from "./App";
import { Login } from "./pages/Login";
import { Error } from "./pages/Error";
import { NotFound } from "./pages/NotFound";
import { Access } from "./pages/Access";
import { ResetPassword } from './pages/ResetPassword';
import { ResetInfo } from './pages/ResetInfo';
import { ForgotPassword } from './pages/ForgotPassword';
import { SignUp } from './pages/SignUp';
import { Activate } from './pages/Activate';
import { Register } from './pages/Register';
import { Welcome } from './pages/Welcome';
import { QuickSla } from './components/sla/QuickSla';
import { QuickTopics } from './components/topic/QuickTopics';
import { QuickDepartments } from './components/user/QuickDepartments';
import { SetPassword } from './pages/SetPassword';
import { Congratulations } from './pages/Congratulations';
import { Galaxy } from './pages/Galaxy';

const AppWrapper = (props) => {
	let location = useLocation();

	useEffect(() => {
		window.scrollTo(0, 0)
	}, [location]);

    switch (props.location.pathname) {
        case "/login":
            return <Route path="/login" component={Login} />
        case "/register":
            return <Route path="/register" component={Register} />
        case "/error":
            return <Route path="/error" component={Error} />
        case "/notfound":
            return <Route path="/notfound" component={NotFound} />
        case "/access":
            return <Route path="/access" component={Access} />
        case "/welcome":
            return <Route path="/welcome" component={Welcome} />
        case "/quick/slas":
            return <Route path="/quick/slas" exact component={QuickSla} />
        case "/quick/topics":
            return <Route path="/quick/topics" exact component={QuickTopics} />
        case "/quick/departments":
            return <Route path="/quick/departments" exact component={QuickDepartments} />
        case "/congratulations":
            return <Route path="/congratulations" component={Congratulations} />
        case "/set/password":
            return <Route path="/set/password" exact component={SetPassword} />
        case "/activate":
            return <Route path="/activate" component={Activate} />
        case "/forgot/password":
            return <Route path="/forgot/password" exact component={ForgotPassword} />
        case "/reset/info":
            return <Route path="/reset/info" exact component={ResetInfo} />
        case "/reset/password":
            return <Route path="/reset/password" exact component={ResetPassword} />
        default:
            if (props.location.pathname.includes("signup")) {
                return <Route path="/signup/:uuid" component={SignUp} />
            } else if (props.location.pathname.includes("galaxy")) {
                return <Route path="/galaxy/:token" component={Galaxy} />
            } else {
                return <App />;
            }
    }
}

export default withRouter(AppWrapper);

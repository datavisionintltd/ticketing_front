import { format } from "date-fns";
import { ROLES, USER_ID } from "../constants";

export function hasRole() {
    let hasRole = false;
    for (const role of arguments) {
        if (checkRole(role)) {
            hasRole = true;
            break;
        };
    }
    return hasRole;
}

const checkRole = roleName => {
    const role = JSON.parse(localStorage.getItem(
        ROLES) != null ?
        localStorage.getItem(ROLES) :
        '[{"id":0,"name":"ROLE_NONE"}]'
    )
        .filter(role => role.name.includes(roleName));
    return role.length > 0
}

export const dateFormat = date => {
    return date ?
        format(new Date(Date.parse(date)), "dd/MM/yyyy") :
        "No Yet"
}

export const dateTimeFormat = date => {
    return date ?
        format(new Date(Date.parse(date)), "dd/MM/yyyy HH:ii") :
        "No Yet"
}

export const getUserId = () => {
    return localStorage.getItem(USER_ID) || null;
}


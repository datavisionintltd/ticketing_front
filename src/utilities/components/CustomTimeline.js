import React from 'react'

export const CustomTimeline = ({ messages }) => {

    const messageBody = (message, index) => (
        <div key={index}>
            {message.position == 'left' ?
                <div className="p-grid">
                    <div className="p-col-10">
                        <div className="card" style={{ borderLeft: '5px #8882bd solid' }}>
                            <div className="p-grid">
                                <div className="p-col-2 p-text-bold">
                                    {message.name}
                                </div>
                                <div className="p-col-8"></div>
                                <p className="p-col-2 p-text-right p-text-bold">
                                    {message.time}
                                </p>
                            </div>
                            <div className="p-grid">
                                <div className="p-col-12">
                                    {message.message}
                                </div>
                            </div>
                        </div>
                    </div>
                </div> :
                <div className="p-grid">
                    <div className="p-col-2"></div>
                    <div className="p-col-10 p-text-right">
                        <div className="card" style={{ borderRight: '5px #8882bd solid' }}>
                            <div className="p-grid">
                                <div className="p-col-2 p-text-left p-text-bold">
                                    {message.time}
                                </div>
                                <div className="p-col-8"></div>
                                <p className="p-col-2 p-text-right p-text-bold">
                                    {message.name}
                                </p>
                            </div>
                            <div className="p-grid">
                                <div className="p-col-12">
                                    {message.message}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </div>
    )
    return (
        <div style={{backgroundColor: '#efefef', padding: '30px'}}>
            { Array.isArray(messages) ? messages.map(messageBody):null }
        </div>

    )
}

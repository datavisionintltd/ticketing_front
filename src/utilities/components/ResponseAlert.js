import { Link } from "react-router-dom"

export const ResponseAlert = props => {

    return (
        <>
            { props.failed === false &&
                <div className="p-message p-component p-message-success" style={{ margin: '0 0 1em 0', display: 'block', marginLeft: "2em", marginRight: "2em" }}>
                    <div className="p-message-wrapper">
                        <div className="p-col-1 p-md-1 p-lg-1">
                            <span className="p-message-icon pi pi-fw pi-2x pi-check"></span>
                        </div>
                        <div className="p-col-10 p-md-10 p-lg-10">
                            <span className="p-message-text">{props.alert}.</span>
                        </div>
                        <div className="p-col-1 p-md-1 p-lg-1 p-text-right">
                            <Link to="#" onClick={() => props.setFailed(null)} className="p-message-icon pi pi-times-circle"></Link>
                        </div>
                    </div>
                </div>
            }
            { props.failed === true &&
                <div className="p-message p-component p-message-warn" style={{ margin: '0 0 1em 0', display: 'block', marginLeft: "2em", marginRight: "2em" }}>
                    <div className="p-message-wrapper">
                        <div className="p-col-1 p-md-1 p-lg-1">
                            <span className="p-message-icon pi pi-fw pi-2x pi-exclamation-triangle"></span>
                        </div>
                        <div className="p-col-10 p-md-10 p-lg-10">
                            <span className="p-message-text">{props.alert}.</span>
                        </div>
                        <div className="p-col-1 p-md-1 p-lg-1 p-text-right">
                            <Link to="#" onClick={() => props.setFailed(null)} className="p-message-icon pi pi-times-circle"></Link>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}

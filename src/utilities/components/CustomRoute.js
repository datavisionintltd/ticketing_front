import { Route } from "react-router"
import { hasRole } from "../Helpers"

export const CustomRoute = ({roles,path,component}) => {
    return hasRole(...roles) ? <Route path={path} component={component} exact/>: "";
}
